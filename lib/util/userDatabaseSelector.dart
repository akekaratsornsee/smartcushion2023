
// move the dialog into it's own stateful widget.
// It's completely independent from your page
// this is good practice
import 'package:custom_cupertino_picker/custom_cupertino_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserParentDatabaseSelector extends StatefulWidget {
  /// initial selection for the slider
  final List parents;

  const UserParentDatabaseSelector({Key key, this.parents}) : super(key: key);

  @override
  _UserParentDatabaseSelectorState createState() => _UserParentDatabaseSelectorState();
}

class _UserParentDatabaseSelectorState extends State<UserParentDatabaseSelector> {
  /// current selection of the slider
  List parents;
  String selector;
  @override
  void initState() {
    super.initState();
    parents = widget.parents;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text('เลือกผู้ใช้'),
      content: Container(
        height: height * 0.25,
        width: width * 0.6,
        child: Column(
          children: [
            Expanded(
              child: CustomCupertinoPicker(
                scrollPhysics: const FixedExtentScrollPhysics(
                  parent: BouncingScrollPhysics(),
                ),
                scrollController: new FixedExtentScrollController(
                    ),
                itemExtent: 32,
                useMagnifier: true,
                magnification: 1.2,
                backgroundColor: Colors.white,
                onSelectedItemChanged: (int index) {
                  setState(() {
                    selector = parents[index];
                  });
                },
                children: parents
                    .map((e) => Center(
                          child: Transform.scale(
                            scale: 1.3,
                            child: Text(e.toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply()),
                          ),
                        ))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            // Use the second argument of Navigator.pop(...) to pass
            // back a result to the page that opened the dialog
            Navigator.pop(context, selector);
          },
          child: Text('เลือก'),
        )
      ],
    );
  }
}
typedef DateTimeCallback = void Function(DateTime onDateTimeChange);
