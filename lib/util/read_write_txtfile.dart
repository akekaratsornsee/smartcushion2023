import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:flutter_restart/flutter_restart.dart';

write(String text, String fileName) async {
  final Directory directory = await getApplicationDocumentsDirectory();
  final File fileuser = File('${directory.path}/$fileName.txt');
  await fileuser.writeAsString(text);
}

Future<String> read(String fileName) async {
  String text;
  try {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File fileuser = File('${directory.path}/$fileName.txt');
    text = await fileuser.readAsString();
  } catch (e) {
    throw ("Couldn't read file");
    print("Couldn't read file ${e.toString()}");
  }
  return text;
}

Future<dynamic> removefile(String fileName) async {
  try {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File fileuser = File('${directory.path}/$fileName.txt');
    await fileuser.delete();
  } catch (e) {
    print("Couldn't remove");
  }
}
//////////////////////////////////////////////////////////////////////////////////

writeIndex(String text, String fileName2) async {
  final Directory directory = await getApplicationDocumentsDirectory();
  final File fileuser = File('${directory.path}/$fileName2.txt');
  await fileuser.writeAsString(text);
}

Future<String> readIndex(String fileName2) async {
  String text;
  try {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File fileuser = File('${directory.path}/$fileName2.txt');
    text = await fileuser.readAsString();
  } catch (e) {
    // throw ("Couldn't read file");
    removefile("databaseName");
    FlutterRestart.restartApp();
  }
  return text;
}

Future<dynamic> removefileIndex(String fileName2) async {
  try {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File fileuser = File('${directory.path}/$fileName2.txt');
    await fileuser.delete();
  } catch (e) {
    print("Couldn't remove");
  }
}
