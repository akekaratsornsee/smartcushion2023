
import 'package:intl/intl.dart';

dairyDataQuery(Map data, {bool fullDate = false, DateTime datetimeSelector , var weeklycall = false}){
  if(fullDate && datetimeSelector == null) throw("plz fill the para 'DateTime datetimeSelector'");
  // Map values = data[data.keys.toList()[0]];
  // Map values2 = data[data.keys.toList()[1]];
  var dateQueryFormat = DateFormat('yyyyMMdd').format(datetimeSelector).toString();
  // print("my dairy queryy $dateQueryFormat");
  var values = data["$dateQueryFormat"];
  // print("my dairy queryy DATA ::  $values");

  var yesterday = DateFormat('yyyyMMdd').format(datetimeSelector.subtract(Duration(days: 1))).toString(); 
  var values2 = data["$yesterday"];

  // print("refresh day value key:: ${values.toString()}");
  var temp = {};
  var tempTime = fullDate ? "23" : datetimeSelector.hour.toString();
  var subter = 0;
  // print("now hour:: $tempTime");
  for(var i = 23; i > -1; i--){
    var newTempTime = (int.parse(tempTime) - subter).toString();
    var hour = newTempTime.length == 1 ? "0" + newTempTime : newTempTime;
    temp[hour+"0000"] = 0.0;
    subter+=1;
    if(newTempTime == "0"){
      subter = 0;
      tempTime="23";
    }
  }
  // print("time format::: $temp");

  tempTime = fullDate ? "23" : datetimeSelector.hour.toString();
  if (values != null) {

  values.forEach((key, value) {

    var tHour = key.toString().substring(0, 2);
    // print("foreach loop :: ${key.toString().substring(0, 2)}");
    var hour = tHour.substring(0, 1)  == "0" ? "0" + tHour.substring(1, 2) : tHour;
    if(temp[tHour+"0000"] != null && int.parse(hour) <=  int.parse(tempTime) ) {
      // print("hour yesssssssssss:: $hour");
      temp[tHour+"0000"] = temp[tHour+"0000"] + 1.0;
    }
  });
  }else{
    if(weeklycall) return null;
  }



  if(!fullDate && values2 != null ){
  values2.forEach((key, value) {
    var tHour = key.toString().substring(0, 2);
    var hour = tHour.substring(0, 1)  == "0" ? "0" + tHour.substring(1, 2) : tHour;
    if(temp[tHour+"0000"] != null && int.parse(hour) > int.parse(tempTime)) {
      // print("hour yesssssssssss:: $hour");
    temp[tHour+"0000"] = temp[tHour+"0000"] + 1.0;

    }
  });
  }else{
    // if(weeklycall && temp != {}) return null;

  }


  return temp;
}