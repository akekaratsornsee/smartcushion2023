var setting = {
  "config": {"multiply_record_in_secs": 28},
  "color_theme": {
    "sky": {
      "base_color": 0xFFFFFFFF,
      "cover_base_color": 0xFFFFFFFF,
      "backdrop": 0xFFFFFFFF,
      "container_chart_padding": 8.0,
      "container_chart_padding_inner": 0.0,
      "container_chart_BorderRadius": 20.0,
      "tapbar_bg": 0xFF424242,
      "chart_backgroud": 0xFFFFFFFF,
      "chart_top": 0xFF5BD7DD,
      "chart_bottom": 0xFF5BD7DD,
      "chart_top_disable": 0xFF5BD7DD,
      "chart_bottom_disable": 0xFFCAEAFA,
      "chart_text": 0xFFadadad,
      "chart_text_now": 0xFF52FF00,
      "circle_chart_wave_non_with_opacity": 0xFFF7B6CB,
      "circle_chart_opacity": 0.2,
      "circle_chart_bg": 0xFFf2f7fb,
      "text_appName": 0xFF303030,
      "text_head": 0xFF2c2c2c,
      "text_body": 0xFF2c2c2c,
      "text_subhead": 0xFFFFFFFF,
      "text_subheadCC&OC": 0xFF546E7A,
      "text_detail": 0xFFFFFFFF,
      "text_subhead_acci": 0xFF616161,
      "text_alarmNoti_card": 0xFFFFFFFF,
      "text_health_card": 0xFFFFFFFF,
      "haed_alarmNoti_card": 0xFF62C4C3,
      "haed_health_card": 0xFF5193B3,
      "body_alarmNoti_card": 0xFF86D9D8,
      "body_health_card": 0xFF8BCAE8,
      "text_datetime_circle": 0xFFFFFFFF,
      "text_overtime_circle": 0xFFFFFFFF,
      "text_cancel_counting_circle": 0xFFFFFFFF,
      "cicle_data_date_bg": 0xFF5193B3,
      "cicle_data_avg_bg": 0xFF5193B3,
      "cicle_data_overtime_bg": 0xFF62C4C3,
      "cicle_data_cancel_counting_bg": 0xFFF8D49B,
      "cicle_data_date_text": 0xFFFFFFFF,
      "cicle_data_avg_text": 0xFFFFFFFF,
      "cicle_data_overtime_text": 0xFFF8E6CB,
      "cicle_data_cancel_counting_text": 0xFF8D8358,
      "circle_chart": {
        "1": 0xFF5BD7DD,
        "2": 0xFF69D9DE,
        "3": 0xFF79DADF,
        "4": 0xFFBDF6FA,
        "5": 0xFF9AE8EC,
        "6": 0xFFC7F6F9,
        "7": 0xFFDAFAFC,
      }
    }
  }
};

var txt_log = {
  "circle_widget_alert": {
    "overt_time": {
      "head": "นั่งเกิน",
      "body":
          "นับจำนวนการนั่งเกินของผู้ใช้เป็นจำนวนครั้ง ตามที่ผู้ใช้ได้ตั้งค่าไว้",
      "image_path": ""
    },
    "calcel": {
      "head": "การยกเลิกแจ้งเตือน",
      "body":
          "นับจำนวนครั้งที่ผู้ใช้กดยกเลิก หรือเพิกเฉย เมื่อมีการแจ้งเ่ตือนการนั่งเกินเวลา",
      "image_path": ""
    },
  }
};

// colorSkyTheme["base_color"]
// colorSkyTheme["cover_base_color"]
// colorSkyTheme["backdrop"]  1C2141 14172D
// colorSkyTheme["chart_backdrop"]
// colorSkyTheme["chart_backgroud"]
// colorSkyTheme["chart_top"]
// colorSkyTheme["chart_bottom"]
// colorSkyTheme["chart_top_disable"]
// colorSkyTheme["chart_bottom_disable"]
// colorSkyTheme["text_head"]
// colorSkyTheme["text_body"]

Map colorSkyTheme = setting["color_theme"]["sky"];
