import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

Widget getDateRangePicker(selectionChanged, isRange) {
  return Container(
      height: 300,
      child: Card(
          child: SfDateRangePicker(
        view: DateRangePickerView.month,
        selectionMode: isRange ? DateRangePickerSelectionMode.range: DateRangePickerSelectionMode.single,
        onSelectionChanged: selectionChanged,
      )));
}

