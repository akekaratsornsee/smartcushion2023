import 'package:appmattress/homepage.dart';
import 'package:appmattress/senddef.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert' show utf8;
import 'package:flutter/painting.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'util/read_write_txtfile.dart';
import 'dart:math';
import 'package:flutter_restart/flutter_restart.dart';

class BLE extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wifi Setter Via BLE',
      debugShowCheckedModeBanner: false,
      home: WifiSetter(),
      routes: {
        '/HOME-page': (context) => Homepage(),
        '/BLEpage-page': (context) => BLE(),
      },
      // theme: ThemeData.dark(),
    );
  }
}

class WifiSetter extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WifiSetterState();
}

class _WifiSetterState extends State<WifiSetter> {
  final String SERVICE_UUID = "4fafc201-1fb5-459e-8fcc-c5c9c331914b";
  final String CHARACTERISTIC_UUID = "beb5483e-36e1-4688-b7f5-ea07361b26a8";
  final String TARGET_DEVICE_NAME = "Smart Cushion";

  final formKey = GlobalKey<FormState>();
  FlutterBlue flutterBlue = FlutterBlue.instance;
  StreamSubscription<ScanResult> scanSubscription;

  BluetoothDevice targetDevice;
  BluetoothCharacteristic targetCharacteristic;

  String connectionText = "";

  final dataRef = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startScan();
    callRead();
  }

  setdefault_AT_TU() async {
    var AlertTS = 2400;
    var countTU = 300;
    String databaseSelector = await read("databaseName");

    await dataRef
        .child(databaseSelector)
        .child('Value')
        .update({'AlertTimeInSeconds': AlertTS.toInt()});
    await dataRef
        .child(databaseSelector)
        .child('Value')
        .update({'TimeUp': countTU.toInt()});
  }

  callRead() async {
    String databaseNameTxt = await read("databaseName");
    print("read from txt file BLE $databaseNameTxt");
    if (databaseNameTxt != "") {
      Navigator.pushNamed(
        context,
        '/HOME-page',
        arguments: smartNameController.text,
      );
    } else {
      print("Push username");
    }
    //   setState(() {
    //     this.widget.dbName(databaseNameTxt);
    //   });
    //   // Navigator.of(context).pop();
    // } else {
    //   // do something
    // }
  }

  startScan() {
    setState(() {
      connectionText = "Smart Cushion Start Scanning";
    });

    scanSubscription = flutterBlue.scan().listen((scanResult) {
      if (scanResult.device.name.contains(TARGET_DEVICE_NAME)) {
        stopScan();

        setState(() {
          connectionText = "Found Target Device ";
        });
        targetDevice = scanResult.device;
        connectToDevice();
      }
    }, onDone: () => stopScan());
  }

  stopScan() {
    scanSubscription?.cancel();
    scanSubscription = null;
  }

  connectToDevice() async {
    if (targetDevice == null) {
      return;
    }
    setState(() {
      connectionText = "Device Connecting";
    });
    await targetDevice.connect();
    setState(() {
      connectionText = "Device Connected";
    });

    discoverServices();
  }

  disconnectFromDevice() {
    if (targetDevice == null) {
      return;
    }
    targetDevice.disconnect();
    setState(() {
      connectionText = "Device Disconnected";
    });
  }

  discoverServices() async {
    if (targetDevice == null) {
      return;
    }
    List<BluetoothService> services = await targetDevice.discoverServices();
    services.forEach((service) {
      if (service.uuid.toString() == SERVICE_UUID) {
        service.characteristics.forEach((characteristics) {
          if (characteristics.uuid.toString() == CHARACTERISTIC_UUID) {
            targetCharacteristic = characteristics;
            setState(() {
              connectionText = "All Ready with ${targetDevice.name}";
            });
          }
        });
      }
    });
  }

  writeData(String data) async {
    if (targetCharacteristic == null) return;
    List<int> bytes = utf8.encode(data);
    await targetCharacteristic.write(bytes);
  }

  @override
  void dispose() {
    super.dispose();
    stopScan();
  }

  readFromTxt() {
    var dbnameHis = "database name from txt file";
    setState(() {});
  }

  submitAction(dbName) {
    var wifiData =
        '${wifiNameController.text},${wifiPasswordController.text},${smartNameController.text}';
    writeData(wifiData);
    setState(() {});
  }

  TextEditingController wifiNameController = TextEditingController();
  TextEditingController wifiPasswordController = TextEditingController();
  TextEditingController smartNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(

        // ),
        body: Container(
            child: targetCharacteristic == null
                ? Center(
                    //   child: ListView(children: [
                    //     Padding(
                    //         padding: EdgeInsets.fromLTRB(0, 200, 6, 15),
                    //         child: Image.asset(
                    //           'images/logo22.png',
                    //           height: 210,
                    //           width: 210,
                    //         )),
                    //     Center(
                    //       // Padding(
                    //       //   padding: EdgeInsets.fromLTRB(155, 250, 20, 0),
                    //       child: Text(
                    //         "SMART CUSHION START SCANNING",
                    //         style: TextStyle(fontSize: 13, color: Colors.teal),
                    //       ),
                    //     ),
                    //     Padding(
                    //       padding: EdgeInsets.fromLTRB(168, 30, 0, 150),
                    //       child: Text(
                    //         "Waiting...",
                    //         style:
                    //             TextStyle(fontSize: 18, color: Colors.grey[600]),
                    //       ),
                    //     ),
                    //   ]),
                    // )

                    child: ListView(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
                          child: Image.asset(
                            'images/logo22.png',
                            height: 180,
                            width: 180,
                          )),
                      Center(
                          child: Text(
                        "CONFIG SMART CUSHION",
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[600],
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(40, 20, 40, 8),
                          child: Text(
                            "สำหรับเข้าสู่ระบบหรือตั้งค่า Smart Cushion",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey[400],
                              //fontWeight: FontWeight.bold,
                            ),
                          )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 0, 40, 15),
                        child: TextField(
                            controller: smartNameController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                labelText: "Username "),
                            style: TextStyle(
                                fontSize: 14.0,
                                height: 0.5,
                                color: Colors.black)),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(40, 0, 40, 8),
                          child: Text(
                            "สำหรับตั้งค่า Smart Cushion",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey[400],
                              //fontWeight: FontWeight.bold,
                            ),
                          )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
                        child: TextField(
                            controller: wifiNameController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                labelText: "WIFI-Name"),
                            style: TextStyle(
                                fontSize: 14.0,
                                height: 0.5,
                                color: Colors.black)),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 5, 40, 0),
                        child: TextField(
                            controller: wifiPasswordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                labelText: "WIFI-Password"),
                            style: TextStyle(
                                fontSize: 14.0,
                                height: 0.5,
                                color: Colors.black)),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(70, 50, 70, 0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () async {
                            // ignore: unnecessary_statements
                            submitAction(smartNameController.text.toString());
                            await write(
                                "${smartNameController.text.toString()}",
                                "databaseName");
                            await writeIndex("0", "IndexBar");
                            await callRead();
                            Navigator.pushNamed(
                              context,
                              '/HOME-page',
                              arguments: smartNameController.text,
                            ); //ข้ามหน้า
                            // Navigator.of(context).pop();
                            setState(() {});
                          },
                          color: Color(0xff5AAFB1),
                          child: Text('เข้าสู่ระบบ'),
                        ),
                      ),
                      Center(
                          child: Text(
                        "___________หรือ___________",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey[600],
                        ),
                      )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(70, 2, 70, 0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () async {
                            // ignore: unnecessary_statements

                            await FlutterRestart.restartApp(); //ข้ามหน้า
                            // Navigator.of(context).pop();
                          },
                          color: Colors.grey[400],
                          child: Text('ค้นหาอุปกรณ์'),
                        ),
                      ),
                    ],
                  ))
                : Center(
                    child: ListView(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
                          child: Image.asset(
                            'images/logo22.png',
                            height: 180,
                            width: 180,
                          )),
                      Center(
                          child: Text(
                        "CONFIG SMART CUSHION",
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[600],
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(40, 20, 40, 8),
                          child: Text(
                            "สำหรับเข้าสู่ระบบหรือตั้งค่า Smart Cushion",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey[400],
                              // fontWeight: FontWeight.bold,
                            ),
                          )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 0, 40, 15),
                        child: TextField(
                            controller: smartNameController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                labelText: "Username "),
                            style: TextStyle(
                                fontSize: 14.0,
                                height: 0.5,
                                color: Colors.black)),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(40, 0, 40, 8),
                          child: Text(
                            "สำหรับตั้งค่า Smart Cushion",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey[400],
                              // fontWeight: FontWeight.bold,
                            ),
                          )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
                        child: TextField(
                            controller: wifiNameController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                labelText: "WIFI-Name"),
                            style: TextStyle(
                                fontSize: 14.0,
                                height: 0.5,
                                color: Colors.black)),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 5, 40, 0),
                        child: TextField(
                            controller: wifiPasswordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                labelText: "WIFI-Password"),
                            style: TextStyle(
                                fontSize: 14.0,
                                height: 0.5,
                                color: Colors.black)),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(70, 50, 70, 0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () async {
                            // ignore: unnecessary_statements
                            submitAction(smartNameController.text.toString());
                            await write(
                                "${smartNameController.text.toString()}",
                                "databaseName");
                            await writeIndex("0", "IndexBar");
                            await callRead();

                            Navigator.pushNamed(
                              context,
                              '/HOME-page',
                              arguments: smartNameController.text,
                            ); //ข้ามหน้า
                            // Navigator.of(context).pop();
                            setState(() {});
                          },
                          color: Color(0xff5AAFB1),
                          child: Text('เข้าสู่ระบบ'),
                        ),
                      ),
                      Center(
                          child: Text(
                        "___________หรือ___________",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey[600],
                        ),
                      )),
                      Padding(
                        padding: EdgeInsets.fromLTRB(70, 2, 70, 0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () async {
                            // ignore: unnecessary_statements
                            submitAction(smartNameController.text.toString());
                            await write(
                                "${smartNameController.text.toString()}",
                                "databaseName");
                            await writeIndex("0", "IndexBar");
                            await callRead();
                            Navigator.pushNamed(
                              context,
                              '/HOME-page',
                              arguments: smartNameController.text,
                            ); //ข้ามหน้า
                            // Navigator.of(context).pop();
                            await setdefault_AT_TU();
                            setState(() {});
                          },
                          color: Color(0xff5A89B1),
                          child: Text('ตั้งค่าอุปกรณ์'),
                        ),
                      ),
                    ],
                  ))));
  }
}

typedef StateDatabaseNameCallBack = void Function(String dbName);
