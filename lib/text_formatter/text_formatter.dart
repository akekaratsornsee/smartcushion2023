import 'package:appmattress/util/config.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

subHaed(context, text) => Padding(
      padding: const EdgeInsets.all(15.0),
      child: AutoSizeText(
        "$text",
        style: Theme.of(context)
            .textTheme
            .headline6
            .apply(color: Color(colorSkyTheme["text_subhead"])),
      ),
    );
