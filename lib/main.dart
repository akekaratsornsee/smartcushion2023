import 'dart:convert';

import 'package:appmattress/BLEpage.dart';
import 'package:appmattress/chartCirle.dart';
import 'package:appmattress/databaseTest.dart';
import 'package:appmattress/test.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/read_write_txtfile.dart';
import 'package:appmattress/util/tempMock.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'homepage.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'navigationDrawer/scrolcontrol_with_tabbar_and_silver_app.dart';

// String databaseSelector;

final _alarmNotiRef =
    FirebaseDatabase.instance.reference().child("Value").child("Alarm");
final myHisdata = FirebaseDatabase.instance.reference();

class GetHistoryData {
  var data;
  init() async {
    String databaseSelector = await read("databaseName");
    var tempData = await myHisdata.child(databaseSelector).child("DATE").once();
    data = tempData.value;

    // data = mock2;
    print("init data in main $data");
  }
}

void main() async {
  Intl.defaultLocale = "th_TH";
  initializeDateFormatting("th_TH");
  // WidgetsFlutterBinding.ensureInitialized();

  // AwesomeNotifications().initialize('resource://drawable/res_app_icon', [
  //   NotificationChannel(
  //       channelKey: 'basic_channel',
  //       channelName: 'Basic notifications',
  //       channelDescription: 'Notification channel for basic tests',
  //       defaultColor: Color(0xFF9D50DD),
  //       ledColor: Colors.white)
  // ]);

  // AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
  //   if (!isAllowed) {
  //     // Insert here your friendly dialog box before call the request method
  //     // This is very important to not harm the user experience
  //     AwesomeNotifications().requestPermissionToSendNotifications();
  //   }
  // });
  // // Create the initialization for your desired push service here
  // FirebaseApp firebaseApp = await Firebase.initializeApp();
  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
  //   print('Got a message whilst in the foreground!');
  //   print('message ==> ${message.data}');

  //   await AwesomeNotifications().createNotification(
  //       content: NotificationContent(
  //           id: 10,
  //           channelKey: 'basic_channel',
  //           title: '${json.decode(message.data["content"])["title"]}',
  //           body: '${json.decode(message.data["content"])["body"]}'));
  //   try {
  //     if (message.data["content"]["sitNoti"] == true) {
  //       await _alarmNotiRef.runTransaction((MutableData mutableData) async {
  //         mutableData.value = 1;
  //         return mutableData;
  //       });
  //     }
  //   } catch (e) {
  //     print("Noti is not alert2!");
  //   }
  // });
  // await FirebaseMessaging.instance.subscribeToTopic("sittingNoti");
  // await notiSubscription(_alarmNotiRef);
  // await generateKey();
  // bool tempInforChecker = await informationChecker();

  return runApp(BLE()); //login => submit => homepage

  // return runApp(MyApp()); // homepage
}

generateKey() async {
  var myKey = await FirebaseMessaging.instance.getToken();
  print("my devices token ::: $myKey");
}

// Declared as global, outside of any class
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("Handling a background message: ${message.messageId}");
  await AwesomeNotifications().createNotificationFromJsonData(message.data);
  await AwesomeNotifications().createNotification(
      content: NotificationContent(
          id: 10,
          channelKey: 'basic_channel',
          title: '${message.data["content"]["title"]}',
          body: '${message.data["content"]["body"]}'));
  try {
    if (message.data["content"]["sitNoti"] == true) {
      await _alarmNotiRef.runTransaction((MutableData mutableData) async {
        mutableData.value = 1;
        return mutableData;
      });
    }
  } catch (e) {
    print("Noti is not alert3!");
  }
}

notiSubscription(_alarmNotiRef) async {
  _alarmNotiRef.keepSynced(true);
  var _counterSubscription = _alarmNotiRef.onValue.listen((Event event) async {
    if (event.snapshot.value == 1) {
      // _sittingNotiDialog();
      print("passed condition");
      // await AwesomeNotifications().createNotification(
      //   content: NotificationContent(
      //       id: 10,
      //       channelKey: 'basic_channel',
      //       title: 'Simple Notification',
      //       body: 'Simple body'
      //   )
      // );
    } else {
      print("not pass condition");
    }
  }, onError: (Object o) {
    final DatabaseError error = o;
  });
}

// class MyApp extends StatelessWidget {
//   MyApp({this.dbName});
//   String dbName;
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       // localizationsDelegates: [
//       //   GlobalMaterialLocalizations.delegate,
//       //   GlobalWidgetsLocalizations.delegate,
//       // ],

//       // supportedLocales: [
//       //   const Locale('th'),
//       //   const Locale('en', 'US'),
//       // ],
//       locale: const Locale('th'),
//       debugShowCheckedModeBanner: false,
//       title: 'MATTRESS',
//       theme: ThemeData(
//           primaryColor: Color(colorSkyTheme["base_color"]),
//           fontFamily: "Athiti"),
//       home: Homepage(),
//       // home: ArcWidget()
//     );
//   }
// }
