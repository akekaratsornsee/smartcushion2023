import 'dart:collection';
import 'dart:math' as math;

import 'package:appmattress/util/config.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class DrawFreqColor extends CustomPainter {
  double maxADay;
  int value;
  String key;
  double circleSize;
  var context;
  // var rect;
  DrawFreqColor(maxADay, value, key, circleSize, context)
      : maxADay = maxADay,
        value = value,
        key = key,
        circleSize = circleSize,
        context = context;
  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double i = 0;
    int counter = 0;
    // print("YES $value");

    // double maxADay = 1;
    Rect rect = Rect.fromCenter(
        center: new Offset((size.width / 2), (size.height / 2) + circleSize),
        width: width * 0.6,
        height: width * 0.6);

    // canvas.drawArc(
    //     rect,
    //     0.0,
    //     2 * math.pi,
    //     false,
    //     Paint()
    //       ..color = Colors.blueGrey
    //       ..strokeWidth = circleSize
    //       ..style = PaintingStyle.stroke);

    if (value > 0) {
      i = ((double.parse(key) - 1) * 14) + 10;
      double end = double.parse(key) * 14;
      i = (i <= 0) ? 340 : i;
      end = (end > 360) ? 360 : end;
      end = (end <= 0) ? 350 : end;
      int count = 0;
      List colorRange1 = List.generate(7, (index) => (index) / 7);
      List colorRange2 = List.generate(7, (index) => (index) / 7);
      List colorRange3 = colorRange2.reversed.toList();
      List totalRange = colorRange1 + [1.0] + colorRange3;
      List colorrrrr = [
        Colors.deepPurple[800],
        Colors.deepPurple[700],
        Colors.deepPurple[500],
        Colors.deepPurple[400],
        Colors.deepPurple[200],
        Colors.deepPurple[100],
        Colors.deepPurple[50]
      ];
      colorrrrr = colorrrrr.reversed.toList();
      while (i < end &&
          counter <= 360 &&
          i > 0 &&
          i <= 360 &&
          end > 0 &&
          end <= 360) {
        Path path2 = Path()
          ..arcTo(rect, (i * math.pi / 180) - math.pi / 2.5, 0.15, false);
        canvas.drawPath(
            path2,
            Paint()
              ..blendMode = BlendMode.srcOver
              ..shader =
                  SweepGradient(startAngle: 0, endAngle: math.pi / 2, colors: [
                colorrrrr[((value * 7) / maxADay).round() - 1 < 0
                        ? 0
                        : ((value * 7) / maxADay).round() - 1]
                    .withOpacity(totalRange[count]),
                colorrrrr[((value * 7) / maxADay).round() - 1 < 0
                        ? 0
                        : ((value * 7) / maxADay).round() - 1]
                    .withOpacity(totalRange[count])
              ]).createShader(rect)
              // ..color = Color(0xff81E5CD).withOpacity(value/maxADay).withGreen((value/maxADay * ((i/end)/15) * 900).round())
              // ..color = colorrrrr[(((value*3)/maxADay)*totalRange[count]).round()-1 < 0 ? 0 : ((value*3)/maxADay).round()-1].withOpacity(totalRange[count])
              ..strokeWidth = circleSize
              ..style = PaintingStyle.stroke);

        i += 1;
        count += 1;
        counter += 1;
      }
      count = 0;
      counter = 0;
    } else {
      print("NOPEEEEEE");
    }
    canvas.save();
    canvas.restore();

    // drawing
    var radius = size.width / 2;

    canvas.translate(radius, radius);
    canvas.restore();
    canvas.save();
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}

class ArcPainter extends CustomPainter {
  var context;
  Map<String, double> listCheckerData;
  SplayTreeMap<String, Map<String, double>> dataListWithMinute;
  DateTime datetimeSelected;
  int maxAday;
  int minAday;
  ArcPainter(listCheckerData, dataListWithMinute, datetimeSelected, maxAday,
      minAday, context)
      : listCheckerData = listCheckerData,
        dataListWithMinute = dataListWithMinute,
        datetimeSelected = datetimeSelected,
        maxAday = maxAday,
        minAday = minAday,
        context = context;

  @override
  bool shouldRepaint(ArcPainter oldDelegate) {
    return false;
  }

  fillTheColor(
      rect, canvas, colorrrrr, colorSelectorIndex, circleSize, start, end) {
    Path path2 = Path()..arcTo(rect, start, end, false);
    canvas.drawPath(
        path2,
        Paint()
          ..blendMode = BlendMode.srcOver
          // ..shader = SweepGradient(startAngle: 0, endAngle: 1.0, colors: [
          //   colorrrrr[colorSelectorIndex].withOpacity(1.0),
          //   colorrrrr[colorSelectorIndex].withOpacity(0.50),
          // ]).createShader(rect)
          ..color = colorrrrr[colorSelectorIndex >= colorrrrr.length
                  ? colorrrrr.length - 1
                  : colorSelectorIndex]
              .withOpacity(1.0)
          // ..color = colorrrrr[(((value*3)/maxADay)*totalRange[count]).round()-1 < 0 ? 0 : ((value*3)/maxADay).round()-1].withOpacity(totalRange[count])
          ..strokeWidth = circleSize
          ..style = PaintingStyle.stroke);
  }

  num degreesToRads(num deg) {
    return (deg * math.pi) / 180.0;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final List text = new List<String>.generate(24, (i) => i.toString());
    final double initialAngle = 0.0;
    // final TextStyle textStyle =  TextStyle(color: Colors.white, fontSize: 10);

    final _textPainter = TextPainter(textDirection: TextDirection.ltr);
    var circleSize = 40.0;
    //new Offset(-(textPainter.width / 2), -(textPainter.height / 2))
    // Rect rect = Rect.fromLTWH(0.0, 10.0, size.width, size.height);
    var center = new Offset((size.width / 2), (size.height / 2) + circleSize);
    Rect rect = Rect.fromCenter(
      center: center,
      width: size.width,
      height: size.width,
    );

    //Outsize shadow
    double radiusShadow = (size.width / 2) + circleSize - 20;
    double radiusShadow2 = 95;
    // draw shadow first
    Path oval = Path()
      ..addOval(Rect.fromCircle(center: center, radius: radiusShadow));
    Path oval2 = Path()
      ..addOval(Rect.fromCircle(center: center, radius: radiusShadow2 - 10));

    Paint shadowPaint = Paint()
      ..color = Colors.grey.withOpacity(0.3)
      ..maskFilter = MaskFilter.blur(BlurStyle.normal, 20);
    Paint shadowPaint2 = Paint()
      ..color = Colors.black.withOpacity(0.3)
      ..maskFilter = MaskFilter.blur(BlurStyle.normal, 20);
    // draw circle
    Paint thumbPaint = Paint()
      ..color = Color(colorSkyTheme["circle_chart_bg"])
      ..style = PaintingStyle.fill;
    Paint thumbPaint2 = Paint()
      ..color = Colors.transparent
      ..style = PaintingStyle.fill;

    // canvas.drawPath(oval, shadowPaint);
    canvas.drawCircle(center, radiusShadow, thumbPaint);
    canvas.drawCircle(center, radiusShadow2,
        thumbPaint2..color = Color(colorSkyTheme["backdrop"]));

    canvas.drawPath(oval2, shadowPaint2);
    canvas.drawCircle(center, radiusShadow2, thumbPaint2);
    //Inner shadow

    canvas.drawArc(
        rect,
        0.0,
        2 * math.pi,
        false,
        Paint()
          ..color = Color(colorSkyTheme["backdrop"]).withOpacity(0.5)
          ..strokeWidth = circleSize
          ..style = PaintingStyle.stroke);

    if (dataListWithMinute?.length > 0) {
      dataListWithMinute.forEach((key, value) {
        int iKey = int.tryParse(key.toString());
        double startRotate90 = degreesToRads(iKey * 15) - degreesToRads(90);
        double minuteMultiple =
            3.75; //3.75 comes from 15 degree (a olock) devided by 4 (amount of minute ranges).
        double start0To15 = startRotate90 + (0 * degreesToRads(minuteMultiple));
        double start15To30 =
            startRotate90 + (1 * degreesToRads(minuteMultiple));
        double start30To45 =
            startRotate90 + (2 * degreesToRads(minuteMultiple));
        double start45To49 =
            startRotate90 + (3 * degreesToRads(minuteMultiple));
        double endMultiple = degreesToRads(minuteMultiple);

        List colorRange1 = List.generate(7, (index) => (index) / 7);
        List colorRange2 = List.generate(7, (index) => (index) / 7);
        List colorRange3 = colorRange2.reversed.toList();
        List totalRange = colorRange1 + [1.0] + colorRange3;
        List<Color> colorrrrr = [
          Color(colorSkyTheme["circle_chart"]["7"]),
          Color(colorSkyTheme["circle_chart"]["6"]),
          Color(colorSkyTheme["circle_chart"]["5"]),
          Color(colorSkyTheme["circle_chart"]["4"]),
          Color(colorSkyTheme["circle_chart"]["3"]),
          Color(colorSkyTheme["circle_chart"]["2"]),
          Color(colorSkyTheme["circle_chart"]["1"]),
        ];
        colorrrrr = colorrrrr.reversed.toList();
        if (value["0-15"] > 0) {
          // print("fill at hour $iKey : 0-15");
          // print(
          // "fill the color start0To15 start ${(start0To15 + plus90) * aRdTodg}  to end ${(start0To15 + plus90 + endMultiple) * aRdTodg}");
          var colorSelectorIndex = getColorIndex(value["0-15"], maxAday);

          fillTheColor(rect, canvas, colorrrrr, colorSelectorIndex, circleSize,
              start0To15, endMultiple);
        }
        if (value["15-30"] > 0) {
          // print("fill at hour $iKey : 15-30");
          // print(
          // "fill the color start15To30 start ${(start15To30 + plus90) * aRdTodg}  to end ${(start15To30 + plus90 + endMultiple) * aRdTodg}");
          var colorSelectorIndex = getColorIndex(value["15-30"], maxAday);
          fillTheColor(rect, canvas, colorrrrr, colorSelectorIndex, circleSize,
              start15To30, endMultiple);
        }
        if (value["30-45"] > 0) {
          // print("fill at hour $iKey : 30-45");
          // print(
          // "fill the color start30To45 start ${(start30To45 + plus90) * aRdTodg}  to end ${(start30To45 + plus90 + endMultiple) * aRdTodg}");
          var colorSelectorIndex = getColorIndex(value["30-45"], maxAday);
          fillTheColor(rect, canvas, colorrrrr, colorSelectorIndex, circleSize,
              start30To45, endMultiple);
        }
        if (value["45-59"] > 0) {
          // print("fill at hour $iKey : 45-59");
          // print(
          // "fill the color start45To49 start ${(start45To49 + plus90) * aRdTodg}  to end ${(start45To49 + plus90 + endMultiple) * aRdTodg}");
          var colorSelectorIndex = getColorIndex(value["45-59"], maxAday);
          fillTheColor(rect, canvas, colorrrrr, colorSelectorIndex, circleSize,
              start45To49, endMultiple);
        }
      });
    }

    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);

    TextStyle textStyle = Theme.of(context)
        .textTheme
        .caption
        .apply(color: Color(colorSkyTheme["text_body"]), fontWeightDelta: 1);
    var angle = 2 * math.pi / 60;
    var radius = (size.width / 2);
    var minuteTcik = new Paint();
    minuteTcik.color = Color(colorSkyTheme["text_body"]);
    minuteTcik.strokeWidth = 10;
    // canvas..drawColor(const Color(0xFFFFFFFF), BlendMode.difference);
    var tickMarkLength;
    var tickPaint = new Paint();
    tickPaint..color = Color(colorSkyTheme["text_body"]);
    final hourTickMarkLength = 10.0;
    final minuteTickMarkLength = 5.0;

    canvas.save();
    final hourTickMarkWidth = 3.0;
    final minuteTickMarkWidth = 1.5;
    canvas.translate(radius, radius + circleSize);
    var now = DateTime.now();
    // drawing
    for (var i = 0; i < 60; i++) {
      //draw the text
      String strFormatter =
          ('${((i ~/ 5 * 22 / 11)).toStringAsFixed(0)}' != "0")
              ? '${((i ~/ 5 * 22 / 11)).toStringAsFixed(0)}'
              : "24";

      tickMarkLength = hourTickMarkLength;
      tickPaint.strokeWidth = hourTickMarkWidth;
      // i % 5 == 0 ?  : minuteTickMarkWidth;

      final arrow_radius = 23;

      if ((now.hour % 2 == 1 &&
              int.parse((strFormatter == "24" ? "0" : strFormatter)) + 1 ==
                  now.hour) &&
          datetimeSelected != null) {
        if (now.minute > 30 && (i % 5 == 4)) {
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 + 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 - 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
        } else if (now.minute < 30 && (i % 5 == 3)) {
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 + 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 - 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
        }
      } else if ((now.hour.toString() == "0" ? "24" : now.hour.toString()) ==
              strFormatter &&
          datetimeSelected != null) {
        if (now.minute > 30 && (i % 5 == 2)) {
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 + 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 - 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
        } else if ((now.minute < 30 && now.minute >= 15 && (i % 5 == 1))) {
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 + 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 - 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
        } else if (now.minute < 15 && (i % 5 == 0)) {
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 + 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
          canvas.drawLine(
              new Offset(0.0, -radius + arrow_radius),
              new Offset(0.0 - 5, -radius + arrow_radius + tickMarkLength),
              tickPaint);
        }
      }

      // canvas.save();
      // canvas.translate(0.0,  -radius+ 90.0);
      // canvas.restore();

      if (i % 5 == 0) {
        canvas.save();
        canvas.translate(0.0, -radius + (datetimeSelected != null ? 39 : 39));
        textPainter.text = new TextSpan(
          // text: '${twentyfourhourADaay((i ~/ 5).toString())}',
          text: strFormatter,
          style: textStyle,
        );

        //helps make the text painted vertically
        canvas.rotate(-angle * i);

        textPainter.layout();

        textPainter.paint(canvas,
            new Offset(-(textPainter.width / 2), -(textPainter.height / 2)));

        canvas.restore();
      }
      // if(DateTime.now().hour.toString() == strFormatter){
      //     Path path2 = Path()
      //         ..arcTo(rect, (i * math.pi / 180) - math.pi / 2.5, 0.15, false);
      //     canvas.drawPath(path2, Paint()
      //             ..color = Colors.red
      //             // ..color = colorrrrr[(((value*3)/maxADay)*totalRange[count]).round()-1 < 0 ? 0 : ((value*3)/maxADay).round()-1].withOpacity(totalRange[count])
      //             ..strokeWidth = circleSize
      //             ..style = PaintingStyle.stroke);
      //     canvas.restore();
      //   }

      canvas.rotate(angle);
    }

    canvas.restore();
  }
}

getColorIndex(compereValue, max) {
  int colorSelectorIndex = ((compereValue * 7) / max).round() <= 0
      ? 0
      : ((compereValue * 7) / max).round() - 1;

  return colorSelectorIndex;
}

twentyfourhourADaay(String tickAnalohNum) {
  if (tickAnalohNum == "0") {
    return "24";
  } else if (tickAnalohNum == "3") {
    return "6";
  } else if (tickAnalohNum == "6") {
    return "12";
  } else if (tickAnalohNum == "9") {
    return "18";
  }
  return "";
}
