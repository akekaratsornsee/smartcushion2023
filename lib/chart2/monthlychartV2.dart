import 'dart:ui';

import 'package:appmattress/chart2/util/monthlyV2.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/mock.dart';
import 'package:appmattress/util/mock copy.dart';
import 'package:appmattress/widget_elements/call_detail_circle.dart';
import 'package:appmattress/widget_elements/util/overtime_and_cancel_counting.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:flutter/cupertino.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:custom_cupertino_picker/custom_cupertino_picker.dart';

class MonthlyChartV2 extends StatefulWidget {
  Map data;
  Counting countingData;
  DateTimeCallback onDateTimeChange;

  MonthlyChartV2(data, countingData, {this.onDateTimeChange})
      : data = data,
        countingData = countingData;
  @override
  State<StatefulWidget> createState() => MonthlyChartV2State();
}

class MonthlyChartV2State extends State<MonthlyChartV2> {
  var myList;
  var myList2;
  var currPage = DateTime.now().month < 7 ? 0 : 1;
  var timeShow = DateTime.now();
  var myAvg = "";
  var overTime;
  var cancel;
  Counting countingdata;
  var myAvg1 = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    countingdata = widget.countingData;
    myInit(timeShow, currPage);
  }

  int _yearNum = DateTime.now().year;

  void _showyearNumPickerDialog() async {
    // <-- note the async keyword here

    // this will contain the result from Navigator.pop(context, result)
    final selectedyearNum = await showDialog<int>(
      context: context,
      builder: (context) => YearNumPickerDialog(initialyearNum: _yearNum),
    );

    // execution of this code continues when the dialog was closed (popped)

    // note that the result can also be null, so check it
    // (back button or pressed outside of the dialog)
    if (selectedyearNum != null) {
      setState(() {
        this.widget.onDateTimeChange(DateTime(selectedyearNum));
        _yearNum = selectedyearNum;
        timeShow = new DateTime(_yearNum, timeShow.month, timeShow.day);
        print("year change :: $_yearNum");
        currPage = 0;
        myInit(timeShow, currPage);
      });
    }
  }

  myInit(datetime, indexing) async {
    myList2 = await monthlyDataQueryV2(widget.data ?? {},
        fullDate: true, datetimeSelector: timeShow);

    await countingdata.init(DateTime(datetime.year, 1, 1),
        end: DateTime(datetime.year, 12, 31));

    setState(() {
      overTime = countingdata.overTimeCounting?.length;
      cancel = countingdata.cancelCounting?.length;
    });

    setState(() {
      myList = myList2["value"][indexing];
      double avg = 0;
      if (myList2 != null) {
        avg = myList2["json"]
                .entries
                .toList()
                .map((m) => m.value)
                .reduce((a, b) => a + b) /
            myList2["json"].entries.toList().length;
      }

      myAvg = ((avg * setting["config"]["multiply_record_in_secs"] / 60) * 12)
          .toStringAsFixed(0);
    });
  }

  changePage(int pageChanging) async {
    currPage = currPage + pageChanging;
    if (currPage < 0) {
      timeShow = DateTime(timeShow.year - 1, timeShow.month, timeShow.day);
      currPage = 1;

      myInit(timeShow, currPage);
    }
    if (currPage > 1) {
      timeShow = DateTime(timeShow.year + 1, timeShow.month, timeShow.day);
      currPage = 0;

      myInit(timeShow, currPage);
    }
    setState(() {
      myList = myList2["value"][currPage];
    });
  }

  final controller = new PageController(initialPage: 999);
  int prevPage = 999;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.only(
          left: colorSkyTheme["container_chart_padding"],
          right: colorSkyTheme["container_chart_padding"],
          bottom: colorSkyTheme["container_chart_padding"],
          top: 0),
      child: AspectRatio(
        aspectRatio: 1.7,
        child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                  colorSkyTheme["container_chart_BorderRadius"])),
          color: Color(colorSkyTheme["chart_backgroud"]),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.all(
                    colorSkyTheme["container_chart_padding_inner"]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 7,
                              child: Text(
                                'เวลานั่งรายปี',
                                style: TextStyle(
                                    height: 0.9,
                                    color: Color(
                                        colorSkyTheme["text_subhead_acci"]),
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                  primary: Colors.blue,
                                ),
                                onPressed: () {
                                  _showyearNumPickerDialog();
                                },
                                child: Row(
                                  children: [
                                    Text(
                                      'เลือกปี',
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          .apply(
                                              color: Color(
                                                  colorSkyTheme["text_body"]),
                                              fontFamily: "Athiti",
                                              fontWeightDelta: 2),
                                    ),
                                    Expanded(
                                        child: Icon(
                                      Icons.arrow_drop_down,
                                      color: Color(colorSkyTheme["text_body"]),
                                    ))
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: AutoSizeText(
                            '''ปี ${DateFormat('yyyy').format(timeShow)}''',
                            style: Theme.of(context).textTheme.subtitle1.apply(
                                color: Color(colorSkyTheme["text_body"]),
                                fontFamily: "Athiti",
                                fontWeightDelta: 2,
                                fontSizeFactor: 1.1),
                          ),
                        ),
//                         Padding(
//                             padding: const EdgeInsets.only(left:  8.0),
//                             child: AutoSizeText("นั่งเกิน 0 ครั้ง และยกเลิกแจ้งเตือน 0ครั้ง",
//                               style: TextStyle(
//                                   height: 1.2,
//                                   color: Color(colorSkyTheme["text_body"]),
//                                   fontSize: 18,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    detailCircleWidgetSlot(width, height, myAvg, "นาที/ปี",
                        overTime ?? "", cancel ?? ""),
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                        child: PageView.builder(
                      onPageChanged: (i) {
                        if (i > prevPage) {
                          print("PAGE RIGHT :: $i");
                          Future.delayed(const Duration(milliseconds: 500), () {
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              changePage(-1);
                            });
                          });

                          prevPage = i;
                        } else if (i < prevPage) {
                          print("PAGE LEFT :: $i");
                          Future.delayed(const Duration(milliseconds: 500), () {
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              changePage(1);
                            });
                          });

                          prevPage = i;
                        }
                      },
                      controller: controller,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      // itemCount: 2,
                      itemBuilder: (context, i) {
                        return Container(
                          width: width,
                          height: width * 0.6,
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 15.0,
                                    right: 15.0,
                                    top: 20,
                                    bottom: 5),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 1.0),
                                  child: BarChart(
                                    BarChartData(
                                        alignment:
                                            BarChartAlignment.spaceAround,
                                        // maxY: 100,
                                        barTouchData: BarTouchData(
                                          enabled: false,
                                          touchTooltipData: BarTouchTooltipData(
                                            tooltipBgColor: Colors.transparent,
                                            tooltipPadding:
                                                const EdgeInsets.only(top: 0),
                                            tooltipMargin: -5,
                                            getTooltipItem: (
                                              BarChartGroupData group,
                                              int groupIndex,
                                              BarChartRodData rod,
                                              int rodIndex,
                                            ) {
                                              return BarTooltipItem(
                                                rod.y
                                                        .toStringAsFixed(0)
                                                        .toString() +
                                                    "น",
                                                TextStyle(
                                                  color: Color(colorSkyTheme[
                                                      "text_body"]),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                        titlesData: FlTitlesData(
                                          show: true,
                                          bottomTitles: SideTitles(
                                            rotateAngle: 340.0,
                                            showTitles: true,
                                            getTextStyles: (value) => TextStyle(
                                                color: (value.toInt() == 999)
                                                    ? Color(colorSkyTheme[
                                                        "chart_text_now"])
                                                    : Color(colorSkyTheme[
                                                        "chart_text"]),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14),
                                            margin: 10,
                                            getTitles: (double value) {
                                              switch (value.toInt()) {
                                                case 1:
                                                  return 'ม.ค.';
                                                case 2:
                                                  return 'ก.พ.';
                                                case 3:
                                                  return 'มี.ค.';
                                                case 4:
                                                  return 'เม.ย.';
                                                case 5:
                                                  return 'พ.ค.';
                                                case 6:
                                                  return 'มิ.ย.';
                                                case 7:
                                                  return 'ก.ค.';
                                                case 8:
                                                  return 'ส.ค.';
                                                case 9:
                                                  return 'ก.ย.';
                                                case 10:
                                                  return 'ต.ค.';
                                                case 11:
                                                  return 'พ.ย.';
                                                case 12:
                                                  return 'ธ.ค.';
                                                case 999:
                                                  return 'เดือนนี้';
                                                default:
                                                  return '';
                                              }
                                            },
                                          ),
                                          leftTitles:
                                              SideTitles(showTitles: false),
                                        ),
                                        borderData: FlBorderData(
                                          show: false,
                                        ),
                                        barGroups: myList),
                                    swapAnimationDuration:
                                        const Duration(milliseconds: 300),
                                    swapAnimationCurve: Curves.elasticOut,
                                  ),
                                ),
                              ),
                              // Align(
                              //     alignment: Alignment.centerLeft,
                              //     child: IconButton(
                              //         onPressed: () => changePage(-1),
                              //         icon: Icon(
                              //           Icons.arrow_left,
                              //           color:
                              //               Color(colorSkyTheme["text_body"]),
                              //           size: 35,
                              //         ))),
                              // Align(
                              //     alignment: Alignment.centerRight,
                              //     child: IconButton(
                              //         onPressed: () => changePage(1),
                              //         icon: Icon(
                              //           Icons.arrow_right,
                              //           color:
                              //               Color(colorSkyTheme["text_body"]),
                              //           size: 35,
                              //         )))
                            ],
                          ),
                        );
                      },
                    )),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// move the dialog into it's own stateful widget.
// It's completely independent from your page
// this is good practice
class YearNumPickerDialog extends StatefulWidget {
  /// initial selection for the slider
  final int initialyearNum;

  const YearNumPickerDialog({Key key, this.initialyearNum}) : super(key: key);

  @override
  _YearNumPickerDialogState createState() => _YearNumPickerDialogState();
}

class _YearNumPickerDialogState extends State<YearNumPickerDialog> {
  /// current selection of the slider
  int _yearNum;
  List yearGen = List.generate(5, (index) => DateTime.now().year - index);

  @override
  void initState() {
    super.initState();
    _yearNum = widget.initialyearNum;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text('เลือกปี'),
      content: Container(
        height: height * 0.25,
        width: width * 0.6,
        child: Column(
          children: [
            Expanded(
              child: CustomCupertinoPicker(
                scrollPhysics: const FixedExtentScrollPhysics(
                  parent: BouncingScrollPhysics(),
                ),
                scrollController: new FixedExtentScrollController(
                    initialItem: yearGen.indexOf(_yearNum)),
                itemExtent: 32,
                useMagnifier: true,
                magnification: 1.2,
                backgroundColor: Colors.white,
                onSelectedItemChanged: (int index) {
                  setState(() {
                    _yearNum = yearGen[index];
                  });
                },
                children: yearGen
                    .map((e) => Center(
                          child: Transform.scale(
                            scale: 1.3,
                            child: Text(e.toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .apply()),
                          ),
                        ))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            // Use the second argument of Navigator.pop(...) to pass
            // back a result to the page that opened the dialog
            Navigator.pop(context, _yearNum);
          },
          child: Text('เลือก'),
        )
      ],
    );
  }
}

typedef DateTimeCallback = void Function(DateTime onDateTimeChange);
