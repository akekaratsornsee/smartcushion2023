import 'package:appmattress/chart2/util/weely2.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/mock.dart';
import 'package:appmattress/util/mock copy.dart';
import 'package:appmattress/widget_elements/call_detail_circle.dart';
import 'package:appmattress/widget_elements/util/overtime_and_cancel_counting.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:flutter/cupertino.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BarChartSample3 extends StatefulWidget {
  Map data;
  Counting countingData;
  BarChartSample3(data, countingData, {this.onDateTimeChange})
      : data = data,
        countingData = countingData;
  DateTimeCallback onDateTimeChange;

  @override
  State<StatefulWidget> createState() => BarChartSample3State();
}

class BarChartSample3State extends State<BarChartSample3> {
  var myList;
  var myAvg = "";
  var myList2;
  var currPage = DateTime.now().month < 7 ? 0 : 1;
  var timeShow = DateTime.now();

  var overTime;
  var cancel;
  Counting countingdata;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    countingdata = widget.countingData;
    myInit(timeShow);
  }

  disPlayRange() {
    DateTime selector = timeShow;
    DateTime end = dateWeeluFormatter(selector);

    DateTime start = end.subtract(Duration(days: 6));
    var temp;
    if ((start.year == end.year) &&
        (start.month == end.month) &&
        (start.day != end.day)) {
      temp = '${start.day} - ${end.day} ${DateFormat('MMMM yyyy').format(end)}';
    } else if ((start.year == end.year) && (start.month != end.month)) {
      temp =
          '${start.day} ${DateFormat('MMMM').format(start)} - ${end.day} ${DateFormat('MMMM yyyy').format(end)}';
    } else {
      temp =
          '${start.day} ${DateFormat('MMMM yyyy').format(start)} - ${end.day} ${DateFormat('MMMM yyyy').format(end)}';
    }
    return temp;
  }

  myInit(DateTime datetime) async {
    var temp = await weeklyDataQueryV2(widget.data ?? {}, datetime);
    var reformat = dateWeeluFormatter(datetime);
    await countingdata.init(reformat.subtract(Duration(days: 6)),
        end: reformat);

    setState(() {
      overTime = countingdata.overTimeCounting?.length;
      cancel = countingdata.cancelCounting?.length;
    });

    setState(() {
      myList2 = temp;
      myList = myList2["value"];
      double avg = 0;
      if (myList != null) {
        avg = myList2["json"]
                .entries
                .toList()
                .map((m) => double.parse(m.value["value"]))
                .reduce((a, b) => a + b) /
            myList2["json"].entries.toList().length;
      }
      myAvg = ((avg * setting["config"]["multiply_record_in_secs"] / 60) * 7)
          .toStringAsFixed(0);
    });
  }

  // DateTime dateWeeluFormatter(selectorDate) {
  //   DateTime firstWekkDay;
  //   if (selectorDate.weekday != 6) {
  //     firstWekkDay = selectorDate.add(Duration(days: 6 - selectorDate.weekday));
  //     if (selectorDate.weekday == 7) {
  //       firstWekkDay = selectorDate.add(Duration(days: 6));
  //     }
  //   } else {
  //     firstWekkDay = selectorDate;
  //   }
  //   return firstWekkDay;
  // }
  changePage(int pageChanging) async {
    if (pageChanging == -1) {
      setState(() {
        timeShow = DateTime(timeShow.year, timeShow.month,
            timeShow.day - (timeShow.weekday == 7 ? 0 : timeShow.weekday) - 1);
      });

      myInit(timeShow);
    }
    if (pageChanging == 1) {
      setState(() {
        timeShow = DateTime(
            timeShow.year,
            timeShow.month,
            timeShow.day +
                (7 - (timeShow.weekday == 7 ? 0 : timeShow.weekday)) +
                6);
      });

      myInit(timeShow);
    }
    // setState(() {
    //   myList = myList2["value"];
    // });
  }

  final controller = new PageController(initialPage: 999);
  int prevPage = 999;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.only(
          left: colorSkyTheme["container_chart_padding"],
          right: colorSkyTheme["container_chart_padding"],
          bottom: colorSkyTheme["container_chart_padding"],
          top: 0),
      child: AspectRatio(
        aspectRatio: 1.7,
        child: Card(
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          color: Color(colorSkyTheme["chart_backgroud"]),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.all(
                    colorSkyTheme["container_chart_padding_inner"]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 7,
                              child: AutoSizeText(
                                'เวลานั่งรายสัปดาห์',
                                maxLines: 1,
                                style: TextStyle(
                                    height: 0.9,
                                    color: Color(
                                        colorSkyTheme["text_subhead_acci"]),
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                  primary: Colors.blue,
                                ),
                                onPressed: () {
                                  showDatePicker(
                                    cancelText: "ยกเลิก",
                                    confirmText: "ตกลง",
                                    context: context,
                                    initialDate: timeShow == null
                                        ? DateTime.now()
                                        : timeShow,
                                    firstDate:
                                        DateTime(DateTime.now().year - 5),
                                    lastDate: DateTime(DateTime.now().year + 5),
                                  ).then((date) {
                                    setState(() {
                                      // pickedDate = date;
                                      if (date != null) {
                                        setState(() {
                                          try {
                                            timeShow = date;
                                            currPage = 0;
                                            myInit(timeShow);
                                          } on Exception catch (e) {
                                            timeShow = DateTime.now();
                                            currPage = 0;
                                            myInit(timeShow);
                                          }
                                          this.widget.onDateTimeChange(date);
                                        });
                                      } else {
                                        setState(() {});
                                      }
                                    });
                                  });
                                },
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 7,
                                        child: AutoSizeText(
                                          'เลือกสัปดาห์',
                                          maxLines: 1,
                                          minFontSize: 10,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1
                                              .apply(
                                                  color: Color(colorSkyTheme[
                                                      "text_body"]),
                                                  fontFamily: "Athiti",
                                                  fontWeightDelta: 2),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.arrow_drop_down,
                                          color:
                                              Color(colorSkyTheme["text_body"]),
                                        ))
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        // const SizedBox(
                        //   height: 4,
                        // ),
                        // Padding(
                        //   padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        //   child: Text(
                        //     'ค่าเฉลี่ยรายสัปดาห์ $myAvg นาที',
                        //     style: TextStyle(
                        //         height: 0.85,
                        //         color: Color(colorSkyTheme["text_body"]),
                        //         fontSize: 18,
                        //         fontWeight: FontWeight.bold),
                        //   ),
                        // ),
                        // Padding(
                        //     padding: const EdgeInsets.only(left:  8.0),
                        //     child: AutoSizeText("นั่งเกิน 0 ครั้ง และยกเลิกแจ้งเตือน 0ครั้ง",
                        //       style: TextStyle(
                        //           height: 1.2,
                        //           color: Color(colorSkyTheme["text_body"]),
                        //           fontSize: 18,
                        //           fontWeight: FontWeight.bold),
                        //     ),
                        //   ),

                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: AutoSizeText(
                            disPlayRange(),
                            style: Theme.of(context).textTheme.subtitle1.apply(
                                color: Color(colorSkyTheme["text_body"]),
                                fontFamily: "Athiti",
                                fontWeightDelta: 2,
                                fontSizeFactor: 1.1),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        detailCircleWidgetSlot(width, height, myAvg,
                            "นาที/สัปดาห์", overTime ?? "", cancel ?? ""),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                        child: PageView.builder(
                      onPageChanged: (i) {
                        if (i > prevPage) {
                          print("PAGE RIGHT :: $i");

                          WidgetsBinding.instance
                              .addPostFrameCallback((_) => setState(() {
                                    changePage(-1);
                                  }));
                          prevPage = i;
                        } else if (i < prevPage) {
                          print("PAGE LEFT :: $i");
                          WidgetsBinding.instance
                              .addPostFrameCallback((_) => setState(() {
                                    changePage(1);
                                  }));
                          prevPage = i;
                        }
                      },
                      controller: controller,
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      // itemCount: 2,
                      itemBuilder: (context, i) {
                        return Container(
                          width: width,
                          height: width * 0.6,
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0,
                                    right: 20.0,
                                    top: 20,
                                    bottom: 5),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 1.0),
                                  child: BarChart(
                                    BarChartData(
                                        // alignment: BarChartAlignment.spaceAround,
                                        // maxY: 100,
                                        barTouchData: BarTouchData(
                                          enabled: false,
                                          touchTooltipData: BarTouchTooltipData(
                                            tooltipBgColor: Colors.transparent,
                                            tooltipPadding:
                                                const EdgeInsets.only(
                                                    bottom: 0),
                                            tooltipMargin: -5,
                                            getTooltipItem: (
                                              BarChartGroupData group,
                                              int groupIndex,
                                              BarChartRodData rod,
                                              int rodIndex,
                                            ) {
                                              return BarTooltipItem(
                                                rod.y
                                                        .toStringAsFixed(0)
                                                        .toString() +
                                                    "น",
                                                TextStyle(
                                                  color: Color(colorSkyTheme[
                                                      "text_body"]),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                        titlesData: FlTitlesData(
                                          show: true,
                                          bottomTitles: SideTitles(
                                            rotateAngle: 340.0,
                                            showTitles: true,
                                            getTextStyles: (value) => TextStyle(
                                                color: (value.toInt() == 999)
                                                    ? Color(colorSkyTheme[
                                                        "chart_text_now"])
                                                    : Color(colorSkyTheme[
                                                        "chart_text"]),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                            margin: 10,
                                            getTitles: (double value) {
                                              switch (value.toInt()) {
                                                case 0:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["0"]["fulldate"])).toString() : ""}';
                                                case 1:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["1"]["fulldate"])).toString() : ""}';
                                                case 2:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["2"]["fulldate"])).toString() : ""}';
                                                case 3:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["3"]["fulldate"])).toString() : ""}';
                                                case 4:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["4"]["fulldate"])).toString() : ""}';
                                                case 5:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["5"]["fulldate"])).toString() : ""}';
                                                case 6:
                                                  return '${(myList2 != null) ? DateFormat.E('th').format(DateTime.parse(myList2["json"]["6"]["fulldate"])).toString() : ""}';
                                                case 999:
                                                  return 'วันนี้';
                                                default:
                                                  return '';
                                              }
                                            },
                                          ),
                                          leftTitles:
                                              SideTitles(showTitles: false),
                                        ),
                                        borderData: FlBorderData(
                                          show: false,
                                        ),
                                        barGroups: myList),
                                    swapAnimationDuration:
                                        const Duration(milliseconds: 500),
                                    swapAnimationCurve: Curves.elasticOut,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    )),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

typedef DateTimeCallback = void Function(DateTime onDateTimeChange);
