import 'dart:collection';

import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/dairy.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

DateTime dateWeeluFormatter(selectorDate) {
  DateTime firstWekkDay;
  if (selectorDate.weekday != 6) {
    firstWekkDay = selectorDate.add(Duration(days: 6 - selectorDate.weekday));
    if (selectorDate.weekday == 7) {
      firstWekkDay = selectorDate.add(Duration(days: 6));
    }
  } else {
    firstWekkDay = selectorDate;
  }
  return firstWekkDay;
}

weeklyDataQueryV2(Map data, DateTime starterDay, {DateTime endDay}) async {
  int nLoop = 0;
  if (endDay == null) {
    nLoop = 7;
  } else {
    nLoop = starterDay.difference(endDay).inDays;
  }
  Intl.defaultLocale = "th";
  var temp = dateWeeluFormatter(starterDay);
  SplayTreeMap<String, Map<String, String>> myResult =
      SplayTreeMap<String, Map<String, String>>();
  var isComplete = false;
  int max = 10;
  List<BarChartGroupData> myList = new List<BarChartGroupData>();
  for (var i = 0; i < nLoop; i++) {
    print("my weeek day:: ${temp.weekday}");
    int sum = (await weeklyDataQueryHelper(temp, data)).toInt();
    if (i == 0) {
      myResult[temp.weekday.toString() == "7" ? "0" : temp.weekday.toString()] =
          {"value": sum.toString(), "fulldate": temp.toString()};
      max = (max > sum) ? max : sum;
      temp = temp.subtract(Duration(days: 1));
      continue;
    }

    max = (max > sum) ? max : sum;

    if (isComplete) {
      myResult[temp.weekday.toString() == "7" ? "0" : temp.weekday.toString()] =
          {"value": "0", "fulldate": temp.toString()};
    } else {
      myResult[temp.weekday.toString() == "7" ? "0" : temp.weekday.toString()] =
          {"value": sum.toString(), "fulldate": temp.toString()};
    }
    if (temp.weekday == 7) {
      print(temp.weekday.toString() == "7" ? "0" : temp.weekday.toString());
      isComplete = true;
    }
    temp = temp.subtract(Duration(days: 1));
  }
  var now = DateTime.now();
  myResult.forEach((key, value) {
    var comparer = DateTime(
        DateTime.parse(value["fulldate"]).year,
        DateTime.parse(value["fulldate"]).month,
        DateTime.parse(value["fulldate"]).day);

    myList.add(BarChartGroupData(
      x: (comparer.difference(DateTime(now.year, now.month, now.day)).inDays ==
              0)
          ? 999
          : int.parse(key),
      barRods: [
        BarChartRodData(
            backDrawRodData: BackgroundBarChartRodData(
                y: max.toDouble() *
                    setting["config"]["multiply_record_in_secs"] /
                    60,
                show: true,
                colors: (comparer
                            .difference(DateTime(now.year, now.month, now.day))
                            .inDays ==
                        0)
                    ? [
                        Color(colorSkyTheme["chart_bottom_disable"]),
                        Color(colorSkyTheme["chart_bottom_disable"])
                      ]
                    : [
                        Color(colorSkyTheme["chart_bottom_disable"]),
                        Color(colorSkyTheme["chart_bottom_disable"])
                      ]),
            y: double.parse(value["value"]) *
                setting["config"]["multiply_record_in_secs"] /
                60,
            colors: (comparer
                        .difference(DateTime(now.year, now.month, now.day))
                        .inDays ==
                    0)
                ? [
                    Color(colorSkyTheme["chart_bottom"]),
                    Color(colorSkyTheme["chart_top"])
                  ]
                : [
                    Color(colorSkyTheme["chart_bottom"]),
                    Color(colorSkyTheme["chart_top"])
                  ],
            width: 25)
      ],
      showingTooltipIndicators: double.parse(value["value"]) == 0.0 ? [1] : [0],
    ));
  });

  return {"value": myList, "json": myResult};
}

weeklyDataQueryHelper(DateTime toDay, Map data) async {
  var dateQueryFormat = DateFormat('yyyyMMdd').format(toDay);
  var dataQuery = data["$dateQueryFormat"];
  print('my weekly data query :: $dateQueryFormat');
  print('my weekly data query >>> :: $dataQuery');

  if (dataQuery == null) return 0;
  var countingMap = dairyDataQuery(data,
      fullDate: true, datetimeSelector: toDay, weeklycall: true);
  print('my weekly data countingMap === :: $countingMap');

  if (countingMap == null) return 0;
  print('my weeky date helper :: $countingMap');
  var countingMapValue = countingMap.values;
  var result = await countingMapValue.reduce((sum, element) => sum + element);
  return result;
}
