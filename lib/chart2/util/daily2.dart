import 'dart:collection';

import 'package:appmattress/util/config.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

dairyDataQueryV2(Map data,
    {bool fullDate = false,
    DateTime datetimeSelector,
    bool isChartcalled = true}) {
  // Map<String, double> minitesMap = {
  //   "0-15": 0.0,
  //   "15-30": 0.0,
  //   "30-45": 0.0,
  //   "45-59": 0.0
  // };
  if (fullDate && datetimeSelector == null)
    throw ("plz fill the para 'DateTime datetimeSelector'");

  var dateQueryFormat =
      DateFormat('yyyyMMdd').format(datetimeSelector).toString();
  // print("my dairy queryy $dateQueryFormat");
  var values;
  SplayTreeMap<String, double> temp = SplayTreeMap<String, double>();
  SplayTreeMap<String, Map<String, double>> tempSubMinuiteCount =
      SplayTreeMap<String, Map<String, double>>();

  if (data != null) {
    values = data["$dateQueryFormat"];
    var tempTime = fullDate ? "23" : datetimeSelector.hour.toString();
    var subter = 0;
    // print("now hour:: $tempTime");
    for (var i = 0; i < 24; i++) {
      var newTempTime = (int.parse(tempTime) - subter).toString();
      var hour = newTempTime.length == 1 ? "0" + newTempTime : newTempTime;
      temp[hour] = 0.0;
      tempSubMinuiteCount[hour] = {
        "0-15": 0.0,
        "15-30": 0.0,
        "30-45": 0.0,
        "45-59": 0.0
      };
      subter += 1;
    }
  }

  if (values != null) {
    values.forEach((key, value) {
      var tHour = key.toString().substring(0, 2);
      var hour =
          tHour.substring(0, 1) == "0" ? "0" + tHour.substring(1, 2) : tHour;
      if (temp[hour] != null) {
        temp[hour] = temp[hour] + 1.0;
        //   int tMinites = int.tryParse(key.toString().substring(2, 4));
        //   if (tMinites >= 0 && tMinites <= 15) {
        //     tempSubMinuiteCount[hour]["0-15"] += 1.0;
        //   } else if (tMinites > 15 && tMinites <= 30) {
        //     tempSubMinuiteCount[hour]["15-30"] += 1.0;
        //   } else if (tMinites > 30 && tMinites <= 45) {
        //     tempSubMinuiteCount[hour]["30-45"] += 1.0;
        //   } else if (tMinites > 45 && tMinites <= 59) {
        //     tempSubMinuiteCount[hour]["45-59"] += 1.0;
        //   }
      }
    });
  }
  SplayTreeMap<String, Map<String, double>> dataxxxxxxx;
  if (values != null) {
    dataxxxxxxx = countRangeDataV2withSubMinute(tempSubMinuiteCount, values);
  }

  List<BarChartGroupData> firstHalf = [];
  List<BarChartGroupData> lastHalf = [];
  double mymax = 0;

  // print("final  daily query data :: $temp");
  // print(
  //     "final sub-minute daily query :: ${tempSubMinuiteCount["00"]} ${datetimeSelector}");

  if (!isChartcalled) return {"json": temp, "jsonSubMinitesCount": dataxxxxxxx};
  var tempFindMax = temp.entries.toList();
  if (tempFindMax != null && tempFindMax.isNotEmpty) {
    tempFindMax.sort((a, b) => a.value.compareTo(b.value));
    print("MY MAXXXXXXXXXXXx :: ${tempFindMax.last.value}");
    mymax = tempFindMax.last.value * 10;
  }
  temp.forEach((key, value) {
    DateTime now = DateTime.now();

    var tempData = BarChartGroupData(
      x: (now.hour == int.parse(key))
          ? ((calculateDifference(datetimeSelector) == 0)
              ? 999
              : int.parse(key))
          : int.parse(key),
      barRods: [
        BarChartRodData(
            backDrawRodData: BackgroundBarChartRodData(
                y: tempFindMax.last.value == 0
                    ? 20
                    : tempFindMax.last.value *
                        setting["config"]["multiply_record_in_secs"] /
                        60,
                show: true,
                colors: (now.hour == int.parse(key))
                    ? ((calculateDifference(datetimeSelector) == 0)
                        ? [
                            Color(colorSkyTheme["chart_bottom_disable"]),
                            Color(colorSkyTheme["chart_bottom_disable"])
                          ]
                        : [
                            Color(colorSkyTheme["chart_bottom_disable"]),
                            Color(colorSkyTheme["chart_bottom_disable"])
                          ])
                    : [
                        Color(colorSkyTheme["chart_bottom_disable"]),
                        Color(colorSkyTheme["chart_bottom_disable"])
                      ]),
            y: (value * setting["config"]["multiply_record_in_secs"] / 60),
            colors: (now.hour == int.parse(key))
                ? ((calculateDifference(datetimeSelector) == 0)
                    ? [
                        Color(colorSkyTheme["chart_bottom"]),
                        Color(colorSkyTheme["chart_top"])
                      ]
                    : [
                        Color(colorSkyTheme["chart_bottom"]),
                        Color(colorSkyTheme["chart_top"])
                      ])
                : [
                    Color(colorSkyTheme["chart_bottom"]),
                    Color(colorSkyTheme["chart_top"])
                  ],
            width: 15)
      ],
      showingTooltipIndicators: value == 0.0 ? [1] : [0],
    );

    if (int.parse(key) < 12) {
      firstHalf.add(tempData);
    } else {
      lastHalf.add(tempData);
    }
  });

  return {
    "value": [firstHalf, lastHalf],
    "json": temp,
    "jsonSubMinitesCount": tempSubMinuiteCount
  };
}

countRangeDataV2withSubMinute(
    SplayTreeMap<String, Map<String, double>> prototype, data) {
  SplayTreeMap<String, Map<String, double>> collectedData = prototype;

  data?.forEach((key, value) {
    if (value != null) {
      int tMinites = int.tryParse(key.toString().substring(2, 4));
      var tHour = key.toString().substring(0, 2);
      var hour =
          tHour.substring(0, 1) == "0" ? "0" + tHour.substring(1, 2) : tHour;
      if (tMinites >= 0 && tMinites <= 15) {
        collectedData[tHour]["0-15"] += 1.0;
      } else if (tMinites > 15 && tMinites <= 30) {
        collectedData[tHour]["15-30"] += 1.0;
      } else if (tMinites > 30 && tMinites <= 45) {
        collectedData[tHour]["30-45"] += 1.0;
      } else if (tMinites > 45 && tMinites <= 59) {
        collectedData[tHour]["45-59"] += 1.0;
      }
    }
  });
  return collectedData;
}

int calculateDifference(DateTime date) {
  DateTime now = DateTime.now();
  return DateTime(date.year, date.month, date.day)
      .difference(DateTime(now.year, now.month, now.day))
      .inDays;
}
