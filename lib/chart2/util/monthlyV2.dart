import 'dart:collection';

import 'package:appmattress/chart2/util/daily2.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/monthly.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

monthlyDataQueryV2(Map data,
    {bool fullDate = false, DateTime datetimeSelector}) {
  if (fullDate && datetimeSelector == null)
    throw ("plz fill the para 'DateTime datetimeSelector'");

  var dateQueryFormat =
      DateFormat('yyyyMMdd').format(datetimeSelector).toString();
  // print("my dairy queryy $dateQueryFormat");
  // var values = data["$dateQueryFormat"];

  List<DataFormatter> tempDateList = new List<DataFormatter>();
  List keys = data.keys.toList();
  for (var i = 0; i < keys.length; i++) {
    var ok = DataFormatter(keys[i].toString(), data[keys[i]]);
    tempDateList.add(ok);
  }

  SplayTreeMap<String, double> temp = SplayTreeMap<String, double>();
  var tempTime = fullDate ? "12" : datetimeSelector.hour.toString();
  var subter = 0;
  // print("now hour:: $tempTime");
  for (var i = 1; i < 13; i++) {
    var newTempTime = (int.parse(tempTime) - subter).toString();
    var hour = newTempTime.length == 1 ? "0" + newTempTime : newTempTime;
    temp[hour] = 0.0;
    subter += 1;
  }
  tempDateList.forEach((element) {
    var year = element.datetime.substring(0, 4);

    var month = element.datetime.substring(4, 6);
    var fMonth =
        month.substring(0, 1) == "0" ? "0" + month.substring(1, 2) : month;
    if (temp[fMonth] != null && year == datetimeSelector.year.toString()) {
      temp[fMonth] =
          temp[fMonth] + double.parse(element.value.length.toString());
    }
  });

  List<BarChartGroupData> firstHalf = [];
  List<BarChartGroupData> lastHalf = [];
  print("final walk monthly :: $temp");
  var tempFindMax = temp.entries.toList();
  if (tempFindMax != null && tempFindMax.isNotEmpty) {
    tempFindMax.sort((a, b) => a.value.compareTo(b.value));
    print("MY MAXXXXXXXXXXX :: ${tempFindMax.last.value}");
  }
  temp.forEach((key, value) {
    DateTime now = DateTime.now();

    var tempData = BarChartGroupData(
      x: (now.month == int.parse(key))
          ? ((calculateDifference(datetimeSelector) == 0)
              ? 999
              : int.parse(key))
          : int.parse(key),
      barRods: [
        BarChartRodData(
            backDrawRodData: BackgroundBarChartRodData(
                y: tempFindMax.last.value == 0
                    ? 20
                    : tempFindMax.last.value *
                        setting["config"]["multiply_record_in_secs"] /
                        60,
                show: true,
                colors: (now.month == int.parse(key))
                    ? ((calculateDifference(datetimeSelector) == 0)
                        ? [
                            Color(colorSkyTheme["chart_bottom_disable"]),
                            Color(colorSkyTheme["chart_bottom_disable"])
                          ]
                        : [
                            Color(colorSkyTheme["chart_bottom_disable"]),
                            Color(colorSkyTheme["chart_bottom_disable"])
                          ])
                    : [
                        Color(colorSkyTheme["chart_bottom_disable"]),
                        Color(colorSkyTheme["chart_bottom_disable"])
                      ]),
            y: value * setting["config"]["multiply_record_in_secs"] / 60,
            colors: (now.month == int.parse(key))
                ? ((calculateDifference(datetimeSelector) == 0)
                    ? [
                        Color(colorSkyTheme["chart_bottom"]),
                        Color(colorSkyTheme["chart_top"])
                      ]
                    : [
                        Color(colorSkyTheme["chart_bottom"]),
                        Color(colorSkyTheme["chart_top"])
                      ])
                : [
                    Color(colorSkyTheme["chart_bottom"]),
                    Color(colorSkyTheme["chart_top"])
                  ],
            width: 30)
      ],
      showingTooltipIndicators: value == 0.0 ? [1] : [0],
    );

    if (int.parse(key) < 7) {
      firstHalf.add(tempData);
    } else {
      lastHalf.add(tempData);
    }
  });

  return {
    "value": [firstHalf, lastHalf],
    "json": temp
  };
}
