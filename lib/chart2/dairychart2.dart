import 'package:appmattress/animation/listViewInitialAnimationVersion2.dart';
import 'package:appmattress/chart2/util/daily2.dart';
import 'package:appmattress/chart2/util/weely2.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/datePickerUtil.dart';
import 'package:appmattress/util/mock.dart';
import 'package:appmattress/util/mock copy.dart';
import 'package:appmattress/widget_elements/call_detail_circle.dart';
import 'package:appmattress/widget_elements/datail_card.dart';
import 'package:appmattress/widget_elements/util/overtime_and_cancel_counting.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:flutter/cupertino.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class DairyChartV2 extends StatefulWidget {
  Map data;
  Counting countingData;
  DateTimeCallback onDateTimeChange;

  DairyChartV2(data, countingData, {this.onDateTimeChange})
      : data = data,
        countingData = countingData;
  @override
  State<StatefulWidget> createState() => DairyChartV2State();
}

class DairyChartV2State extends State<DairyChartV2> {
  var myList;
  var myList2;
  var myAvg = "";
  var currPage = DateTime.now().hour < 12 ? 0 : 1;
  var timeShow = DateTime.now();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myInit(timeShow, currPage);
  }

  @override
  void dispose() {
    super.dispose();
    //  dairyCountingData = new Counting();
  }

  myInit(datetime, indexing) async {
    myList2 = await dairyDataQueryV2(widget.data ?? {},
        fullDate: true, datetimeSelector: datetime);
    // Counting refreshDataCounting =  new Counting();
    // await refreshDataCounting.init(datetime);
    // dairyCountingData = refreshDataCounting;

    setState(() {
      myList = myList2["value"][indexing];
      double avg = 0;
      if (myList != null) {
        avg = myList2["json"]
                .entries
                .toList()
                .map((m) => m.value)
                .reduce((a, b) => a + b) /
            myList2["json"].entries.toList().length;
      }

      myAvg = (avg * setting["config"]["multiply_record_in_secs"] / 60)
          .toStringAsFixed(0);
    });
  }

  changePage(int pageChanging) async {
    currPage = currPage + pageChanging;
    if (currPage < 0) {
      timeShow = timeShow.subtract(Duration(days: 1));

      setState(() {
        currPage = 1;
      });

      myInit(timeShow, currPage);
    }
    if (currPage > 1) {
      timeShow = timeShow.add(Duration(days: 1));
      setState(() {
        currPage = 0;
      });

      myInit(timeShow, currPage);
    }
    setState(() {
      myList = myList2["value"][currPage];
    });
  }

  final controller = new PageController(initialPage: 999);
  int prevPage = 999;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.only(
          left: colorSkyTheme["container_chart_padding"],
          right: colorSkyTheme["container_chart_padding"],
          bottom: colorSkyTheme["container_chart_padding"],
          top: 0),
      child: AspectRatio(
        aspectRatio: 1.7,
        child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                  colorSkyTheme["container_chart_BorderRadius"])),
          color: Color(colorSkyTheme["chart_backgroud"]),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.all(
                    colorSkyTheme["container_chart_padding_inner"]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 7,
                                child: Text(
                                  'เวลานั่งรายวัน',
                                  style: TextStyle(
                                      height: 0.9,
                                      color: Color(
                                          colorSkyTheme["text_subhead_acci"]),
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                    primary: Colors.blue,
                                  ),
                                  onPressed: () {
                                    showDatePicker(
                                      cancelText: "ยกเลิก",
                                      confirmText: "ตกลง",
                                      context: context,
                                      initialDate: timeShow == null
                                          ? DateTime.now()
                                          : timeShow,
                                      firstDate:
                                          DateTime(DateTime.now().year - 5),
                                      lastDate:
                                          DateTime(DateTime.now().year + 5),
                                    ).then((date) {
                                      setState(() {
                                        // pickedDate = date;
                                        if (date != null) {
                                          setState(() {
                                            try {
                                              timeShow = date;
                                              currPage = 0;
                                              myInit(timeShow, currPage);
                                            } on Exception catch (e) {
                                              timeShow = DateTime.now();
                                              currPage = 0;
                                              myInit(timeShow, currPage);
                                            }
                                            this.widget.onDateTimeChange(date);
                                          });
                                        } else {
                                          setState(() {});
                                        }
                                      });
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      Expanded(
                                          flex: 7,
                                          child: AutoSizeText(
                                            'เลือกวัน',
                                            maxLines: 1,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1
                                                .apply(
                                                    color: Color(colorSkyTheme[
                                                        "text_body"]),
                                                    fontFamily: "Athiti",
                                                    fontWeightDelta: 2),
                                          )),
                                      Expanded(
                                          flex: 1,
                                          child: Icon(
                                            Icons.arrow_drop_down,
                                            color: Color(
                                                colorSkyTheme["text_body"]),
                                          ))
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: AutoSizeText.rich(
                              TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                      text: DateFormat('EE dd MMMM yyyy')
                                          .format(timeShow),
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1
                                          .apply(
                                              color: Color(
                                                  colorSkyTheme["text_body"]),
                                              fontFamily: "Athiti",
                                              fontWeightDelta: 2,
                                              fontSizeFactor: 1.1)),
                                  TextSpan(
                                    text:
                                        "${currPage == 0 ? ' 0.00น-11.00น' : ' 12.00น-23.00น'}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle1
                                        .apply(
                                            color: Color(
                                                colorSkyTheme["text_body"]),
                                            fontFamily: "Athiti",
                                            fontWeightDelta: 2),
                                  )
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          detailCircleWidgetSlot(
                            width,
                            height,
                            myAvg,
                            "นาที/วัน",
                            widget.countingData?.overTimeCounting?.length,
                            widget.countingData?.cancelCounting?.length,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                        ]),
                    Expanded(
                        child: PageView.builder(
                      onPageChanged: (i) {
                        if (i > prevPage) {
                          print("PAGE RIGHT :: $i");

                          WidgetsBinding.instance
                              .addPostFrameCallback((_) => setState(() {
                                    changePage(-1);
                                  }));
                          prevPage = i;
                        } else if (i < prevPage) {
                          print("PAGE LEFT :: $i");
                          WidgetsBinding.instance
                              .addPostFrameCallback((_) => setState(() {
                                    changePage(1);
                                  }));
                          prevPage = i;
                        }
                      },
                      controller: controller,
                      reverse: true,
                      // scrollDirection: Axis.horizontal,
                      // itemCount: 2,
                      itemBuilder: (context, i) {
                        return Container(
                          width: width,
                          height: width * 0.6,
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 20, top: 20, bottom: 20),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 1.0,
                                  ),
                                  child: BarChart(
                                    BarChartData(
                                        // alignment: BarChartAlignment.spaceAround,
                                        // maxY: 100,
                                        barTouchData: BarTouchData(
                                          enabled: false,
                                          touchTooltipData: BarTouchTooltipData(
                                            tooltipBgColor: Colors.transparent,
                                            tooltipPadding:
                                                const EdgeInsets.only(top: 0),
                                            tooltipMargin: -5,
                                            getTooltipItem: (
                                              BarChartGroupData group,
                                              int groupIndex,
                                              BarChartRodData rod,
                                              int rodIndex,
                                            ) {
                                              return BarTooltipItem(
                                                rod.y
                                                        .toStringAsFixed(0)
                                                        .toString() +
                                                    "น",
                                                TextStyle(
                                                  fontSize: 13,
                                                  color: Color(colorSkyTheme[
                                                      "text_body"]),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                        titlesData: FlTitlesData(
                                          show: true,
                                          bottomTitles: SideTitles(
                                            rotateAngle: 290.0,
                                            showTitles: true,
                                            getTextStyles: (value) => TextStyle(
                                                color: (value.toInt() == 999)
                                                    ? Color(colorSkyTheme[
                                                        "chart_text_now"])
                                                    : Color(colorSkyTheme[
                                                        "chart_text"]),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13),
                                            margin: 5,
                                            getTitles: (double value) {
                                              switch (value.toInt()) {
                                                case 0:
                                                  return '0.00น';
                                                case 1:
                                                  return '1.00น';
                                                case 2:
                                                  return '2.00น';
                                                case 3:
                                                  return '3.00น';
                                                case 4:
                                                  return '4.00น';
                                                case 5:
                                                  return '5.00น';
                                                case 6:
                                                  return '6.00น';
                                                case 7:
                                                  return '7.00น';
                                                case 8:
                                                  return '8.00น';
                                                case 9:
                                                  return '9.00น';
                                                case 10:
                                                  return '10.00น';
                                                case 11:
                                                  return '11.00น';
                                                case 12:
                                                  return '12.00น';
                                                case 13:
                                                  return '13.00น';
                                                case 14:
                                                  return '14.00น';
                                                case 15:
                                                  return '15.00น';
                                                case 16:
                                                  return '16.00น';
                                                case 17:
                                                  return '17.00น';
                                                case 18:
                                                  return '18.00น';
                                                case 19:
                                                  return '19.00น';
                                                case 20:
                                                  return '20.00น';
                                                case 21:
                                                  return '21.00น';
                                                case 22:
                                                  return '22.00น';
                                                case 23:
                                                  return '23.00น';
                                                case 999:
                                                  return 'ชั่วโมงนี้';
                                                default:
                                                  return '';
                                              }
                                            },
                                          ),
                                          leftTitles:
                                              SideTitles(showTitles: false),
                                        ),
                                        borderData: FlBorderData(
                                          show: false,
                                        ),
                                        barGroups: myList),
                                    swapAnimationDuration:
                                        const Duration(milliseconds: 500),
                                    swapAnimationCurve: Curves.elasticOut,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    )),
                    const SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

typedef DateTimeCallback = void Function(DateTime onDateTimeChange);
