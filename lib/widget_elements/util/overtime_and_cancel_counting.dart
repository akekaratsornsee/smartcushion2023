import 'package:appmattress/navigationDrawer/expansionTile.dart';
import 'package:appmattress/util/read_write_txtfile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';

final RefConting11 = FirebaseDatabase.instance.reference();

callReadCC() async {
  String databaseSelector = await read("databaseName"); //"ice"
  return databaseSelector;
}

// class ContingRef {
//   var data;
//   init() async {
//     String databaseSelector = await read("databaseName"); //"ice"
//     var countingdatabaseConnectionx =
//         await RefConting11.child(databaseSelector).child("DATE").once();

//     data = countingdatabaseConnectionx;
//     // data = mock2;
//     // print("init data in main $data");
//   }

//   ContingRef(this.data);
// }

class Counting {
  List cancelCounting = [];
  List overTimeCounting = [];

  // String NameCall = callReadCC().toString();

  init(DateTime start, {DateTime end}) async {
    String databaseSelector = await read("databaseName");
    final countingdatabaseConnection = FirebaseDatabase.instance
        .reference()
        .child(databaseSelector)
        .child('Value')
        .child('Counting');

    Map allRowData;
    var startFormatter = DateFormat('yyyyMMdd').format(start);
    var endFormatter = end != null ? DateFormat('yyyyMMdd').format(end) : null;
    if (start != null && end != null) {
      DataSnapshot allRowDataConnect = await countingdatabaseConnection
          .orderByKey()
          .startAt("$startFormatter")
          .endAt("$endFormatter")
          .once();
      countingHelper(allRowDataConnect, isRange: true);
    } else {
      DataSnapshot allRowDataConnect =
          await countingdatabaseConnection.child(startFormatter).once();
      countingHelper(allRowDataConnect);
    }
    return this;
  }

  countingHelper(DataSnapshot allRowDataConnect, {bool isRange = false}) {
    //Range     :: parent => date_1, ..., date_n: | children => cancel and overtime records
    //Non-Range :: parent => cancel and overtime records
    if (isRange) {
      var result = countingRangeHelper(allRowDataConnect?.value);
      this.cancelCounting = result[0];
      this.overTimeCounting = result[1];
    } else {
      Map cancelMap = allRowDataConnect?.value != null
          ? allRowDataConnect?.value["CancelAlarm"]
          : null;
      Map overTimeMap = allRowDataConnect?.value != null
          ? allRowDataConnect?.value["OverTimeCount"]
          : null;
      var result = countingSigleHelper(cancelMap, overTimeMap);
      this.cancelCounting = result[0];
      this.overTimeCounting = result[1];
    }
  }

  countingRangeHelper(Map records) {
    List cancelList = [];
    List overTimeList = [];
    records?.forEach((key, value) {
      var tempResult =
          countingSigleHelper(value["CancelAlarm"], value["OverTimeCount"]);
      cancelList += tempResult[0];
      overTimeList += tempResult[1];
    });
    return [cancelList, overTimeList];
  }

  countingSigleHelper(Map cancelRecords, Map overTimeRecords) {
    List cancelList = [];
    List overTimeList = [];
    cancelRecords?.forEach((key, value) {
      cancelList.add(value);
    });
    overTimeRecords?.forEach((key, value) {
      overTimeList.add(value);
    });

    return [cancelList, overTimeList];
  }
}
