import 'package:appmattress/animation/listViewInitialAnimation.dart';
import 'package:appmattress/util/config.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

Widget detailCardItemIcon(path) => Padding(
    padding: const EdgeInsets.all(8.0),
    child: SvgPicture.asset(path, semanticsLabel: 'A red up arrow')

    // Image.asset(
    //   path,
    //   fit: BoxFit.fill,
    // ),
    );
Widget detailCardItemText(context, data, unit) => Column(
      children: [
        Expanded(
            flex: 7,
            child: Transform.translate(
              offset: Offset(0, 3),
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: AutoSizeText(
                  "$data",
                  style: Theme.of(context).textTheme.headline2.apply(
                      color: Color(colorSkyTheme["text_health_card"]),
                      fontFamily: "Athiti",
                      fontWeightDelta: 3),
                ),
              ),
            )),
        Expanded(
          flex: 3,
          child: Transform.translate(
            offset: Offset(0, -10),
            child: Text("$unit",
                style: TextStyle(
                    color: Color(colorSkyTheme["text_health_card"]),
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Athiti")),
          ),
        )
      ],
    );

Widget detailCardItembody(context, colorr, up, down) => Text(
      '''
    $up
  $down''',
      style: Theme.of(context)
          .textTheme
          .bodyText1
          .apply(color: colorr, fontFamily: "Athiti", fontSizeDelta: 2),
    );

Widget detailCardItembodyV2(context, colorr, up, down) => Center(
      child: Text(
        '''$up''',
        style: Theme.of(context)
            .textTheme
            .headline6
            .apply(color: colorr, fontFamily: "Athiti"),
      ),
    );

Widget detailCicleDateTime(width, context,
    {DateTime dateimteORstartDateTime,
    DateTime endDateTime,
    colorBg = Colors.amberAccent,
    textColor = Colors.red}) {
  //Month and year's datetimes are the same || single datetime
  //ex: 12 มี.ค 2564 OR 12-24 มี.ค 2564
  Widget selectedWidget;
  if (endDateTime == null ||
      (dateimteORstartDateTime.month == endDateTime?.month &&
          dateimteORstartDateTime.year == endDateTime?.year)) {
    String day = dateimteORstartDateTime.day.toString();
    if (endDateTime != null &&
        (dateimteORstartDateTime.day != endDateTime?.day))
      day = dateimteORstartDateTime?.day.toString() +
          "-" +
          endDateTime.day.toString();
    selectedWidget = Column(children: [
      Expanded(
        child: Transform.translate(
            offset: Offset(0, 10),
            child: AutoSizeText(day,
                style: Theme.of(context).textTheme.headline4.apply(
                    color: textColor,
                    fontFamily: "Athiti",
                    fontWeightDelta: 3,
                    fontSizeDelta: 2))),
      ),
      Expanded(
        child: Transform.translate(
          offset: Offset(0, -5),
          child: AutoSizeText(
              DateFormat.MMM().format(dateimteORstartDateTime).toString() +
                  " " +
                  DateFormat.y().format(dateimteORstartDateTime).toString(),
              style: Theme.of(context).textTheme.subtitle1.apply(
                  color: textColor, fontFamily: "Athiti", fontWeightDelta: 3)),
        ),
      )
    ]);
  }

  //year's datetimes is the same, month's datetimes is difference
  //ex: 12ก.พ.-24 มี.ค 64
  else if (endDateTime != null &&
      (dateimteORstartDateTime.month != endDateTime.month)) {
    var yearStartAdder = "";
    if (dateimteORstartDateTime.year != endDateTime.year) {
      yearStartAdder = dateimteORstartDateTime.year
          .toString()
          .substring(0, dateimteORstartDateTime.year.toString().length - 2);
    }

    var end = DateFormat("dMMMy").format(endDateTime).toString();

    selectedWidget = Column(
      children: [
        Expanded(
            flex: 5,
            child: Transform.translate(
              offset: Offset(0, 8),
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: AutoSizeText(
                    DateFormat("dMMM").format(dateimteORstartDateTime) +
                        yearStartAdder,
                    style: Theme.of(context).textTheme.headline5.apply(
                        color: textColor,
                        fontSizeDelta: 2,
                        fontFamily: "Athiti",
                        fontWeightDelta: 3)),
              ),
            )),
        Expanded(
          flex: 3,
          child: AutoSizeText(
            "ถึง",
            style: Theme.of(context).textTheme.headline6.apply(
                color: textColor, fontFamily: "Athiti", fontWeightDelta: 3),
          ),
        ),
        Expanded(
            flex: 5,
            child: Transform.translate(
              offset: Offset(0, -8),
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: AutoSizeText(
                  end.substring(0, end.length - 4) +
                      end.substring(end.length - 2),
                  minFontSize: 5,
                  style: Theme.of(context).textTheme.headline5.apply(
                      color: textColor,
                      fontFamily: "Athiti",
                      fontSizeDelta: 1,
                      fontWeightDelta: 3),
                ),
              ),
            ))
      ],
    );
  }

  return Container(
    width: width * 0.25,
    height: width * 0.25,
    decoration: BoxDecoration(
      color: colorBg,
      shape: BoxShape.circle,
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          blurRadius: 5,
          offset: Offset(0, 5), // Shadow position
        ),
      ],
    ),
    child: Padding(padding: const EdgeInsets.all(3.0), child: selectedWidget),
  );
}

Widget detailCicle(label, value, unit, width, context,
    {colorBg = Colors.red, textColor = Colors.tealAccent}) {
  return Container(
    width: width * 0.25,
    height: width * 0.25,
    decoration: BoxDecoration(
      color: colorBg,
      shape: BoxShape.circle,
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          blurRadius: 5,
          offset: Offset(0, 5), // Shadow position
        ),
      ],
    ),
    child: Padding(
      padding: const EdgeInsets.all(3.0),
      child: Stack(
        children: [
          Center(
            child: Transform.translate(
                offset: Offset(0, -5),
                child: AutoSizeText.rich(
                  TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: value,
                          style: Theme.of(context).textTheme.headline4.apply(
                              color: textColor,
                              fontFamily: "Athiti",
                              fontWeightDelta: 3)),
                    ],
                  ),
                )),
          ),
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Transform.translate(
                  offset: Offset(0, 0),
                  child: Text("$label",
                      style: TextStyle(
                          color: textColor,
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Athiti")),
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Transform.translate(
                    offset: Offset(0, 0),
                    child: AutoSizeText.rich(
                      TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: unit,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .apply(
                                      color: textColor,
                                      fontFamily: "Athiti",
                                      fontSizeDelta: 2,
                                      fontWeightDelta: 3))
                        ],
                      ),
                      maxLines: 1,
                      minFontSize: 5,
                    )),
              ),
            ),
          ]),
        ],
      ),
    ),
  );
}

Widget detailCard(Widget head, Widget body, width, height, Color colorHaed,
    Color colorBody, Color bodyTextColor, context) {
  var bodyWidth = width * 0.65 / 5;
  var allHeight = width * 0.4;
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Stack(
      children: [
        Container(
          width: width * 0.41,
          height: allHeight,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                blurRadius: 5,
                offset: Offset(0, 5), // Shadow position
              ),
            ],
          ),
        ),
        Container(
          width: width * 0.6,
          height: allHeight,
//          decoration: BoxDecoration(
// boxShadow: [
//                   BoxShadow(
//                     color: Colors.grey.withOpacity(0.3),
//                     blurRadius: 3,
//                     offset: Offset(4, 8), // Shadow position
//                   ),
//                 ],
//          ),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20)),
                    color: colorHaed,
                    // boxShadow: [
                    //   BoxShadow(
                    //     color: Colors.grey.withOpacity(0.3),
                    //     blurRadius: 3,
                    //     offset: Offset(4, 8), // Shadow position
                    //   ),
                    // ],
                  ),
                  width: bodyWidth,
                  height: allHeight,
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 3.0),
                        child: body,
                      )),
                ),
              ),
              Expanded(
                child: Stack(
                  children: [
                    // Transform.translate(
                    //   offset: Offset((bodyWidth - bodyWidth * 1.2), 0),
                    //   child:
                    // Container(
                    //     decoration: BoxDecoration(
                    //       color: Color(colorSkyTheme["backdrop"]),

                    //       borderRadius: BorderRadius.all(Radius.circular(20)),
                    //       //   boxShadow: [
                    //       //   BoxShadow(
                    //       //     color: Colors.grey.withOpacity(0.3),
                    //       //     blurRadius: 3,
                    //       //     offset: Offset(4, 8), // Shadow position
                    //       //   ),
                    //       // ],
                    //     ),
                    //     width: allHeight*0.50,
                    //     height: allHeight,
                    //   ),),

                    Transform.translate(
                      offset: Offset((bodyWidth - bodyWidth * 1.2), 0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: colorBody,

                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          //   boxShadow: [
                          //   BoxShadow(
                          //     color: Colors.grey.withOpacity(0.3),
                          //     blurRadius: 3,
                          //     offset: Offset(4, 8), // Shadow position
                          //   ),
                          // ],
                        ),
                        width: allHeight * 0.50,
                        height: allHeight,
                        child: Center(child: head),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}
