import 'package:appmattress/animation/listViewInitialAnimationVersion2.dart';
import 'package:appmattress/animation/widget_bouncing.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/widget_elements/call_detail_alert.dart';
import 'package:flutter/material.dart';

import 'datail_card.dart';

detailCircleWidgetSlot(
        width, height, avgValue, unit, overTimeCounting, cancelCounting,
        
        {bool isCircleChart = false, DateTime start, DateTime end,DateTime onDateTimeChange}) =>
    Container(
      height: width * 0.25,
      width: width,
      child: Padding(
        padding: const EdgeInsets.only(left: 0, right: 0),
        child: Center(
          child: ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              itemCount: 3,
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  width: 20,
                );
              },
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return [
                  isCircleChart
                      ? BouncingButton(detailCicleDateTime(width, context,
                          dateimteORstartDateTime: start,
                          endDateTime: end,
                          colorBg: Color(colorSkyTheme["cicle_data_date_bg"]),
                          textColor:
                              Color(colorSkyTheme["cicle_data_date_text"])))
                      : BouncingButton(detailCicle(
                          "นั่งไปแล้ว", "$avgValue", "$unit", width, context,
                          colorBg: Color(colorSkyTheme["cicle_data_avg_bg"]),
                          textColor:
                              Color(colorSkyTheme["cicle_data_avg_text"]))),
                  BouncingButton(
                    detailCicle("นั่งเกิน", "$overTimeCounting", "ครั้ง", width,
                        context,
                        colorBg: Color(colorSkyTheme["cicle_data_overtime_bg"]),
                        textColor:
                            Color(colorSkyTheme["cicle_data_overtime_text"])),
                    action: () => circleDetailNotiDialog(
                        context,
                        txt_log["circle_widget_alert"]["overt_time"]["head"],
                        txt_log["circle_widget_alert"]["overt_time"]["body"]),
                  ),
                  BouncingButton(
                    detailCicle(
                      "ยกเลิกเตือน",
                      "$cancelCounting",
                      "ครั้ง",
                      width,
                      context,
                      colorBg:
                          Color(colorSkyTheme["cicle_data_cancel_counting_bg"]),
                      textColor: Color(
                          colorSkyTheme["cicle_data_cancel_counting_text"]),
                    ),
                    action: () => circleDetailNotiDialog(
                        context,
                        txt_log["circle_widget_alert"]["calcel"]["head"],
                        txt_log["circle_widget_alert"]["calcel"]["body"]),
                  ),
                ].toList()[index];
              }),
        ),
      ),
    );

typedef DateTimeCallback = void Function(DateTime onDateTimeChange);