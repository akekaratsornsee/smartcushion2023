import 'package:appmattress/widget_elements/alert_widget/%E0%B8%B7number_seclector_and_input_timeUp.dart';
import 'package:flutter/material.dart';

import 'alert_widget/circle_detail_alert.dart';
import 'alert_widget/number_selector_and_input.dart';

circleDetailNotiDialog(context, title, body) async {
  await showDialog<CircleAlertDetail>(
    context: context,
    builder: (context) => CircleAlertDetail(title: title, body: body),
  );
}

updateNotificationSittingTimeValue(rootcontext, title, body, value) async {
  await showDialog<NumberSelectorDialog>(
    context: rootcontext,
    builder: (contextchild) => NumberSelectorDialog(
        title: title,
        body: body,
        initialValue: value,
        rootContext: rootcontext),
  );
}

updateNotificationTimeupValue(rootcontext, title, body, value) async {
  await showDialog<NumberSelectorDialog2>(
    context: rootcontext,
    builder: (contextchild) => NumberSelectorDialog2(
        title: title,
        body: body,
        initialValue: value,
        rootContext: rootcontext),
  );
}
