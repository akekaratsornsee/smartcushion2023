import 'package:appmattress/util/read_write_txtfile.dart';
import 'package:custom_cupertino_picker/custom_cupertino_picker.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SittingNotification extends StatefulWidget {
  /// initial selection for the slider
  final String body;
  final String title;
  const SittingNotification({Key key, this.title, this.body}) : super(key: key);

  @override
  _SittingNotificationState createState() => _SittingNotificationState();
}

class _SittingNotificationState extends State<SittingNotification> {
  /// current selection of the slider
  String _title;
  String _body;

  List yearGen = List.generate(5, (index) => DateTime.now().year - index);

  // final _cancelPushRef = FirebaseDatabase.instance.reference().child("Value");

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _body = widget.body;
  }

  Future<void> _increment() async {
    String databaseSelector = await read("databaseName");
    final _cancelRef = FirebaseDatabase.instance
        .reference()
        .child(databaseSelector)
        .child("Value")
        .child("Alarm");
    // Increment counter in transaction.
    final TransactionResult transactionResult =
        await _cancelRef.runTransaction((MutableData mutableData) async {
      if (mutableData.value == 1) {
        mutableData.value = 0;
      } else {
        mutableData.value = 0;
      }
      // mutableData.value = (mutableData.value ?? 0) - 1;
      return mutableData;
    });

    // push (adding)

    // if (transactionResult.committed) {
    //   await _cancelPushRef.push().set(<String, String>{"Cancel Alarm": transactionResult.toString()});
    // } else {
    //   print('Transaction not committed.');
    //   if (transactionResult.error != null) {
    //     print(transactionResult.error.message);
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text(_title),
      content: Container(
        height: 150,
        child: Column(
          children: [
            Expanded(child: Text(_body)),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () async {
            await _increment();
            // Use the second argument of Navigator.pop(...) to pass
            // back a result to the page that opened the dialog
            Navigator.pop(context);
          },
          child: Text('ไม่ลุก'),
        ),
        FlatButton(
          onPressed: () async {
            // Use the second argument of Navigator.pop(...) to pass
            // back a result to the page that opened the dialog
            Navigator.pop(context);
          },
          child: Text('ลุก'),
        )
      ],
    );
  }
}
