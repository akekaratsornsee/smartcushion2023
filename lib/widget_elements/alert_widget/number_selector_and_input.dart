import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/read_write_txtfile.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:custom_cupertino_picker/custom_cupertino_picker.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:numberpicker/numberpicker.dart';

// final alertTimeInSeconds = FirebaseDatabase.instance
//     .reference()
//     .child("ice")
//     .child("Value")
//     .child("AlertTimeInSeconds");

class NumberSelectorDialog extends StatefulWidget {
  /// initial selection for the slider
  final String body;
  final String title;
  final int initialValue;
  final BuildContext rootContext;
  const NumberSelectorDialog(
      {Key key, this.title, this.body, this.initialValue, this.rootContext})
      : super(key: key);

  @override
  _NumberSelectorDialogState createState() => _NumberSelectorDialogState();
}

class _NumberSelectorDialogState extends State<NumberSelectorDialog>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  /// current selection of the slider
  String _title;
  String _body;

  List yearGen = List.generate(5, (index) => DateTime.now().year - index);
  @override
  bool get wantKeepAlive => true;
  var _controller;

  TextEditingController _textEditingController;
  int _currentValue = 0;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _body = widget.body;
    _currentValue = widget.initialValue ?? 0;
    _textEditingController = TextEditingController(text: '$_currentValue');
  }

  var alertTimeInSeconds;
  Future<void> updatealertTimeInSeconds(int value) async {
    String databaseNameTxt = await read("databaseName");
    alertTimeInSeconds = FirebaseDatabase.instance
        .reference()
        .child(databaseNameTxt)
        .child("Value")
        .child("AlertTimeInSeconds");
    final TransactionResult transactionResult = await alertTimeInSeconds
        .runTransaction((MutableData mutableData) async {
      mutableData.value = value;
      return mutableData;
    });

    // push (adding)
    Future.delayed(const Duration(milliseconds: 2000), () {
      if (transactionResult.committed) {
        ScaffoldMessenger.of(widget.rootContext).showSnackBar(SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text("บันทึกข้อมูลเรียบร้อยแล้ว"),
        ));
        Navigator.pop(context);
      } else {
        ScaffoldMessenger.of(widget.rootContext).showSnackBar(SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text("บันทึกข้อมูลล้มเหลว"),
        ));
        Navigator.pop(context);
      }
      Navigator.pop(context);
    });

    //   await _cancelPushRef.push().set(<String, String>{"Cancel Alarm": transactionResult.toString()});
    // } else {
    //   print('Transaction not committed.');
    //   if (transactionResult.error != null) {
    //     print(transactionResult.error.message);
    //   }
    // }
  }

  showLoaderDialog(BuildContext rootcontext) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          SizedBox(
            height: 55,
            child: SpinKitPulse(
              color: Colors.green,
              size: 50.0,
              controller: _controller,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("กำลังอยู่ระหว่างการบันทึก..."),
          ),
        ],
      ),
    );

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text(
        _title,
        style: Theme.of(context).textTheme.headline6.apply(
            color: Color(colorSkyTheme["text_subhead"]), fontWeightDelta: 3),
      ),
      content: Container(
          height: height * 0.3,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // a ruler picker
                Column(
                  children: <Widget>[
                    NumberPicker(
                      axis: Axis.horizontal,
                      value: _currentValue,
                      minValue: 0,
                      maxValue: 100,
                      onChanged: (value) {
                        setState(() {
                          _currentValue = value;
                        });
                        _textEditingController.text = value.toString();
                        _textEditingController.selection =
                            TextSelection.fromPosition(TextPosition(
                                offset: _textEditingController.text.length));
                      },
                    ),
                    Text('ตั้งเวลาแจ้งเตือนการนั่ง: $_currentValueนาที'),
                  ],
                ),
                // a text field to sync the value of the ruler picker
                Container(
                  margin: EdgeInsets.only(top: 20),
                  width: 300,
                  child: CupertinoTextField(
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    autofocus: false,
                    keyboardType: TextInputType.number,
                    controller: _textEditingController,
                    onChanged: (value) {
                      // if(int.parse(value)< 200){
                      //   showValue = int.parse(value);
                      // }
                      if (value.isNotEmpty) {
                        setState(() {
                          if (int.parse(value) < 0) {
                            _currentValue = 0;
                            _textEditingController.text = "100";
                          } else if (int.parse(value) > 100) {
                            _currentValue = 100;
                            _textEditingController.text = "100";
                          } else {
                            _currentValue = int.parse(value);
                          }
                        });
                        _textEditingController.selection =
                            TextSelection.fromPosition(TextPosition(
                                offset: _textEditingController.text.length));
                      }
                    },
                    onEditingComplete: () {
                      setState(() {
                        if (int.parse(_textEditingController.text) < 0) {
                          _textEditingController.text = "0";
                          _textEditingController.selection =
                              TextSelection.fromPosition(TextPosition(
                                  offset: _textEditingController.text.length));
                        } else if (int.parse(_textEditingController.text) >
                            100) {
                          _currentValue = 100;
                          _textEditingController.text = "100";
                          _textEditingController.selection =
                              TextSelection.fromPosition(TextPosition(
                                  offset: _textEditingController.text.length));
                        } else {
                          // _textEditingController.text = "100";'
                          if (_textEditingController.text.isNotEmpty) {
                            _currentValue =
                                int.tryParse(_textEditingController.text);
                          }
                        }
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                ListTile(
                  title: Text(
                    "คำแนะนำ",
                    style: TextStyle(
                        color: Colors.black,
                        decoration: TextDecoration.underline,
                        decorationColor: Colors.black,
                        decorationStyle: TextDecorationStyle.solid),
                  ),
                  subtitle: Text(
                    'ไม่ควรนั่งติดต่อกันนานเกิน 40 นาที',
                  ),
                ),
              ],
            ),
          )
          // Column(
          //   children: [
          //     Expanded(
          //       child: Text(_body, style: Theme.of(context)
          //   .textTheme
          //   .subtitle1
          //   .apply(
          //       color:
          //           Color(colorSkyTheme["text_subhead"]),
          //       fontWeightDelta: 1, fontStyle : FontStyle.italic) ,)
          //     ),
          //   ],
          // ),
          ),
      actions: <Widget>[
        TextButton(
          onPressed: () async {
            Navigator.pop(context);
          },
          child: Text(
            'ยกเลิก',
            style: Theme.of(context).textTheme.headline6.apply(
                color: Colors.grey[600]
                // Color(colorSkyTheme["text_subhead"])
                ,
                fontWeightDelta: 3),
          ),
        ),
        TextButton(
          onPressed: () async {
            // Use the second argument of Navigator.pop(...) to pass
            // back a result to the page that opened the dialog
            // Navigator.pop(context);
            try {
              int.parse(_textEditingController.text);
            } catch (e) {
              showCupertinoDialog(
                  context: context,
                  builder: (context) {
                    return CupertinoAlertDialog(
                      title: Text('error'),
                      content: Text('Phone Field is Empty'),
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ok'))
                      ],
                    );
                  });
            }
            if (_textEditingController.text.isEmpty) {
              showCupertinoDialog(
                  context: context,
                  builder: (context) {
                    return CupertinoAlertDialog(
                      title: Text('error'),
                      content: Text('Phone Field is Empty'),
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ok'))
                      ],
                    );
                  });
            } else {
              // Validation passed
              try {
                showLoaderDialog(widget.rootContext);
                await updatealertTimeInSeconds(_currentValue * 60);
                // Future.delayed(const Duration(milliseconds: 2000), () {
                //   Navigator.pop(context);
                // });

              } catch (e) {}
            }
          },
          child: Text(
            'ตกลง',
            style: Theme.of(context).textTheme.headline6.apply(
                color: Colors.grey[600]
                // Color(colorSkyTheme["text_subhead"])
                ,
                fontWeightDelta: 3),
          ),
        )
      ],
    );
  }
}
