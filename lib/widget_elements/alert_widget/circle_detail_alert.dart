import 'package:appmattress/util/config.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:custom_cupertino_picker/custom_cupertino_picker.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class CircleAlertDetail extends StatefulWidget {
  /// initial selection for the slider
  final String body;
  final String title;
  const CircleAlertDetail({Key key, this.title, this.body}) : super(key: key);

  @override
  _CircleAlertDetailState createState() => _CircleAlertDetailState();
}

class _CircleAlertDetailState extends State<CircleAlertDetail> {
  /// current selection of the slider
  String _title;
  String _body;

  List yearGen = List.generate(5, (index) => DateTime.now().year - index);

  final _cancelRef = FirebaseDatabase.instance
      .reference()
      .child("Value")
      .child("Cancel Alarm");
  final _cancelPushRef = FirebaseDatabase.instance.reference().child("Value");

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _body = widget.body;
  }

  Future<void> _increment() async {
    // Increment counter in transaction.
    final TransactionResult transactionResult =
        await _cancelRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;
      return mutableData;
    });

    // push (adding)

    // if (transactionResult.committed) {
    //   await _cancelPushRef.push().set(<String, String>{"Cancel Alarm": transactionResult.toString()});
    // } else {
    //   print('Transaction not committed.');
    //   if (transactionResult.error != null) {
    //     print(transactionResult.error.message);
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text(
        _title,
        style: Theme.of(context).textTheme.headline6.apply(
            color: Color(colorSkyTheme["text_subheadCC&OC"]),
            fontWeightDelta: 3),
      ),
      content: Container(
        height: height * 0.1,
        child: Column(
          children: [
            Expanded(
                child: Text(
              _body,
              style: Theme.of(context).textTheme.subtitle1.apply(
                  color: Color(colorSkyTheme["text_subheadCC&OC"]),
                  fontWeightDelta: 1,
                  fontStyle: FontStyle.italic),
            )),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () async {
            // Use the second argument of Navigator.pop(...) to pass
            // back a result to the page that opened the dialog
            Navigator.pop(context);
          },
          child: Text(
            'ปิด',
            style: Theme.of(context).textTheme.headline6.apply(
                color: Color(colorSkyTheme["text_subheadCC&OC"]),
                fontWeightDelta: 3),
          ),
        )
      ],
    );
  }
}
