import 'dart:async';
import 'dart:collection';
import 'dart:collection';
import 'dart:convert';
import 'package:appmattress/BLEpage.dart';
import 'package:appmattress/navigationDrawer/scrolcontrol_with_tabbar_and_silver_app.dart';
import 'package:appmattress/senddef.dart';
import 'package:appmattress/sittingStatus.dart';
import 'package:appmattress/txt_read_write/txt_rw.dart';
import 'package:appmattress/util/read_write_txtfile.dart';
import 'package:appmattress/util/userDatabaseSelector.dart';
import 'package:appmattress/widget_elements/alert_widget/alertDialog.dart';

import 'package:appmattress/widget_elements/util/overtime_and_cancel_counting.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:ui' as ui;
import 'package:appmattress/animation/listViewInitialAnimationVersion2.dart';
import 'package:appmattress/main.dart';
import 'package:appmattress/text_formatter/text_formatter.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/cross_fade.dart';
import 'package:appmattress/widget_elements/call_detail_circle.dart';
import 'package:appmattress/widget_elements/datail_card.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:appmattress/chart2/util/daily2.dart';
import 'package:appmattress/chartCirle.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'animation/listViewInitialAnimation.dart';
import 'animation/widget_bouncing.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
import 'chart2/dairychart2.dart';
import 'chart2/monthlychartV2.dart';
import 'chart2/util/weely2.dart';
import 'chart2/weeklychart.dart';
import 'navigationDrawer/expansionTile.dart';
import 'navigationDrawer/tabviewWithSliverAppbarHelper.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'dart:math' as math;
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart'; ///////////////////////////

import 'widget_elements/call_detail_alert.dart';

import 'main.dart';

import 'package:appmattress/ripple_animation.dart';
import 'package:appmattress/splash_screen.dart';

class Homepage extends StatefulWidget {
  @override
  HomepageState createState() => HomepageState();
}

class HomepageState extends State<Homepage>
    with AutomaticKeepAliveClientMixin<Homepage> {
  static DateTime pickedDate = DateTime.now();
  static String formattedDate = DateFormat('dd-MM-y').format(pickedDate);
  static String day = DateFormat('dd').format(pickedDate);
  static String month = DateFormat('MMM').format(pickedDate);
  static String year = DateFormat('y').format(pickedDate);

  List valueOneWeek = List<int>.generate(7, (int index) => 0);
  List valueOneMonth = List<int>.generate(30, (int index) => 0);
  double width;
  double height;
  int OUTSW = 0;
  bool isLoadingData = true;
  bool ckeckFunc = false;
  bool initAnimateDetailCard = false;
  String databaseSelectorxxx;
  final DatabaseReference dataBaseCall = FirebaseDatabase.instance.reference();
  DatabaseReference dataBase;
  DatabaseReference dataRef;
  final dataRef1 = FirebaseDatabase.instance.reference();
  // final alertTimeValue = FirebaseDatabase.instance
  //     .reference()
  //     .child("Value")
  //     .child("AlertTimeInSeconds");
  var myalertTimeValue = 0;

  DatabaseReference realTimesittingTimeValuex;
  DatabaseReference realTimesittingCheckCall;
  var picR = false;
  var realTimesittingTimeValue;
  var realTimesittingCheck;
  double percent_stime = 0;
  DatabaseReference _alarmNotiRef;
  DatabaseReference _resetAlarmRef;
  DatabaseReference countingdatabaseConnectionRef;
  SplayTreeMap<String, Map<String, double>> dataListWithMinute =
      new SplayTreeMap<String, Map<String, double>>();
  var dataHistory = GetHistoryData();
  var hisDataRe;
  var overTimeandCancelCounting = new Counting();
  var dairyoverTimeandCancelCounting = new Counting();
  var weeklyoverTimeandCancelCounting = new Counting();
  var monthlyoverTimeandCancelCounting = new Counting();
  var top = 0.0;
  // var tabIndex; ///////////////////////////////////////////////////////////////////////
  var isRange = false;
  var alert = false;
  var nRange = 0;
  var nDay = 0;
  var temp = "";
  var maxADay = 0;
  var minADay = 0;
  bool disableControl = true;
  var tabIndex;
  final NestedScrollController nestedScrollController =
      NestedScrollController();

  ScrollController scController = ScrollController();
  List<Widget> itemdrawColor = [];
  List tempFindMax;
  var dateDisplay = RichText(
    text: TextSpan(
      children: <TextSpan>[
        TextSpan(
            text: DateFormat.yMMMEd('th').format(DateTime.now()),
            style: TextStyle(
                color: Color(colorSkyTheme["text_body"]),
                fontSize: 15,
                fontWeight: FontWeight.bold,
                fontFamily: "Athiti")),
      ],
    ),
  );
  String strDateSingle = DateFormat.yMMMEd('th').format(DateTime.now());
  AutoSizeText strDateRange = AutoSizeText.rich(
    TextSpan(
      children: <TextSpan>[
        TextSpan(
            text: 'เลือกช่วงวันที่',
            style: TextStyle(
                fontFamily: "Athiti", fontSize: 15, color: Colors.white)),
      ],
    ),
  );
  DateTime start;
  DateTime end;

  printlogScolling() {
    // var data = nestedScrollController.totalOffset;
    // nestedScrollController.nestedJumpTo(offset)
    // print("print inner offset  :: $data");
  }

  Map<String, double> myList = {};
  SplayTreeMap<String, Map<String, double>> subMinuiteCount =
      new SplayTreeMap<String, Map<String, double>>();
  var timeShow = DateTime.now();
  String myAvgPerDay = "0.0";

  var sittingData;

  bool checkShowSittingTime() {
    try {
      DateTime dateSelected = timeShow;
      DateTime now = DateTime.now();

      bool condition1 =
          DateTime(dateSelected.year, dateSelected.month, dateSelected.day) ==
              DateTime(now.year, now.month, now.day);
      bool condition2 = !isRange
          ? true
          : DateTime(start.year, start.month, start.day) ==
              DateTime(end.year, end.month, end.day);
      return condition1 && (!isRange || condition2);
    } catch (e) {
      // TODO
      return false;
    }
  }

  bool _switchValue = false;

  void _sittingNotiDialog() async {
    await showDialog<dynamic>(
      context: context,
      builder: (context) => SittingNotification(
        title: "แจ้งเตือนการนั่ง",
        body:
            "ขณะนี้ถึงเวลาเหมาะสมสำหรับการเปลี่ยนอิริยาบทแล้ว กรุณาลุกจากเบาะรองนั่งด้วยค่ะ",
      ),
    ).whenComplete(() {
      // await _alarmNotiRef.runTransaction((MutableData mutableData) async {
      //   mutableData.value = 0;

      //   return mutableData;
      // });
      // OUTSW = 0;
    });
  }

  myInit(DateTime datetime, {DateTime start, DateTime end}) async {
    var refreshOverTimeandCancelCounting = new Counting();
    await refreshOverTimeandCancelCounting.init(isRange ? start : datetime,
        end: end);
    setState(() {
      overTimeandCancelCounting = refreshOverTimeandCancelCounting;
    });

    print("DO :: myInit");
    maxADay = 0;
    minADay = 0;
    List<SplayTreeMap<String, Map<String, double>>> tempSunMinuteData = [];

    if (isRange) {
      print(start);
      print(end);
      var nLoop = DateTime(end.year, end.month, end.day)
          .difference(DateTime(start.year, start.month, start.day))
          .inDays;
      List<dynamic> tempData = [];
      SplayTreeMap<String, double> temp = new SplayTreeMap<String, double>();
      for (var i = 0; i <= nLoop; i++) {
        var getDataquery = await dairyDataQueryV2(dataHistory.data ?? {},
            fullDate: true,
            datetimeSelector: DateTime(start.year, start.month, start.day + i),
            isChartcalled: false);
        SplayTreeMap<String, double> value = getDataquery["json"];
        SplayTreeMap<String, Map<String, double>> tempSubMinuiteCount =
            getDataquery["jsonSubMinitesCount"];
        // print("exec value $i" + value.toString());
        tempData.add(value);
        tempSunMinuteData.add(tempSubMinuiteCount);
      }
      print(tempData);
      // List tlist = [tempmyList, tempmyList2];
      var subter = 0;
      // print("now hour:: $tempTime");
      for (var i = 0; i < 24; i++) {
        var newTempTime = (23 - subter).toString();
        var hour = newTempTime.length == 1 ? "0" + newTempTime : newTempTime;
        temp[hour] = 0.0;

        tempData.forEach((element) {
          // print("data element :: ${element[hour]}");
          temp[hour] = temp[hour] + element[hour];
        });
        subter += 1;
      }

      setState(() {
        nRange = nLoop;
        nDay = nRange + 1;
        myList = temp;
        // subMinuiteCount = getDataquery[""]
        double avg = 0;

        if (myList != null) {
          avg = myList.entries
              .toList()
              .map((m) => m.value)
              .reduce((a, b) => a + b);
        }
        myAvgPerDay =
            ((avg * setting["config"]["multiply_record_in_secs"] / 60) / nDay)
                .toStringAsFixed(0);
      });
      drawColor();
    } else {
      var getData = await dairyDataQueryV2(dataHistory.data ?? {},
          fullDate: true, datetimeSelector: datetime, isChartcalled: false);
      SplayTreeMap<String, double> tempmyList = getData["json"];
      SplayTreeMap<String, Map<String, double>> tempSubMinuiteCount =
          getData["jsonSubMinitesCount"];
      tempSunMinuteData.add(tempSubMinuiteCount);
      double avg = 0;
      if (tempmyList != null) {
        avg = tempmyList.entries
            .toList()
            .map((m) => m.value)
            .reduce((a, b) => a + b);
      }

      setState(() {
        myList = tempmyList;

        myAvgPerDay = (avg * setting["config"]["multiply_record_in_secs"] / 60)
            .toStringAsFixed(0);
      });

      drawColor();
    }

    SplayTreeMap<String, Map<String, double>> prototype =
        collectRangeDataPrototype();
    SplayTreeMap<String, Map<String, double>> sumSittingTimeRange =
        countRangeDataV2withSubMinute(prototype, tempSunMinuteData);

    setState(() {
      dataListWithMinute = sumSittingTimeRange;
      print("data collected dataListWithMinute $dataListWithMinute");
    });
    findMaxV2withSubMinute(sumSittingTimeRange);
  }

  getData(DateTime startDatatime, DateTime endDatatime) async {
    var toDay = new DateTime.now();
    var temp = await dataBase
        .orderByKey()
        .startAt("20210327")
        .endAt("20210327")
        .once();
    var values = temp.value;
    for (int i = 0; i < 8; i++) {}
    // print("My range value ::$values");
    await getADayData(null);
    setState(() {
      sittingData = values;
      isLoadingData = false;
    });
  }

  getADayData(DateTime datatime) async {
    DateTime tempDate = DateFormat('d/M/yyyy').parse("27/03/2021");
    var strDateTime = DateFormat('yyyyMMdd').format(tempDate);
    var temp =
        await dataBase.orderByKey().equalTo(strDateTime.toString()).once();
    var values = temp.value;
    print("datetime string :: ${strDateTime.toString()}");
    // print("xxx:: ${values.toString()}");
  }

  collectRangeDataPrototype() {
    SplayTreeMap<String, Map<String, double>> prototype =
        new SplayTreeMap<String, Map<String, double>>();

    for (var i = 0; i <= 24; i++) {
      var key = i.toString();
      if (key.length == 1) key = "0" + key;
      prototype[key] = {"0-15": 0.0, "15-30": 0.0, "30-45": 0.0, "45-59": 0.0};
    }
    return prototype;
  }

  countRangeDataV2withSubMinute(
      SplayTreeMap<String, Map<String, double>> prototype,
      List<SplayTreeMap<String, Map<String, double>>> data) {
    SplayTreeMap<String, Map<String, double>> collectedData = prototype;
    data.forEach((parentValue) {
      parentValue?.forEach((key, value) {
        if (value["0-15"] > 0) collectedData[key]["0-15"] += value["0-15"];
        if (value["15-30"] > 0) collectedData[key]["15-30"] += value["15-30"];
        if (value["30-45"] > 0) collectedData[key]["30-45"] += value["30-45"];
        if (value["45-59"] > 0) collectedData[key]["45-59"] += value["45-59"];
      });
    });
    return collectedData;
  }

  findMaxV2withSubMinute(
      SplayTreeMap<String, Map<String, double>> collectedData) {
    var tempCollectedData = collectedData.entries
        .map((e) => e.value.values.toList())
        .toList(); // List<List<double>>
    var newList = [];
    if (tempCollectedData != null && tempCollectedData.isNotEmpty) {
      newList = tempCollectedData
          .expand((x) => x)
          .toList(); // List<List<double>> to List<double>
      newList.sort((a, b) => a.compareTo(b));
      newList.removeWhere((element) => element == 0.0);
    }
    tempFindMax = [0];

    if (newList.isNotEmpty) {
      setState(() {
        maxADay =
            (newList.last * setting["config"]["multiply_record_in_secs"] / 60)
                .toInt();
        minADay =
            (newList.first * setting["config"]["multiply_record_in_secs"] / 60)
                .toInt();
        tempFindMax = [newList.last, newList.first];
      });
    }
  }

  bool isSwitched = false;
  List<Widget> drawColor() {
    Rect rect = Rect.fromCenter(
        center: new Offset((250 / 2), (250 / 2) + 50.0),
        width: 250,
        height: 250);
    List<Widget> itempItemdrawColor = [];
    String currentHour = DateTime.now().hour.toString();
    Map<String, double> xmyList = {"$currentHour": 2.0};
    // if (xmyList?.length > 0){
    //   xmyList.forEach((key, value) {if (value > 0.0) itempItemdrawColor.add(
    //     Tooltip(
    //       message: "xoxoxo",
    //       child: InkWell(
    //         onTap: () => print(value.toString()),
    //         child: new CustomPaint(
    //           painter:
    //           // ArcPainter(myList)
    //           DrawFreqColor(2.0, value.toInt(), key, 50.0),
    //         ),
    //       ),
    //     ));});
    //   setState(() {
    //   itemdrawColor = itempItemdrawColor;
    // });
    // print("Asign draw color :: $itemdrawColor");
    // }

    return itempItemdrawColor;
  }

  temptempGetaDay() async {
    //"E, MMMM d yyyy"
    var mdyString = '04/11/2020';
    var dateTime2 = DateFormat('d/M/yyyy').parse(mdyString);
    var dayName = DateFormat.EEEE().format(dateTime2).toString();
    var mounthName = DateFormat.MMMM().format(dateTime2).toString();
    var day = DateFormat.d().format(dateTime2).toString();
    var year = DateFormat.y().format(dateTime2).toString();
    var validFormat =
        "$dayName, $mounthName ${(day.length == 1) ? "0" + day : day} $year  ";
    var temp = await dataBase.orderByKey().equalTo(validFormat).once();
    var values = temp.value;
    print(
        "Query database by specify the day (${validFormat.toString().replaceAll("  ", "")})");

    setState(() {
      sittingData = values;
      isLoadingData = false;
    });
  }

  notiSubscription() async {
    _alarmNotiRef.keepSynced(true);
    var _counterSubscription =
        _alarmNotiRef.onValue.listen((Event event) async {
      if (event.snapshot.value == 1) {
        _sittingNotiDialog();
      } else {
        print("not pass condition");
      }
    }, onError: (Object o) {
      final DatabaseError error = o;
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      setupDatabaseSelector();

      // mainsteam().of(context).rebuild();
    });
    if (tabIndex == 0) {
      setState(() {
        detailCircleWidgetSlot(
          width,
          height,
          myAvgPerDay,
          "นาที/วัน",
          overTimeandCancelCounting.overTimeCounting?.length,
          overTimeandCancelCounting.cancelCounting?.length,
          isCircleChart: true,
          start: isRange ? start : timeShow,
          end: isRange ? end : null,
        );
      });
    }

    Intl.defaultLocale = "th";
    initializeDateFormatting();
    nestedScrollController.addListener(() {
      printlogScolling();
    });

    setupDatabaseSelector();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      // setupDatabaseSelector();
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => BLE()),
      // );
      // setupDatabaseSelector();
      await setupDatabaseSelector();
      await yoyo();
      // _sittingNotiDialog();
      await notiSubscription();
      await myInit(timeShow);
    });
  }

  @override
  void dispose() {
    super.dispose();
    // overTimeandCancelCounting = new Counting();
  }

  var timeUp;
  var alertTimeInSeconds;
  String NameU = "...";
  String IndexBarTxt;
  setupDatabaseSelector() async {
    String databaseNameTxt = await read("databaseName");
    IndexBarTxt = await readIndex("IndexBar");
    if (IndexBarTxt == null) {
      removefile("databaseName");
      FlutterRestart.restartApp();
    }
    tabIndex = int.parse(IndexBarTxt);
    print("database name connected on $databaseNameTxt");
    dataBase = dataBaseCall.child(databaseNameTxt).child("DATE");
    dataRef = dataBaseCall.child(databaseNameTxt).child("Value");

    realTimesittingCheckCall =
        dataBaseCall.child(databaseNameTxt).child("State");
    countingdatabaseConnectionRef = FirebaseDatabase.instance
        .reference()
        .child(databaseNameTxt)
        .child('Value')
        .child("Counting");
    NameU = databaseNameTxt;
    _alarmNotiRef =
        dataBaseCall.child(databaseNameTxt).child("Value").child("Alarm");
    _resetAlarmRef = dataBaseCall.child(databaseNameTxt).child("Value");
    realTimesittingTimeValuex =
        dataBaseCall.child(databaseNameTxt).child("Value");
    realTimesittingTimeValue = realTimesittingTimeValuex;
    realTimesittingCheck = realTimesittingCheckCall;
    timeUp = FirebaseDatabase.instance
        .reference()
        .child(databaseNameTxt)
        .child("Value")
        .child("TimeUp");
    alertTimeInSeconds = FirebaseDatabase.instance
        .reference()
        .child(databaseNameTxt)
        .child("Value")
        .child("AlertTimeInSeconds");
  }

  DISsm() async {
    String databaseNameTxt = await read("databaseName");
    dataRef1.child(databaseNameTxt).child('Value').update({'silenceMode': 0});

    await writefile("F");
  }

  yoyo() async {
    print("DO :: yoyo");
    var now = DateTime.now();
    // myalertTimeValue = (await alertTimeValue.once()).value ?? 0;
    await dataHistory.init();
    await overTimeandCancelCounting.init(now);
    await dairyoverTimeandCancelCounting.init(now);
    await weeklyoverTimeandCancelCounting.init(
        dateWeeluFormatter(now).subtract(Duration(days: 6)),
        end: dateWeeluFormatter(now));
    await monthlyoverTimeandCancelCounting.init(DateTime(now.year, 1, 1),
        end: DateTime(now.year, 12, 12));

    setState(() {
      hisDataRe = dataHistory.data;
      sittingData = true;

      isLoadingData = false;
    });
  }

  _showParentDialog() async {
    final DatabaseReference parentDatabase =
        FirebaseDatabase.instance.reference().root();
    DataSnapshot myParent = await parentDatabase.once();
    Map mapParent = myParent.value;
    List parents = [];
    mapParent.forEach((key, value) {
      parents.add(key.toString());
      print(key);
    });
    // print("log key parent :: ${myParent.value}");
    final databaseSelection = await showDialog<String>(
      context: context,
      builder: (context) => UserParentDatabaseSelector(parents: parents),
    );
    print("my log selector ${databaseSelection}");
    if (databaseSelection != null) {
      setState(() {
        // databaseSelector = databaseSelection;
        setupDatabaseSelector();
      });
    }
  }

  mainsteam() {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return MaterialApp(
        theme: ThemeData(
            primaryColor: Color(colorSkyTheme["base_color"]),
            fontFamily: "Athiti"),
        locale: const Locale('th', 'TH'),
        home: tabIndex == null
            ? Splash()
            : DefaultTabController(
                length: 4,
                initialIndex: tabIndex,
                child: SafeArea(
                  child: Scaffold(
                      // drawerScrimColor: Colors.white,
                      endDrawer: Builder(builder: (contextx) {
                        return Theme(
                          data: ThemeData(
                              primaryIconTheme:
                                  IconThemeData(color: Colors.red)),
                          child: Drawer(
                            child: Container(
                              color: Colors.transparent,
                              child: ListView(
                                shrinkWrap: true,
                                // Important: Remove any padding from the ListView.
                                padding: EdgeInsets.zero,
                                children: <Widget>[
                                  Stack(
                                    children: [
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 8.0, top: 30),
                                          child: IconButton(
                                              iconSize: 35.0,
                                              onPressed: () {
                                                Navigator.of(contextx).pop();
                                              },
                                              icon: Icon(Icons.arrow_back)),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 30.0),
                                          child: Text(
                                            "การตั้งค่า",
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline4
                                                .apply(
                                                    color: Color(colorSkyTheme[
                                                        "text_head"]),
                                                    fontWeightDelta: 3),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),

                                  SnackBarButton(),
                                  // ListTile(
                                  //   leading: Icon(Icons.person_add),
                                  //   title: Text("ลงชื่อเข้าใช้งาน"),
                                  //   onTap: () {
                                  //     Navigator.push(
                                  //       context,
                                  //       MaterialPageRoute(
                                  //           builder: (context) => WifiSetter()),
                                  //     );
                                  //   },
                                  // ),
                                  ListTile(
                                    leading: Icon(Icons.arrow_back),
                                    title: Text("ออกจากระบบ"),
                                    onTap: () async {
                                      await DISsm();
                                      await removefile("databaseName");
                                      await removefileIndex("IndexBar");
                                      await FlutterRestart.restartApp();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => BLE()),

                                        //Navigator.of(context).pop();

                                        // Navigator.of(contextx).pop();
                                        // ScaffoldMessenger.of(contextx)
                                        //     .showSnackBar(SnackBar(
                                        //   behavior: SnackBarBehavior.floating,
                                        //   content: Text("คุณได้ออกจากระบบเรียบร้อยแล้ว"),
                                        // )
                                        // );
                                      );
                                    },
                                  ),
                                  // IconButton(
                                  //   onPressed: () async {
                                  //     await _showParentDialog();
                                  //   },
                                  //   icon: Icon(Icons.supervised_user_circle_rounded),
                                  //   iconSize: 60,
                                  // )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                      //////////////////////////////////////////////////////////////////////////////////////////////////////////
                      backgroundColor: Color(colorSkyTheme["backdrop"]),
                      // appBar: buildAppBar(),
                      body: NestedScrollView(
                        controller: nestedScrollController,
                        floatHeaderSlivers: true,
                        // physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                        headerSliverBuilder:
                            (BuildContext contextxx, bool innerBoxIsScrolled) {
                          return <Widget>[
                            SliverAppBar(
                                toolbarHeight: 80,
                                // forceElevated: innerBoxIsScrolled,
                                iconTheme: IconThemeData(
                                    color: Color(colorSkyTheme["text_appName"]),
                                    size: 35),
                                elevation: 0.0,
                                // expandedHeight: 140.0,
                                floating: false,
                                // forceElevated: false,
                                backgroundColor:
                                    Color(colorSkyTheme["base_color"]),
                                pinned: true,
                                // snap: false,
                                // stretch: true,
                                // excludeHeaderSemantics: true,
                                flexibleSpace: LayoutBuilder(builder:
                                    (BuildContext context,
                                        BoxConstraints constraints) {
                                  return FlexibleSpaceBar(
                                    title: Transform.translate(
                                      offset: Offset(-60, 0),
                                      child: ListTile(
                                        title: Text(
                                          "Cushion",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4
                                              .apply(
                                                  color: Color(colorSkyTheme[
                                                      "text_appName"]),
                                                  fontWeightDelta: 3),
                                        ),
                                        subtitle: Text(
                                          'ยินดีต้อนรับคุณ $NameU',
                                          style: TextStyle(fontSize: 18),
                                        ),
                                        contentPadding:
                                            EdgeInsets.only(bottom: 25),
                                      ),
                                    ),
                                    // background: Container(
                                    //   color: Color(colorSkyTheme["base_color"]),
                                    //   child: Stack(
                                    //     children: [
                                    //       Align(
                                    //         alignment: Alignment.centerRight,
                                    //         child: Padding(
                                    //             padding: const EdgeInsets.all(5.0),
                                    //             child: Transform(
                                    //               alignment: Alignment.center,
                                    //               transform: Matrix4.rotationY(math.pi),
                                    //               child: Image.asset(
                                    //                 'lib/pictures/office woker.png',
                                    //                 fit: BoxFit.fill,
                                    //               ),
                                    //             )),
                                    //       ),
                                    //     ],
                                    //   ),
                                    // ),
                                  );
                                })),
                            SliverPersistentHeader(
                              delegate: SliverAppBarDelegate(
                                TabBar(
                                  onTap: (index) {
                                    setState(() {
                                      tabIndex = index;
                                      // disableControl = false;
                                    });
                                    if (nestedScrollController.totalOffset
                                            .round()
                                            .abs() !=
                                        87) {
                                      print(
                                          "Amination scolling router condition passed");
                                      // nestedScrollController.animateTo(87,
                                      //     duration: Duration(seconds: 1),
                                      //     curve: Curves.fastOutSlowIn);
                                      nestedScrollController.animateTo(1,
                                          duration: Duration(seconds: 1),
                                          curve: Curves.fastOutSlowIn);
                                    }
                                  },
                                  labelColor:
                                      Color(colorSkyTheme["text_subhead"]),
                                  labelStyle: TextStyle(
                                      fontSize: 18,
                                      fontFamily: "Athiti",
                                      fontWeight: FontWeight.w600),
                                  indicatorColor:
                                      Color(colorSkyTheme["text_subhead"]),
                                  indicatorWeight: 5,
                                  indicatorSize: TabBarIndicatorSize.label,
                                  tabs: [
                                    Container(
                                        height: 150,
                                        width: width / 4.5,
                                        decoration: BoxDecoration(
                                            color: tabIndex == 0
                                                ? Color(
                                                    colorSkyTheme["tapbar_bg"])
                                                : Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    .withOpacity(0.1),
                                            border: Border.all(
                                              color: Colors.transparent,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Padding(
                                          padding: const EdgeInsets.all(1.0),
                                          child: Center(
                                              child: Text(
                                            'วัน',
                                            style: TextStyle(
                                                color: tabIndex != 0
                                                    ? Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    : Color(colorSkyTheme[
                                                        "text_subhead"])),
                                          )),
                                        )),
                                    Container(
                                        height: 150,
                                        width: width / 4.5,
                                        decoration: BoxDecoration(
                                            color: tabIndex == 1
                                                ? Color(
                                                    colorSkyTheme["tapbar_bg"])
                                                : Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    .withOpacity(0.1),
                                            border: Border.all(
                                              color: Colors.transparent,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Padding(
                                          padding: const EdgeInsets.all(1.0),
                                          child: Center(
                                              child: Text(
                                            'สัปดาห์',
                                            style: TextStyle(
                                                color: tabIndex != 1
                                                    ? Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    : Color(colorSkyTheme[
                                                        "text_subhead"])),
                                          )),
                                        )),
                                    Container(
                                        height: 150,
                                        width: width / 4.5,
                                        decoration: BoxDecoration(
                                            color: tabIndex == 2
                                                ? Color(
                                                    colorSkyTheme["tapbar_bg"])
                                                : Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    .withOpacity(0.1),
                                            border: Border.all(
                                              color: Colors.transparent,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Padding(
                                          padding: const EdgeInsets.all(1.0),
                                          child: Center(
                                              child: Text(
                                            'ปี',
                                            style: TextStyle(
                                                color: tabIndex != 2
                                                    ? Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    : Color(colorSkyTheme[
                                                        "text_subhead"])),
                                          )),
                                        )),
                                    Container(
                                        height: 150,
                                        width: width / 4.5,
                                        decoration: BoxDecoration(
                                            color: tabIndex == 3
                                                ? Color(
                                                    colorSkyTheme["tapbar_bg"])
                                                : Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    .withOpacity(0.1),
                                            border: Border.all(
                                              color: Colors.transparent,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: Padding(
                                          padding: const EdgeInsets.all(1.0),
                                          child: Center(
                                              child: Text(
                                            'การนั่ง',
                                            style: TextStyle(
                                                color: tabIndex != 3
                                                    ? Color(colorSkyTheme[
                                                        "tapbar_bg"])
                                                    : Color(colorSkyTheme[
                                                        "text_subhead"])),
                                          )),
                                        )),
                                    // Tab(text: 'รายวัน'),
                                    // Tab(text: 'สัปดาห์'),
                                    // Tab(text: 'เดือน'),
                                  ],
                                ),
                              ),
                              pinned: true,
                            ),
                            // If the main content is a list, use SliverList instead.
                          ];
                        }, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        body: LiquidPullToRefresh(
                          backgroundColor: Colors.black12,
                          color: Colors.white,
                          animSpeedFactor: 1.0,
                          onRefresh: _pullRefresh,
                          child: (!isLoadingData && (sittingData != null))
                              ? LayoutBuilder(builder: (BuildContext context,
                                  BoxConstraints constraints) {
                                  nestedScrollController.enableScroll(context);
                                  nestedScrollController
                                      .enableCenterScroll(constraints);
                                  // NestedScrollView.sliverOverlapAbsorberHandleFor(context);
                                  return Center(
                                    child: ListView(
                                      // shrinkWrap: true,
                                      physics: BouncingScrollPhysics(),
                                      children: [
                                        Container(
                                          color:
                                              Color(colorSkyTheme["backdrop"]),
                                          child: AnimatedSwitcher(
                                              duration:
                                                  Duration(milliseconds: 500),
                                              child: tabbarviewChildren(
                                                  width, height)[tabIndex]),
                                          // chart(),
                                        ),

                                        // Padding(
                                        //   padding: const EdgeInsets.all(15.0),
                                        //   child: Row(
                                        //     mainAxisAlignment :MainAxisAlignment.spaceEvenly,
                                        //     children: ,
                                        //   ),
                                        // ),

                                        Container(
                                          height: height * 0.12,
                                          width: width,
                                          child: ListView.separated(
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount: 1,
                                              separatorBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return SizedBox(
                                                  width: 0,
                                                );
                                              },
                                              shrinkWrap: true,
                                              scrollDirection: Axis.horizontal,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Padding(
                                                  padding: index == 0
                                                      ? const EdgeInsets.only(
                                                          left: 15.0)
                                                      : const EdgeInsets.all(
                                                          0.0),
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: width / 2,
                                                        child: [
                                                          StreamBuilder(
                                                              stream:
                                                                  alertTimeInSeconds
                                                                      .onValue,
                                                              builder: (context,
                                                                  snap) {
                                                                var value = 0;
                                                                if (snap.hasData &&
                                                                    !snap
                                                                        .hasError &&
                                                                    snap.data.snapshot
                                                                            .value !=
                                                                        null) {
                                                                  value = snap
                                                                      .data
                                                                      .snapshot
                                                                      .value;
                                                                }
                                                                return AnimatedSwitcher(
                                                                    duration: Duration(
                                                                        seconds:
                                                                            5),
                                                                    child: AnimatedListItemVersion2(
                                                                        index,
                                                                        BouncingButton(
                                                                          detailCard(
                                                                              detailCardItemText(context, (value / 60)?.round().toString() ?? 0, "นาที"),
                                                                              detailCardItembodyV2(context, Color(colorSkyTheme["text_alarmNoti_card"]), "นั่ง", ""),
                                                                              width,
                                                                              height,
                                                                              Color(colorSkyTheme["body_alarmNoti_card"]),
                                                                              Color(colorSkyTheme["haed_alarmNoti_card"]),
                                                                              Color(colorSkyTheme["text_alarmNoti_card"]),
                                                                              context),
                                                                          action: () => updateNotificationSittingTimeValue(
                                                                              context,
                                                                              "เปลี่ยนเวลาการแจ้งเตือน",
                                                                              "",
                                                                              (value / 60).round()),
                                                                        )));
                                                              }),
                                                          // BouncingButton(detailCard(
                                                          //     detailCardItemIcon(
                                                          //         'lib/pictures/thumbs-up.svg'),
                                                          //     detailCardItembody(
                                                          //         context,
                                                          //         Color(colorSkyTheme[
                                                          //             "text_health_card"]),
                                                          //         "สุขภาพ",
                                                          //         "การนั่งดี"),
                                                          //     width,
                                                          //     height,
                                                          //     Color(colorSkyTheme[
                                                          //         "body_health_card"]),
                                                          //     Color(colorSkyTheme[
                                                          //         "haed_health_card"]),
                                                          //     Color(colorSkyTheme[
                                                          //         "text_health_card"]),
                                                          //     context)),
                                                        ].toList()[index],
                                                      ),
                                                      Container(
                                                        width: width / 2,
                                                        child: [
                                                          StreamBuilder(
                                                              stream: timeUp
                                                                  .onValue,
                                                              builder: (context,
                                                                  snap) {
                                                                var value = 0;
                                                                if (snap.hasData &&
                                                                    !snap
                                                                        .hasError &&
                                                                    snap.data.snapshot
                                                                            .value !=
                                                                        null) {
                                                                  value = snap
                                                                      .data
                                                                      .snapshot
                                                                      .value;
                                                                }
                                                                return AnimatedSwitcher(
                                                                  duration:
                                                                      Duration(
                                                                          seconds:
                                                                              5),
                                                                  child: AnimatedListItemVersion2(
                                                                      index,
                                                                      BouncingButton(
                                                                        detailCard(
                                                                            detailCardItemText(
                                                                                context,
                                                                                (value / 60)?.round().toString() ?? 0,
                                                                                "นาที"),
                                                                            detailCardItembodyV2(context, Color(colorSkyTheme["text_health_card"]), "ลุก", ""),
                                                                            width,
                                                                            height,
                                                                            Color(colorSkyTheme["body_health_card"]),
                                                                            Color(colorSkyTheme["haed_health_card"]),
                                                                            Color(colorSkyTheme["text_health_card"]),
                                                                            context),
                                                                        action: () => updateNotificationTimeupValue(
                                                                            context,
                                                                            "เปลี่ยนเวลาการแจ้งเตือน",
                                                                            " ",
                                                                            (value / 60).round()),
                                                                      )),
                                                                );
                                                              }),
                                                        ].toList()[index],
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              }),
                                        ),
                                      ],
                                    ),

                                    // subHaed(context, "เกี่ยวกับการนั่ง"),
                                    // Padding(
                                    //   padding: const EdgeInsets.all(15.0),
                                    //   child: Center(
                                    //     child: Container(
                                    //         // height:
                                    //         //     MediaQuery.of(context).size.height * 0.4,
                                    //         width: MediaQuery.of(context).size.width *
                                    //             0.90,
                                    //         decoration: BoxDecoration(
                                    //           // border: Border.all(width: 3.0),
                                    //           color: Color(
                                    //               colorSkyTheme["cover_base_color"]),
                                    //           borderRadius: BorderRadius.all(
                                    //               Radius.circular(colorSkyTheme[
                                    //                   "container_chart_BorderRadius"]) //                 <--- border radius here
                                    //               ),
                                    //         ),
                                    //         child: Padding(
                                    //           padding: const EdgeInsets.all(15.0),
                                    //           child: AutoSizeText(
                                    //               "      เป็นที่ทราบกันดีว่าการขาดการออกกำลังกายก่อให้เกิดโรคไม่ติดต่อหลายโรค แต่หลายคนอาจลืม หรือนึกไม่ถึงว่า การนั่งทำงานตลอดทั้งวันโดยไม่ได้มีกิจกรรมเผาผลาญะพลังงานในร่างกายมากนัก ก็อันตรายต่อสุขภาพได้ไม่น้อยไปกว่าการขาดการออกกำลังกาย และยังอาจเกิดขึ้นได้กับคนที่นั่งทำงานทั้งวันอย่าง พนักงานออฟฟิศ คนขับรถ แคชเชียร์ คนขายตั๋ว พนักงานให้บริการในสำนักงานต่าง ๆ เช่น คอลเซ็นเตอร์ ศูนย์ราชการต่าง ๆ เป็นต้น",
                                    //               style: Theme.of(context)
                                    //                   .textTheme
                                    //                   .subtitle2
                                    //                   .apply(
                                    //                       fontSizeFactor: 1.2,
                                    //                       color: Colors.white
                                    //                           .withOpacity(0.85))),
                                    //         )),
                                    //   ),
                                    // ),

                                    // const SizedBox(
                                    //   height: 1,
                                    // ),
                                  );
                                })
                              : Center(child: CircularProgressIndicator()),
                        ),
                      )),
                ),
                ////////////////////////////////////////////////////////////
              ));
  }

  @override
  Widget build(BuildContext context) => mainsteam();

  // sittingTimeDisplayRealTimeRouter(width, height) {
  //   return BouncingButton(
  //     Padding(
  //         padding: const EdgeInsets.only(bottom: 20.0),
  //         child:
  //             (realTimesittingTimeValue == null || realTimesittingCheck == null)
  //                 ? warningToSitingNotice(
  //                     width,
  //                     height,
  //                     Color(colorSkyTheme["text_body"]),
  //                     "ไม่พบ",
  //                     "การเชื่อมต่อ",
  //                     Colors.red)
  //                 : StreamBuilder(
  //                     stream: realTimesittingCheck.onValue,
  //                     builder: (context, snap) {
  //                       var value = false;
  //                       if (snap.hasData &&
  //                           !snap.hasError &&
  //                           snap.data.snapshot.value != null) {
  //                         value = snap.data.snapshot.value;
  //                       }
  //                       return AnimatedSwitcher(
  //                           duration: Duration(seconds: 5),
  //                           child: value
  //                               ? sittingTimeDisplayRealTime(width, height)
  //                               : warningToSitingNotice(
  //                                   width,
  //                                   height,
  //                                   Color(colorSkyTheme["text_body"]),
  //                                   "โปรดนั่ง",
  //                                   "เพื่อจับเวลา",
  //                                   Colors.yellow));
  //                     })),
  //   );
  // }

  // sittingTimeDisplayRealTime(width, height) => StreamBuilder(
  //     stream: realTimesittingTimeValue.onValue,
  //     builder: (context, snap) {
  //       var sittingTimeInSecondsValue = 0.0;
  //       var alertTimeInSecondsValue = 1.0;
  //       if (snap.hasData &&
  //           !snap.hasError &&
  //           snap.data.snapshot.value != null) {
  //         sittingTimeInSecondsValue =
  //             double.tryParse(snap.data.snapshot.value["stime"].toString()) ??
  //                 55;
  //         alertTimeInSecondsValue = double.tryParse(
  //                 snap.data.snapshot.value["AlertTimeInSeconds"].toString()) ??
  //             55;
  //       }
  //       return Container(
  //         width: (width * 0.6) - 40,
  //         height: (width * 0.6) - 40,
  //         child: LiquidCircularProgressIndicator(
  //           value: sittingTimeInSecondsValue /
  //               alertTimeInSecondsValue, // Defaults to 0.5.
  //           valueColor: AlwaysStoppedAnimation(
  //               Color(colorSkyTheme["circle_chart_wave_non_with_opacity"])
  //                   .withOpacity(colorSkyTheme["circle_chart_opacity"] ??
  //                       0.2)), // Defaults to the current Theme's accentColor.
  //           backgroundColor: Colors
  //               .transparent, // Defaults to the current Theme's backgroundColor.
  //           borderColor: Colors.transparent,
  //           borderWidth: 5.0,
  //           direction: Axis
  //               .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.vertical.
  //           center: Center(
  //               child: CrossFade<double>(
  //                   initialData: 0,
  //                   data: sittingTimeInSecondsValue,
  //                   builder: (value) => AnimatedListItem(
  //                       BouncingButton(
  //                         Container(
  //                           width: 150,
  //                           height: 150,
  //                           child: Padding(
  //                             padding: const EdgeInsets.all(8.0),
  //                             child: Column(
  //                               children: [
  //                                 Expanded(
  //                                   flex: 10,
  //                                   child: AutoSizeText(
  //                                     (value / 60).round().toString(),
  //                                     maxLines: 1,
  //                                     style: Theme.of(context)
  //                                         .textTheme
  //                                         .headline1
  //                                         .apply(
  //                                             color: Color(colorSkyTheme[
  //                                                 "text_subhead_acci"]),
  //                                             fontSizeDelta: 1.5,
  //                                             fontWeightDelta: 2),
  //                                   ),
  //                                 ),
  //                                 SizedBox(
  //                                   height: 15,
  //                                 ),
  //                                 Expanded(
  //                                     flex: 2,
  //                                     child: Transform.translate(
  //                                       offset: Offset(0, -30),
  //                                       child: AutoSizeText(
  //                                         '''นาที/วัน''',
  //                                         maxLines: 1,
  //                                         style: Theme.of(context)
  //                                             .textTheme
  //                                             .headline6
  //                                             .apply(
  //                                                 color: Color(colorSkyTheme[
  //                                                     "text_subhead_acci"]),
  //                                                 fontWeightDelta: 2),
  //                                       ),
  //                                     ))
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                         action: () => print("xoxoxo"),
  //                       ),
  //                       true))),
  //         ),
  //       );
  //     });

  Widget drawValueInChart(text) => Padding(
        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
        child: Text(
          "${text}",
          style: TextStyle(
            color: text == 0
                ? Color(colorSkyTheme["circle_chart"]["1"])
                : Colors.white,
            fontWeight: FontWeight.bold,
            shadows: <Shadow>[
              // Shadow(
              //   offset:
              //       Offset(0, 0),
              //   blurRadius: 15.0,
              //   color:
              //       Color.fromARGB(
              //           255,
              //           0,
              //           0,
              //           0),
              // ),
              // Shadow(
              //   offset:
              //       Offset(
              //           0,
              //           0),
              //   blurRadius:
              //       18.0,
              //   color: Color
              //       .fromARGB(
              //           125,
              //           0,
              //           0,
              //           255),
              // ),
            ],
          ),
        ),
      );

  warningToSitingNotice(width, height, Color textColor, String warningTextLine1,
          String warningTextLine2, Color iconProgressColor) =>
      Container(
          color: Colors.transparent,
          width: (width * 0.6) - 40,
          height: (width * 0.6) - 40,
          child: LiquidCircularProgressIndicator(
              value: 0.00, // Defaults to 0.5.
              valueColor: AlwaysStoppedAnimation(
                  Color(colorSkyTheme["circle_chart_wave_non_with_opacity"])
                      .withOpacity(colorSkyTheme["circle_chart_opacity"] ??
                          0.2)), // Defaults to the current Theme's accentColor.
              backgroundColor: Colors
                  .transparent, // Defaults to the current Theme's backgroundColor.
              borderColor: Colors.transparent,
              borderWidth: 5.0,
              direction: Axis
                  .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.vertical.
              center: Container(
                // margin: EdgeInsets.all(100.0),
                decoration: BoxDecoration(
                    color: Colors.transparent, shape: BoxShape.circle),
                width: (width * 0.6) - 40,
                height: (width * 0.6) - 40,
                child: CrossFade<String>(
                    initialData: "0",
                    data: "N/A",
                    builder: (value) => Padding(
                          padding: const EdgeInsets.only(top: 55, bottom: 30),
                          child: Column(
                            children: [
                              Expanded(
                                flex: 5,
                                child: Column(
                                  children: [
                                    Expanded(
                                        flex: 2,
                                        child: Center(
                                            child: AutoSizeText(
                                          "$warningTextLine1",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline6
                                              .apply(
                                                  color: textColor,
                                                  fontSizeDelta: 3,
                                                  fontWeightDelta: 2),
                                        ))),
                                    Expanded(
                                        flex: 2,
                                        child: Center(
                                            child: AutoSizeText(
                                          "$warningTextLine2",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .apply(
                                                  color: textColor,
                                                  fontSizeDelta: 1.5,
                                                  fontWeightDelta: 2),
                                        ))),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 5,
                                child: SpinKitDoubleBounce(
                                  color: iconProgressColor,
                                  size: 50.0,
                                ),
                              ),
                            ],
                          ),
                        )),
              )));

  sittingTimeDisplayHistory(data) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35),
      child: Container(
        width: 150,
        height: 150,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: CrossFade<String>(
                  initialData: "0",
                  data: double.parse(data).toStringAsFixed(0),
                  builder: (value) => BouncingButton(
                        Column(
                          children: [
                            Expanded(
                              flex: 3,
                              child: Text(
                                'นั่งไปแล้ว',
                                maxLines: 1,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .apply(
                                        color: Color(
                                            colorSkyTheme["text_subhead_acci"]),
                                        fontWeightDelta: 1),
                              ),
                            ),
                            Expanded(
                              flex: 7,
                              child: AutoSizeText(
                                value,
                                maxLines: 1,
                                style: Theme.of(context)
                                    .textTheme
                                    .headline1
                                    .apply(
                                        color: Color(
                                            colorSkyTheme["text_subhead_acci"]),
                                        fontSizeDelta: 1.5,
                                        fontWeightDelta: 2),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Expanded(
                                flex: 2,
                                child: Transform.translate(
                                  offset: Offset(0, -30),
                                  child: AutoSizeText(
                                    '''นาที/วัน''',
                                    maxLines: 1,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .apply(
                                            color: Color(colorSkyTheme[
                                                "text_subhead_acci"]),
                                            fontWeightDelta: 2),
                                  ),
                                ))
                          ],
                        ),
                        action: () => print("xoxoxomm"),
                      )
                  //                   RichText(
//                         textAlign: TextAlign.center,
//                         text: TextSpan(
//                           children: <TextSpan>[
// //                             TextSpan(
// //                               text: '''นั่งเฉลี่ย
// // ''',
// //                               style: Theme.of(context)
// //                                   .textTheme
// //                                   .subtitle1
// //                                   .apply(
// //                                     color: Color(colorSkyTheme["text_body"]),
// //                                     fontWeightDelta: 1,
// //                                     fontSizeDelta: 5,
// //                                   ),
// //                             ),
//                             TextSpan(
//                               text: '''$value''',
//                               style: Theme.of(context)
//                                   .textTheme
//                                   .headline3
//                                   .apply(
//                                       color: Color(colorSkyTheme["text_body"]),
//                                       fontSizeDelta: 1.5,
//                                       fontWeightDelta: 2),
//                             ),
//                             TextSpan(
//                               text: "นาที",
//                               style: Theme.of(context)
//                                   .textTheme
//                                   .headline6
//                                   .apply(
//                                       color: Color(colorSkyTheme["text_body"]),
//                                       fontSizeDelta: 0.6,
//                                       fontWeightDelta: 2),
//                             ),
//                           ],
//                         ),
//                       )
                  )),
        ),
      ),
    );
  }

  Future<void> _pullRefresh() async {
    await writeIndex("${tabIndex.toString()}", "IndexBar");
    Navigator.of(context).pop();
    Navigator.pushNamed(
      context,
      '/HOME-page',
    );
    print(".................refresh allready");
  }

  List tabbarviewChildren(width, height) => [
        // Container(
        //   color: Color(colorSkyTheme["backdrop"]),
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: <Widget>[
        //       Padding(
        //         padding: const EdgeInsets.all(5.0),
        //         child: Container(
        //             height: (width * 0.65) + (height * 0.3),
        //             child:
        //                 DairyChartV2(hisDataRe, dairyoverTimeandCancelCounting,
        //                     onDateTimeChange: (date) async {
        //               final temp =
        //                   await dairyoverTimeandCancelCounting.init(date);
        //               this.setState(() {
        //                 dairyoverTimeandCancelCounting = temp;
        //               });
        //             })),
        //       ),
        //     ],
        //   ),
        // ),
        Container(
          child: Padding(
            padding: EdgeInsets.only(
                left: colorSkyTheme["container_chart_padding"],
                right: colorSkyTheme["container_chart_padding"],
                bottom: colorSkyTheme["container_chart_padding"],
                top: 0),
            child: Container(
              color: Color(colorSkyTheme["backdrop"]),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(
                          colorSkyTheme["container_chart_padding_inner"]),
                      child: Container(
                        height: (width - 20) + width * 0.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                              colorSkyTheme["container_chart_BorderRadius"]),
                          color: Color(colorSkyTheme["chart_backgroud"]),
                          // boxShadow: [
                          //   BoxShadow(color: Colors.green, spreadRadius: 3),
                          // ],
                        ),
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 7,
                                    child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: AutoSizeText(
                                        'จำนวนการนั่งต่อวัน',
                                        style: TextStyle(
                                            fontSize: 24.0,
                                            fontWeight: FontWeight.bold,
                                            color: Color(colorSkyTheme[
                                                "text_subhead_acci"])),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                          backgroundColor: Color(colorSkyTheme[
                                              "text_subhead_acci"]),
                                          primary: Colors.red,
                                        ),
                                        onPressed: () async {
                                          await dateSelectorDialog(
                                              height, false);
                                        },
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 7,
                                              child: Center(
                                                child: AutoSizeText(
                                                  'รายวัน',
                                                  maxLines: 1,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1
                                                      .apply(
                                                          color: Colors.white,
                                                          fontFamily: "Athiti",
                                                          fontWeightDelta: 2),
                                                ),
                                              ),
                                            ),

                                            // AutoSizeText('เลือกวัน'),
                                            // Expanded(
                                            //   flex: 1,
                                            //   child: Icon(
                                            //     Icons.arrow_drop_down,
                                            //     color: Color(
                                            //         colorSkyTheme["text_body"]),
                                            //   ),
                                            // )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: TextButton(
                                        style: TextButton.styleFrom(
                                          backgroundColor: Color(colorSkyTheme[
                                              "text_subhead_acci"]),
                                          primary: Colors.red,
                                        ),
                                        onPressed: () async {
                                          await dateSelectorDialog(
                                              height, true);
                                        },
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 7,
                                              child: Center(
                                                child: AutoSizeText(
                                                  'ช่วง',
                                                  maxLines: 1,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1
                                                      .apply(
                                                          color: Colors.white,
                                                          fontFamily: "Athiti",
                                                          fontWeightDelta: 2),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),

                              detailCircleWidgetSlot(
                                width,
                                height,
                                myAvgPerDay,
                                "นาที/วัน",
                                overTimeandCancelCounting
                                    .overTimeCounting?.length,
                                overTimeandCancelCounting
                                    .cancelCounting?.length,
                                isCircleChart: true,
                                start: isRange ? start : timeShow,
                                end: isRange ? end : null,
                              ),

                              // Padding(
                              //     padding:
                              //         const EdgeInsets.only(left: 15.0 + 8.0),
                              //     child: Builder(builder: (context) {
                              //       return dateDisplay;
                              //     })),
                              BouncingButton(
                                  Container(
                                    width: height * 0.6,
                                    height: width,
                                    child: Stack(
                                      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Center(
                                          child: Transform.translate(
                                            offset: Offset(-35, 0),
                                            child: Stack(
                                              alignment:
                                                  AlignmentDirectional.center,
                                              children: (itemdrawColor.length >
                                                      0)
                                                  ? [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  bottom: 155),
                                                          child: new SizedBox(
                                                            width: width * 0.6,
                                                            height: width * 0.6,
                                                            child:
                                                                new CustomPaint(
                                                              painter: new ArcPainter(
                                                                  myList,
                                                                  dataListWithMinute,
                                                                  checkShowSittingTime()
                                                                      ? timeShow
                                                                      : null,
                                                                  maxADay,
                                                                  minADay,
                                                                  context),
                                                            ),
                                                          ),
                                                        )
                                                      ] +
                                                      itemdrawColor
                                                          .map<Padding>(
                                                            (e) => Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        bottom:
                                                                            155),
                                                                child: SizedBox(
                                                                    width:
                                                                        250.0,
                                                                    height:
                                                                        250.0,
                                                                    child: e)),
                                                          )
                                                          .toList() +
                                                      [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(0.0),
                                                          child:
                                                              sittingTimeDisplayHistory(
                                                                  myAvgPerDay),
                                                        )
                                                      ]
                                                  : [
                                                      //แสดงผลกราฟวงกลม
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                bottom: 155),
                                                        child: new SizedBox(
                                                          width: width * 0.6,
                                                          height: width * 0.6,
                                                          child:
                                                              new CustomPaint(
                                                            painter: new ArcPainter(
                                                                myList,
                                                                dataListWithMinute,
                                                                checkShowSittingTime()
                                                                    ? timeShow
                                                                    : null,
                                                                maxADay,
                                                                minADay,
                                                                context),
                                                          ),
                                                        ),
                                                      ),
                                                      // checkShowSittingTime()
                                                      //     ? Padding(
                                                      //         padding:
                                                      //             const EdgeInsets
                                                      //                     .only(
                                                      //                 bottom:
                                                      //                     55,
                                                      //                 top: 1),
                                                      //         child:
                                                      //             sittingTimeDisplayRealTimeRouter(
                                                      //                 width,
                                                      //                 height))
                                                      //     :
                                                      sittingTimeDisplayHistory(
                                                          myAvgPerDay),
                                                    ],
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.centerRight,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 80.0, right: 25),
                                            child: Container(
                                              decoration: new BoxDecoration(
                                                border: Border.all(
                                                    color: Color(colorSkyTheme[
                                                        "backdrop"]), // Set border color
                                                    width: 5.0),
                                                // boxShadow: [
                                                //   BoxShadow(
                                                //     color: Colors.grey
                                                //         .withOpacity(0.1),
                                                //     blurRadius: 1,
                                                //     offset: Offset(0,
                                                //         5), // Shadow position
                                                //   ),
                                                // ],
                                                borderRadius:
                                                    BorderRadius.circular(30),
                                                gradient: new LinearGradient(
                                                    colors: [
                                                      Color(colorSkyTheme[
                                                          "circle_chart"]["7"]),
                                                      Color(colorSkyTheme[
                                                          "circle_chart"]["1"]),
                                                    ]
                                                    //  [
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["7"]),
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["6"]),
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["5"]),
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["4"]),
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["3"]),
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["2"]),
                                                    //   Color(
                                                    //       colorSkyTheme["circle_chart"]
                                                    //           ["1"]),

                                                    // ]
                                                    ,
                                                    begin:
                                                        const FractionalOffset(
                                                            0.0, 1.0),
                                                    end: const FractionalOffset(
                                                        0.0, 0.0),
                                                    stops: [0, 1]

                                                    // [
                                                    //   0.0,
                                                    //   0.1067,
                                                    //   0.334,
                                                    //   0.501,
                                                    //   0.668,
                                                    //   0.835,
                                                    //   1.00
                                                    // ]
                                                    ,
                                                    tileMode: TileMode.clamp),
                                              ),
                                              height: (width * 0.6),
                                              width: 40,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: tempFindMax != null
                                                    ? tempFindMax
                                                        .map((e) => drawValueInChart((e *
                                                                setting["config"]
                                                                    [
                                                                    "multiply_record_in_secs"] /
                                                                60)
                                                            .toStringAsFixed(
                                                                1)))
                                                        .toList()
                                                    : [Container()],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  disable: true),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),

        Container(
          color: Color(colorSkyTheme["backdrop"]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                    height: (width * 0.65) + (height * 0.3),
                    child: BarChartSample3(
                      hisDataRe,
                      weeklyoverTimeandCancelCounting,
                      onDateTimeChange: (date) async {
                        final temp = await weeklyoverTimeandCancelCounting.init(
                            dateWeeluFormatter(date)
                                .subtract(Duration(days: 6)),
                            end: dateWeeluFormatter(date));
                        this.setState(() {
                          weeklyoverTimeandCancelCounting = temp;
                        });
                      },
                    )),
              ),
            ],
          ),
        ),
        Container(
          color: Color(colorSkyTheme["backdrop"]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                    height: (width * 0.65) + (height * 0.3),
                    child: MonthlyChartV2(
                      hisDataRe,
                      monthlyoverTimeandCancelCounting,
                      onDateTimeChange: (date) async {
                        final temp = await monthlyoverTimeandCancelCounting
                            .init(DateTime(date.year, 1, 1),
                                end: DateTime(date.year, 12, 12));
                        this.setState(() {
                          monthlyoverTimeandCancelCounting = temp;
                        });
                      },
                    )),
              ),
            ],
          ),
        ),
        sittingStatus(dataRef, realTimesittingTimeValue, width, height,
            myalertTimeValue, percent_stime)
      ];

  dateSelectorDialog(height, R) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          print("$R");
          setState(() {
            //   isRange = !isRange;
            if (R == true && picR == false) {
              isRange = !isRange;
              picR = true;
            } else if (R == false && picR == true) {
              isRange = !isRange;
              picR = false;
            } else {}
          });
          // if (R == false) {
          //   setState(() {
          //     isRange = !isRange;
          //     setState(() {
          //       isRange = !isRange;
          //     });
          //   });
          // } else if (R == true) {
          //   setState(() {
          //     isRange = isRange;
          //     setState(() {
          //       isRange = isRange;
          //     });
          //   });
          // }
          // setState(() {});
          // if (R) {
          //   setState(() {
          //     if (R == true) {
          //       setState(() {
          //         isRange = isRange;
          //       });
          //     } else {
          //       setState(() {
          //         isRange = !isRange;
          //       });
          //     }
          //   });
          // }

          return Dialog(
              // titlePadding: EdgeInsets.all(0),
              // backgroundColor: Colors.orange,

              // title:
              child: Container(
            height: height * 0.7,
            child: Stack(
              overflow: Overflow.visible,
              alignment: Alignment.center,
              children: [
                Column(children: [
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: height * 0.2,
                      color: Colors.blue,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 3,
                            child: Padding(
                              padding: const EdgeInsets.all(3.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 8,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Icon(
                                              isRange
                                                  ? Icons.date_range_sharp
                                                  : Icons.first_page,
                                              color: Colors.white),
                                        ),
                                        Expanded(
                                          flex: 8,
                                          child: Text(
                                            '${isRange ? "เลือกวันแบบช่วง" : "เลือกวันแบบวันเดียว"}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption
                                                .apply(color: Colors.white),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // Expanded(
                                  //   flex: 3,
                                  //   child: MaterialButton(
                                  //       child: Text(
                                  //         "เปลี่ยน",
                                  //         style: TextStyle(color: Colors.white),
                                  //       ),
                                  //       onPressed: () {
                                  //         setState(() {
                                  //           isRange = !isRange;
                                  //         });
                                  //       }),
                                  // ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: isRange
                                  ? Center(child: strDateRange)
                                  : Center(
                                      child: Text(
                                        '$strDateSingle',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .apply(
                                                color: Colors.white,
                                                fontFamily: 'Athiti'),
                                      ),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 8,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Container(
                              height: height * 0.5,
                              child: Card(
                                  child: SfDateRangePicker(
                                navigationMode: isRange
                                    ? DateRangePickerNavigationMode.scroll
                                    : DateRangePickerNavigationMode.snap,
                                initialDisplayDate: isRange ? start : timeShow,
                                initialSelectedDate: timeShow,
                                initialSelectedRange:
                                    PickerDateRange(start, end),
                                view: DateRangePickerView.month,
                                selectionMode: isRange
                                    ? DateRangePickerSelectionMode.range
                                    : DateRangePickerSelectionMode.single,
                                onSelectionChanged:
                                    (DateRangePickerSelectionChangedArgs args) {
                                  print(args.value);

                                  SchedulerBinding.instance
                                      ?.addPostFrameCallback((duration) {
                                    setState(() {});
                                  });

                                  if (isRange) {
                                    setState(() {
                                      if (args.value.startDate == null) {
                                        start = null;
                                        strDateRange = AutoSizeText.rich(
                                          TextSpan(
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5
                                                .apply(color: Colors.white),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: 'เลือกวันเริ่มต้น ')
                                            ],
                                          ),
                                        );
                                      } else if (args.value.endDate == null &&
                                          args.value.startDate != null) {
                                        print("should pass this condition");
                                        end = null;
                                        strDateRange = AutoSizeText.rich(
                                          TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: DateFormat(
                                                        'EE dd MMMM yyyy')
                                                    .format(
                                                        args.value.startDate)
                                                    .toString(),
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline5
                                                    .apply(
                                                        color: Colors.white,
                                                        fontFamily: "Athiti"),
                                              ),
                                              TextSpan(
                                                  text: ' ถึง ',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline4
                                                      .apply(
                                                          color: Colors.white,
                                                          fontWeightDelta: 2,
                                                          fontFamily:
                                                              "Athiti")),
                                              TextSpan(
                                                text: ' เลือกวันสิ้นสุด',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline5
                                                    .apply(
                                                        color: alert
                                                            ? Colors.red
                                                            : Colors.white,
                                                        fontFamily: "Athiti"),
                                              ),
                                            ],
                                          ),
                                          maxLines: 1,
                                          minFontSize: 5,
                                        );
                                      } else {
                                        alert = false;
                                        start = DateTime.parse(
                                            args.value.startDate.toString());
                                        end = DateTime.parse(
                                            args.value.endDate.toString());
                                        if ((start.month == end.month) &&
                                            (start.year == end.year) &&
                                            (start.day != end.day)) {
                                          temp =
                                              '${start.day} - ${end.day} ${DateFormat('MMMM yyyy').format(args.value.startDate)}';
                                          strDateRange = AutoSizeText.rich(
                                            TextSpan(
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5
                                                  .apply(
                                                      color: Colors.white,
                                                      fontFamily: "Athiti"),
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: temp,
                                                ),
                                              ],
                                            ),
                                            maxLines: 1,
                                            minFontSize: 5,
                                          );
                                        } else if (start.day == end.day) {
                                          temp =
                                              '${DateFormat('EE dd MMMM yyyy').format(args.value.startDate)}';
                                          strDateRange = AutoSizeText.rich(
                                            TextSpan(
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5
                                                  .apply(
                                                      color: Colors.white,
                                                      fontFamily: "Athiti"),
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: temp,
                                                ),
                                              ],
                                            ),
                                            maxLines: 1,
                                            minFontSize: 5,
                                          );
                                        } else {
                                          if (start.year != end.year) {
                                            temp =
                                                '${DateFormat('d MMMM yyyy').format(args.value.startDate)} - ${DateFormat('d MMMM yyyy').format(args.value.endDate)}';
                                            strDateRange = AutoSizeText.rich(
                                              TextSpan(
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline5
                                                    .apply(
                                                        color: Colors.white,
                                                        fontFamily: "Athiti"),
                                                children: <TextSpan>[
                                                  TextSpan(
                                                    text: temp,
                                                  ),
                                                ],
                                              ),
                                              maxLines: 1,
                                              minFontSize: 5,
                                            );
                                          } else {
                                            temp =
                                                '${DateFormat('d MMMM').format(args.value.startDate)} - ${DateFormat('d MMMM ').format(args.value.endDate)} ${start.year}';
                                            strDateRange = AutoSizeText.rich(
                                              TextSpan(
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline5
                                                    .apply(
                                                        color: Colors.white,
                                                        fontFamily: "Athiti"),
                                                children: <TextSpan>[
                                                  TextSpan(
                                                    text: temp,
                                                  )
                                                ],
                                              ),
                                              maxLines: 1,
                                              minFontSize: 5,
                                            );
                                          }
                                        }
                                      }
                                    });
                                  } else {
                                    setState(() {
                                      strDateSingle =
                                          DateFormat('EE dd MMMM yyyy')
                                              .format(args.value);
                                      timeShow = args.value;
                                    });
                                  }
                                },
                              ))),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: height * 0.2,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                MaterialButton(
                                  child: Text(
                                    "ยกเลิก",
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                MaterialButton(
                                  child: Text("ตกลง",
                                      style: TextStyle(color: Colors.blue)),
                                  onPressed: () {
                                    if (isRange) {
                                      if (start == null || end == null) {
                                        // set up the button
                                        Widget okButton = FlatButton(
                                          child: Text("OK"),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        );

                                        // set up the AlertDialog
                                        AlertDialog alert = AlertDialog(
                                          title: Text("แจ้ง"),
                                          content: Text(
                                              "โปรดเลือกช่วงเวลาให้ครบถ้วน"),
                                          actions: [
                                            okButton,
                                          ],
                                        );

                                        // show the dialog
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return alert;
                                          },
                                        );
                                        // setState(
                                        //       () {
                                        //  alert = true;

                                        //   });
                                        return;
                                      }
                                      myInit(start, start: start, end: end);
                                      drawColor();
                                      this.setState(() {
                                        dateDisplay = RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: temp,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1
                                                      .apply(
                                                          color: Color(
                                                              colorSkyTheme[
                                                                  "text_body"]),
                                                          fontWeightDelta: 2,
                                                          fontFamily:
                                                              "Athiti")),
                                              // TextSpan(
                                              //     text: " 24 ชม",
                                              //     style: TextStyle(
                                              //         color: const Color(colorSkyTheme["text_body"]),
                                              //         fontSize: 17,
                                              //         fontWeight: FontWeight.bold)),
                                            ],
                                          ),
                                        );
                                      });
                                    } else {
                                      myInit(timeShow,
                                          start: timeShow, end: timeShow);
                                      // myInit(
                                      //   overTimeandCancelCounting
                                      //       .overTimeCounting?.length,
                                      // );
                                      // myInit(
                                      //   overTimeandCancelCounting
                                      //       .cancelCounting?.length,
                                      // );
                                      // myInit(overTimeandCancelCounting);
                                      drawColor();

                                      this.setState(() {
                                        dateDisplay = RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: DateFormat(
                                                          'EE dd MMMM yyyy')
                                                      .format(timeShow),
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .subtitle1
                                                      .apply(
                                                          color: Color(
                                                              colorSkyTheme[
                                                                  "text_body"]),
                                                          fontWeightDelta: 2,
                                                          fontFamily:
                                                              "Athiti")),
                                              // TextSpan(
                                              //     text: " 24 ชม",
                                              //     style: TextStyle(
                                              //         color: const Color(colorSkyTheme["text_body"]),
                                              //         fontSize: 17,
                                              //         fontWeight: FontWeight.bold)),
                                            ],
                                          ),
                                        );
                                      });
                                    }

                                    // strDateRange = "";
                                    // strDateSingle =
                                    //     "";
                                    Navigator.pop(context);
                                  },
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ])
              ],
            ),
          ));
        });
      });
  // void callR(isRange, R) {
  //   if (R == true) {
  //     setState(() {
  //       isRange = isRange;
  //     });
  //   } else {
  //     setState(() {
  //       isRange = !isRange;
  //     });
  //   }
  // }

  @override
  bool get wantKeepAlive => false;
}
