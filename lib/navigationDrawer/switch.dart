library lite_rolling_switch;

import 'package:flutter/material.dart';
import 'dart:ui';
import 'dart:math';

/// Customable and attractive Switch button.
/// Currently, you can't change the widget
/// width and height properties.
///
/// As well as the classical Switch Widget
/// from flutter material, the following
/// arguments are required:
///
/// * [value] determines whether this switch is on or off.
/// * [onChanged] is called when the user toggles the switch on or off.
///
/// If you don't set these arguments you would
/// experiment errors related to animationController
/// states or any other undesirable behavior, please
/// don't forget to set them.
///
class LiteRollingSwitch extends StatefulWidget {
  @required
  final bool value;
  @required
  final Function(bool) onChanged;
  final String textOff;
  final String textOn;
  final Color colorOn;
  final Color colorOff;
  final double textSize;
  final Duration animationDuration;
  final IconData iconOn;
  final IconData iconOff;
  final Function onTap;
  final Function onDoubleTap;
  final Function onSwipe;

  LiteRollingSwitch(
      {this.value = false,
      this.textOff = "Off",
      this.textOn = "On",
      this.textSize = 14.0,
      this.colorOn = Colors.green,
      this.colorOff = Colors.red,
      this.iconOff = Icons.flag,
      this.iconOn = Icons.check,
      this.animationDuration = const Duration(milliseconds: 600),
      this.onTap,
      this.onDoubleTap,
      this.onSwipe,
      this.onChanged});

  @override
  _RollingSwitchState createState() => _RollingSwitchState();
}

class _RollingSwitchState extends State<LiteRollingSwitch>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;
  double value = 0.0;

  bool turnState;

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this,
        lowerBound: 0.0,
        upperBound: 1.0,
        duration: widget.animationDuration);
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.easeInOut);
    animationController.addListener(() {
      setState(() {
        value = animation.value;
      });
    });
    turnState = widget.value;
    _determine();
  }

  @override
  Widget build(BuildContext context) {
    Color transitionColor = Color.lerp(widget.colorOff, widget.colorOn, value);

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onDoubleTap: () {
        _action();
        if (widget.onDoubleTap != null) widget.onDoubleTap();
      },
      onTap: () {
        _action();
        if (widget.onTap != null) widget.onTap();
      },
      onPanEnd: (details) {
        _action();
        if (widget.onSwipe != null) widget.onSwipe();
        //widget.onSwipe();
      },
      child: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: <Widget>[
          FittedBox(
            fit: BoxFit.contain,
            child: SizedBox(
              width: 110,
              height: 50,
              // color: Colors.grey,
              child: Material(
                color: transitionColor,
                borderRadius: BorderRadius.circular(50),
                child: Container(
                  padding: EdgeInsets.all(5),
                  // width: 130,
                  // height: 50,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(50)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(10 * value, 0), //original
            child: Opacity(
              opacity: (1 - value).clamp(0.0, 1.0),
              child: Container(
                padding: EdgeInsets.only(right: 10),
                alignment: Alignment.centerRight,
                height: 70,
                child: Text(
                  widget.textOff,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: widget.textSize),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(10 * (1 - value), 0), //original
            child: Opacity(
              opacity: value.clamp(0.0, 1.0),
              child: Container(
                padding: EdgeInsets.only(/*top: 10,*/ left: 5),
                alignment: Alignment.centerLeft,
                height: 40,
                child: Text(
                  widget.textOn,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: widget.textSize),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(60 * value, 0),
            child: Transform.rotate(
              angle: lerpDouble(0, 2 * pi, value),
              child: Container(
                height: 55,
                width: 55,
                alignment: Alignment.center,
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ], shape: BoxShape.circle, color: Colors.white),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Opacity(
                        opacity: (1 - value).clamp(0.0, 1.0),
                        child: Icon(
                          widget.iconOff,
                          size: 25,
                          color: Color(0x30B6AE).withOpacity(1),
                        ),
                      ),
                    ),
                    Center(
                        child: Opacity(
                            opacity: value.clamp(0.0, 1.0),
                            child: Icon(
                              widget.iconOn,
                              size: 25,
                              color: Color(0x30B6AE).withOpacity(1),
                            ))),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _action() {
    _determine(changeState: true);
  }

  _determine({bool changeState = false}) {
    setState(() {
      if (changeState) turnState = !turnState;
      (turnState)
          ? animationController.forward()
          : animationController.reverse();

      widget.onChanged(turnState);
    });
  }
}
