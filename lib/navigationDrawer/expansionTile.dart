import 'dart:async';

import 'package:appmattress/navigationDrawer/switch.dart';
import 'package:appmattress/txt_read_write/txt_rw.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/read_write_txtfile.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class SnackBarButton extends StatefulWidget {
  @override
  State createState() => new ExpansionTileExample();
}

class ExpansionTileExample extends State<SnackBarButton> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemBuilder: (BuildContext contextyoyo, int index) =>
          EntryItem(data[index], context),
      itemCount: data.length,
    );
  }
}

// One entry in the multilevel list displayed by this app.
class Entry {
  const Entry(this.title, [this.children = const <Entry>[], this.icon]);
  final String title;
  final Icon icon;
  final List<Entry> children;
}

// Data to display.
const List<Entry> data = <Entry>[
  Entry(
    'โหมดเงียบ',
    <Entry>[
      // Entry('เปิด', [], Icon(Icons.notifications_active)),
      // Entry('ปิด', [], Icon(Icons.notifications_off))
    ],
  ),
  // Entry(
  //   'ธีม',
  //   <Entry>[
  //     Entry('เปิด', [], Icon(Icons.power)),
  //     Entry('ปิด', [],  Icon(Icons.power)),
  //   ],
  // ),
];

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
class EntryItem extends StatelessWidget {
  final dataRef = FirebaseDatabase.instance.reference();
  EntryItem(this.entry, this.contextmmo);
  final Entry entry;
  var contextmmo;

  readread() async {
    var switchBool;
    var readtocheck = await readfile();
    if (readtocheck == "T") {
      switchBool = true;
    } else {
      switchBool = false;
    }
    return switchBool;
  }

  notficationSetter(currentState) async {
    String databaseNameTxt = await read("databaseName");

    if (currentState == "" || currentState == null) {
      await writefile("F");
    } else {
      if (currentState) {
        await writefile("F");
        dataRef
            .child(databaseNameTxt)
            .child('Value')
            .update({'silenceMode': 0});
      } else {
        await writefile("T");
        dataRef
            .child(databaseNameTxt)
            .child('Value')
            .update({'silenceMode': 1});
      }
    }
  }

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty)
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // root.icon,
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 25.0),
              child: Text(
                root.title,
                style: Theme.of(contextmmo).textTheme.title.apply(
                    color: Color(colorSkyTheme["text_head"]),
                    fontSizeDelta: 0.1,
                    fontStyle: FontStyle.normal,
                    fontFamily: "Athiti"),
              ),
            ),
          ),
          Container(
            child: Transform.scale(
              scale: 0.5,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: FutureBuilder(
                  future: readread(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return LiteRollingSwitch(
                        value: snapshot.data,
                        textOn: '',
                        textOff: '',
                        colorOn: Colors.greenAccent,
                        colorOff: Colors.grey,
                        iconOn: Icons.circle,
                        iconOff: Icons.circle,
                        textSize: 25.0,
                        onTap: () async {
                          print("Swich on onTap");
                          var switchBool = snapshot.data;
                          await notficationSetter(switchBool);
                          Future.delayed(const Duration(milliseconds: 500), () {
                            ScaffoldMessenger.of(contextmmo)
                                .showSnackBar(SnackBar(
                              behavior: SnackBarBehavior.floating,
                              content: Text(switchBool
                                  ? "ปืดการใช้งานโหมดเงียบ"
                                  : "เปืดการใช้งานโหมดเงียบ"),
                            ));
                          }).whenComplete(() => Navigator.of(contextmmo).pop());
                        },
                        onChanged: (bool state) async {
                          // print("Swich on onChanged");
                        },
                        onSwipe: () async {
                          print("Swich on onSwipe");
                        },
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                ),
              ),
            ),
          ),
        ],
      );
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Row(
        children: [
          Icon(Icons.notifications),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(root.title),
          ),
        ],
      ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}
