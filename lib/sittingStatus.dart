import 'dart:ui';

import 'package:appmattress/animation/widget_bouncing.dart';
import 'package:appmattress/animation/widget_bouncing1.dart';
import 'package:appmattress/util/config.dart';
import 'package:appmattress/util/cross_fade.dart';
import 'package:appmattress/widget_elements/datail_card.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class SitPositiondata {
  SitPositiondata({this.xValue, this.yValue});
  SitPositiondata.fromMap(Map<String, dynamic> dataMap)
      : xValue = dataMap['time'],
        yValue = dataMap['valueRead'];
  final double xValue;
  final int yValue;
}

sittingStatus(dataRef, realTimesittingTimeValue, width, height,
        myalertTimeValue, percent_stime) =>
    Container(
      color: Color(colorSkyTheme["backdrop"]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text(
              'ตำแหน่งของการนั่งขณะปัจจุบัน',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Color(colorSkyTheme["text_subhead_acci"])),
            ),
          ),
          Container(
              child: StreamBuilder(
            stream: dataRef.onValue,
            builder: (BuildContext context, AsyncSnapshot<Event> snap) {
              Widget widget;
              List<SitPositiondata> sitPositiondata = <SitPositiondata>[];

              getValue(snap, key) => snap.data.snapshot.value[key].toString();

              if (snap.hasData) {
                print("snap.hasData ${snap.data.snapshot.value}");
                String txtData = snap.data.snapshot.value.toString();
                String txtData2 = snap.data.snapshot.value["A"].toString();
                String txtData3 = snap.data.snapshot.value["B"].toString();
                String txtData4 = snap.data.snapshot.value["C"].toString();
                String txtData5 = snap.data.snapshot.value["D"].toString();
                String txtData6 = snap.data.snapshot.value["D"].toString();
                var txA = double.parse(txtData2);
                var txB = double.parse(txtData3);
                var txC = double.parse(txtData4);
                var txD = double.parse(txtData5);
                var txall = txA + txB + txC + txD;
                var txall2 = txall.toStringAsFixed(2);
                var all4 = txall / 4;
                var txTS = double.parse(txtData6);
                var SMcenter = 0.25;
                return Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            width: 300,
                            height: 100,
                            decoration: BoxDecoration(
                                color: Colors.teal[100],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  child: Text(
                                    'น้ำหนักขณะที่คุณนั่ง : ' +
                                        ('$txall2') +
                                        '  กก.',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ),
                                ),
                                if (txA < all4 + SMcenter &&
                                    txB < all4 + SMcenter &&
                                    txC < all4 + SMcenter &&
                                    txD < all4 + SMcenter &&
                                    txA > all4 - SMcenter &&
                                    txB > all4 - SMcenter &&
                                    txC > all4 - SMcenter &&
                                    txD > all4 - SMcenter)
                                  (Text(
                                    'นั่งได้ตำแหน่งที่ สมดุล แล้ว',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txC > txA &&
                                    txC > txB &&
                                    txC > txD &&
                                    txA + txB + txD < 30)
                                  (Text(
                                    'นั่งเอียงไปตำแหน่ง หน้าซ้าย',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txB > txA &&
                                    txB > txC &&
                                    txB > txD &&
                                    txA + txC + txD < 30)
                                  (Text(
                                    'นั่งเอียงไปตำแหน่ง หลังซ้าย',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txD > txA &&
                                    txD > txB &&
                                    txD > txC &&
                                    txB + txC + txD < 30)
                                  (Text(
                                    'นั่งเอียงไปตำแหน่ง หน้าขวา',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txA > txB &&
                                    txA > txC &&
                                    txA > txD &&
                                    txB + txC + txD < 30)
                                  (Text(
                                    'นั่งเอียงไปตำแหน่ง หลังขวา',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txC + txD > txA + txB &&
                                    txC > 10 &&
                                    txD > 10)
                                  (Text(
                                    'นั่งเอียงไปด้านหน้า',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txA + txB > txC + txD &&
                                    txA > 10 &&
                                    txB > 10)
                                  (Text(
                                    'นั่งเอียงไปด้านหลัง',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txB + txC > txA + txD &&
                                    txB > 10 &&
                                    txC > 10)
                                  (Text(
                                    'นั่งเอียงไปด้านซ้าย',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else if (txA + txD > txB + txC &&
                                    txA > 10 &&
                                    txD > 10)
                                  (Text(
                                    'นั่งเอียงไปด้านขวา',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                                else
                                  (Text(
                                    'ยังตรวจสอบไม่ได้',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey[700]),
                                  ))
                              ],
                            ),
                          ),
                        ),
                      ]),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: Container(
                    width: 300,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.teal[100],
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Center(
                      child: Text(
                        'Loading',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[700]),
                      ),
                    ),
                  ),
                );
              }
            },
          )),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              height: 300,
              width: 300,
              decoration: BoxDecoration(
                  color: Color(colorSkyTheme["body_alarmNoti_card"]),
                  image: DecorationImage(
                      image: AssetImage("images/base.png"), fit: BoxFit.fill),
                  border: Border.all(color: Colors.grey[500], width: 3),
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              child: SafeArea(
                  child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  children: [
                    Expanded(
                        child: StreamBuilder(
                      stream: dataRef.onValue,
                      builder:
                          (BuildContext context, AsyncSnapshot<Event> snap) {
                        Widget widget;
                        List<SitPositiondata> sitPositiondata =
                            <SitPositiondata>[];

                        getValue(snap, key) =>
                            snap.data.snapshot.value[key].toString();

                        if (snap.hasData) {
                          String txtData = snap.data.snapshot.value.toString();
                          String txtData2 =
                              snap.data.snapshot.value["A"].toString();
                          String txtData3 =
                              snap.data.snapshot.value["B"].toString();
                          String txtData4 =
                              snap.data.snapshot.value["C"].toString();
                          String txtData5 =
                              snap.data.snapshot.value["D"].toString();
                          print("snap :: $txtData");
                          // print("snap :: ${getValue(snap, 'Read Weight A')}");
                          var txA = double.parse(txtData2);
                          var txB = double.parse(txtData3);
                          var txC = double.parse(txtData4);
                          var txD = double.parse(txtData5);

                          return Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 95,
                                        width: 95,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            shape: BoxShape.circle),
                                        child: Padding(
                                          padding: const EdgeInsets.all(12),
                                          // fromLTRB(
                                          //     10, 10, 55, 10),
                                          child: Container(
                                            height: 90,
                                            width: 90,
                                            child:
                                                LiquidCircularProgressIndicator(
                                              value: txC / 25,
                                              // Defaults to 0.5.
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Colors.teal[200]),
                                              backgroundColor: Colors.white,
                                              borderColor: Colors.grey[600],
                                              borderWidth: 4.0,
                                              direction: Axis.vertical,
                                              center: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  // Text('A',
                                                  //     style: TextStyle(
                                                  //         fontSize: 30)),
                                                  AutoSizeText(
                                                    getValue(snap, 'C') + ' kg',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: 95,
                                        width: 95,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            shape: BoxShape.circle),
                                        child: Padding(
                                          padding: const EdgeInsets.all(12),
                                          // fromLTRB(
                                          //     10, 10, 55, 10),
                                          child: Container(
                                            height: 90,
                                            width: 90,
                                            child:
                                                LiquidCircularProgressIndicator(
                                              value: txB / 25,
                                              // Defaults to 0.5.
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Colors.teal[200]),
                                              backgroundColor: Colors.white,
                                              borderColor: Colors.grey[600],
                                              borderWidth: 4.0,
                                              direction: Axis.vertical,
                                              center: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  // Text('A',
                                                  //     style: TextStyle(
                                                  //         fontSize: 30)),
                                                  AutoSizeText(
                                                    getValue(snap, 'B') + ' kg',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ]),
                                Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        height: 95,
                                        width: 95,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            shape: BoxShape.circle),
                                        child: Padding(
                                          padding: const EdgeInsets.all(12),
                                          // fromLTRB(
                                          //     10, 10, 55, 10),
                                          child: Container(
                                            height: 90,
                                            width: 90,
                                            child:
                                                LiquidCircularProgressIndicator(
                                              value: txD / 25,
                                              // Defaults to 0.5.
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Colors.teal[200]),
                                              backgroundColor: Colors.white,
                                              borderColor: Colors.grey[600],
                                              borderWidth: 4.0,
                                              direction: Axis.vertical,
                                              center: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  // Text('A',
                                                  //     style: TextStyle(
                                                  //         fontSize: 30)),
                                                  AutoSizeText(
                                                    getValue(snap, 'D') + ' kg',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: 95,
                                        width: 95,
                                        decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            shape: BoxShape.circle),
                                        child: Padding(
                                          padding: const EdgeInsets.all(12),
                                          // fromLTRB(
                                          // 55, 100, 10, 10),
                                          child: Container(
                                            height: 90,
                                            width: 90,
                                            child:
                                                LiquidCircularProgressIndicator(
                                              value: txA / 25,
                                              // Defaults to 0.5.
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Colors.teal[200]),
                                              backgroundColor: Colors.white,
                                              borderColor: Colors.grey[600],
                                              borderWidth: 4.0,
                                              direction: Axis.vertical,
                                              center: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  // Text('D',
                                                  //     style: TextStyle(
                                                  //         fontSize: 30)),
                                                  AutoSizeText(
                                                    getValue(snap, 'A') + ' kg',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        color: Colors.black),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ]),
                              ],
                            ),
                          );
                        } else {
                          return Center(
                            child: Text(
                              'Loading....',
                              style: TextStyle(fontSize: 20),
                            ),
                          );
                        }
                      },
                    )),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: realTimesittingTimeValue == null
                ? Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        LinearPercentIndicator(
                          width: 300,
                          lineHeight: 35.0,
                          percent: 0.0,
                          center: AutoSizeText(
                            'ไม่มีการเชื่อมต่อ',
                            style: TextStyle(
                                fontSize: 30,
                                color: Colors.grey[700],
                                fontWeight: FontWeight.bold),
                          ),
                          linearStrokeCap: LinearStrokeCap.roundAll,
                          progressColor: Color(0xFF86D9D8),
                        ),
                      ],
                    ),
                  )
                // Container(
                //     color: Colors.transparent,

                //     child: LiquidLinearProgressIndicator(

                //         // borderRadius: BorderRadius.all(Radius.circular(25)),
                //         value: 0.00, // Defaults to 0.5.
                //         valueColor: AlwaysStoppedAnimation(Colors.pink.withOpacity(
                //             0.3)), // Defaults to the current Theme's accentColor.
                //         backgroundColor: Colors
                //             .transparent, // Defaults to the current Theme's backgroundColor.
                //         borderColor: Colors.blue[400],
                //         borderWidth: 5.0,
                //         borderRadius: 12.0,
                //         direction: Axis
                //             .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.vertical.
                //         center: Container(
                //           // margin: EdgeInsets.all(100.0),
                //           decoration: BoxDecoration(
                //               color: Colors.transparent,
                //               shape: BoxShape.circle),
                //           width: 250,
                //           height: 50,
                //           child: Center(
                //             child: CrossFade<String>(
                //                 initialData: "0",
                //                 data: "N/A",
                //                 builder: (value) => Stack(
                //                       children: [
                //                         SpinKitDoubleBounce(
                //                           color: Colors.red,
                //                           size: 50.0,
                //                         ),
                //                         Center(child: Text("ไม่มีการเชื่อมต่อ"))
                //                       ],
                //                     )),
                //           ),
                //         )
                //         )
                //         )
                : StreamBuilder(
                    stream: realTimesittingTimeValue.onValue,
                    builder: (context, snap) {
                      var value = 0;
                      if (snap.hasData &&
                          !snap.hasError &&
                          snap.data.snapshot.value != null) {
                        // value = snap.data.snapshot.value;
                        value = double.tryParse(
                                snap.data.snapshot.value["stime"].toString())
                            .toInt();
                        myalertTimeValue = double.tryParse(snap
                                .data.snapshot.value['AlertTimeInSeconds']
                                .toString())
                            .toInt();
                        percent_stime = double.parse(
                            (value / (myalertTimeValue)).toStringAsFixed(1));

                        if (percent_stime >= 1) {
                          percent_stime = 1.00;
                        }
                        // percent_stime = double.parse((value / (myalertTimeValue + 40))
                        //     .toStringAsFixed(1));
                        // print('percent_stime $percent_stime');
                      }
                      return Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            LinearPercentIndicator(
                              width: 300,
                              animation: false,
                              backgroundColor: Colors.grey[300],
                              lineHeight: 35.0,
                              animationDuration: 2500,
                              percent: percent_stime,
                              center: AutoSizeText(
                                'เวลาที่นั่ง:  ' +
                                    (value.toInt() / 60).round().toString() +
                                    '  นาที',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold),
                              ),
                              linearStrokeCap: LinearStrokeCap.roundAll,
                              progressColor: Color(0xFF86D9D8),
                            ),
                          ],
                        ),
                      );
                    }),
          ),
        ],
      ),
    );
