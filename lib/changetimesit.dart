import 'package:appmattress/homepage.dart';
import 'package:appmattress/text_formatter/text_formatter.dart';
import 'package:appmattress/util/config.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChangeTS extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChangeTSState();
}

class _ChangeTSState extends State<ChangeTS> {
  final dataRef = FirebaseDatabase.instance.reference().child('ice');
  TextEditingController ChangetimeController = TextEditingController();
  String valueChoose;
  List listitem = ['1', '5', '15', '40', '45', '50', '55', '60', '90', '120'];

  showAlertDialog(BuildContext context) {
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("คุณ"),
      content: Text("ได้ทำการเปลี่ยนเวลาในการแจ้งเตือนเรียบร้อยแล้ว"),
      // actions: [
      //   TextButton(
      //     onPressed: () {
      //       Navigator.pop(context);
      //     },
      //     child: const Text('OK'),
      //   ),
      // ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(colorSkyTheme["base_color"]),
          iconTheme: IconThemeData(
              color: Color(colorSkyTheme["text_appName"]), size: 30),
          title: Text(
            'Smart cushion',
            style: Theme.of(context).textTheme.headline4.apply(
                  color: Color(colorSkyTheme["text_appName"]),
                  fontWeightDelta: 3,
                ),
          ),
        ),
        body: ListView(
          children: [
            Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'เปลี่ยนเวลาในการเตือน',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                        width: 100,
                        child: DropdownButton(
                          hint: Text('เลือก'),
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 30,
                          isExpanded: true,
                          style: TextStyle(color: Colors.black, fontSize: 20),
                          value: valueChoose,
                          onChanged: (newValue) {
                            setState(() {
                              valueChoose = newValue;
                            });
                          },
                          items: listitem.map((valueitem) {
                            return DropdownMenuItem(
                              value: valueitem,
                              child: Text(valueitem),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        'นาที',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Center(
                  child: RaisedButton(
                    color: Colors.teal[300],
                    child: Text('ตกลง'),
                    onPressed: () {
                      var chosetime = valueChoose;
                      print('$valueChoose');
                      var timesit = double.parse(chosetime);
                      var chtime = timesit * 60;
                      dataRef
                          .child('Value')
                          .update({'AlertTimeInSeconds': chtime.toInt()});
                      showAlertDialog(context);
                    },
                  ),
                ),
                subHaed(context, "เกี่ยวกับการนั่ง"),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Center(
                    child: Container(
                        height: MediaQuery.of(context).size.height * 0.4,
                        width: MediaQuery.of(context).size.width * 0.90,
                        decoration: BoxDecoration(
                          // border: Border.all(width: 3.0),
                          color: Color(colorSkyTheme["cover_base_color"]),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  5.0) //                 <--- border radius here
                              ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: AutoSizeText(
                              "      เป็นที่ทราบกันดีว่าการขาดการออกกำลังกายก่อให้เกิดโรคไม่ติดต่อหลายโรค แต่หลายคนอาจลืม หรือนึกไม่ถึงว่า การนั่งทำงานตลอดทั้งวันโดยไม่ได้มีกิจกรรมเผาผลาญะพลังงานในร่างกายมากนัก ก็อันตรายต่อสุขภาพได้ไม่น้อยไปกว่าการขาดการออกกำลังกาย และยังอาจเกิดขึ้นได้กับคนที่นั่งทำงานทั้งวันอย่าง พนักงานออฟฟิศ คนขับรถ แคชเชียร์ คนขายตั๋ว พนักงานให้บริการในสำนักงานต่าง ๆ เช่น คอลเซ็นเตอร์ ศูนย์ราชการต่าง ๆ เป็นต้น",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .apply(
                                      fontSizeFactor: 1.2,
                                      color: Colors.black)),
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Center(
                    child: Container(
                        height: MediaQuery.of(context).size.height * 0.17,
                        width: MediaQuery.of(context).size.width * 0.90,
                        decoration: BoxDecoration(
                          // border: Border.all(width: 3.0),
                          color: Color(colorSkyTheme["body_alarmNoti_card"]),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  5.0) //                 <--- border radius here
                              ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: AutoSizeText(
                              "     เวลาที่ที่เหมาะสมสำหรับการลุกเปลี่ยนอิริยาบทคือ 40 นาที  ",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .apply(
                                      fontSizeFactor: 1.4,
                                      color: Colors.black)),
                        )),
                  ),
                ),
              ],
            )),
          ],
        ));
  }
}
