import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class TestDatabase extends StatelessWidget {

  final DatabaseReference database = FirebaseDatabase.instance.reference().child("test");

  sendData() {
    database.once().then((value) => print(value.value[1].toString()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Firebase"),
        backgroundColor: Colors.amber,
      ),
      body: Center(
        child: FlatButton(
            onPressed: () => sendData(),
            child: Text("Send"),
        color: Colors.amber),
      ),
    );
  }
}
