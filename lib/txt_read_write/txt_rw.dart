import 'package:path_provider/path_provider.dart';
import 'dart:io';

Future<String> _localPath() async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> _localFile() async {
  String path = await _localPath();
  return File('$path/databaseNameSM.txt');
}

Future<File> writefile(String textSM) async {
  File fileSM = await _localFile();

  // Write the file
  return fileSM.writeAsString('$textSM');
}

Future<String> readfile() async {
  try {
    final fileSM = await _localFile();

    // Read the file
    String textSM = await fileSM.readAsString();
    return textSM;
  } catch (e) {
    // If encountering an error, return 0
    print("Error Read text ........file");
    return "error";
  }
}

Future<dynamic> removefileSM() async {
  try {
    final fileSM = await _localFile();
    final textSM = await fileSM;
    await fileSM.delete();
  } catch (e) {
    print("Couldn't remove");
  }
}
