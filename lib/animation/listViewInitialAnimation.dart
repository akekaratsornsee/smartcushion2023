import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AnimatedListItem extends StatefulWidget {
  final Widget widget;
  var isStart;

  AnimatedListItem(this.widget,this.isStart, {Key key}) : super(key: key);

  @override
  _AnimatedListItemState createState() => _AnimatedListItemState();
}

class _AnimatedListItemState extends State<AnimatedListItem>  with TickerProviderStateMixin{
  bool _animate = false;

  static bool _isStart;

  AnimationController animation;
  Animation<double> _fadeInFadeOut;
  @override
  void initState() {
    super.initState();
    _isStart = widget.isStart;
    animation = AnimationController(vsync: this, duration: Duration(milliseconds: 2000),);
    _fadeInFadeOut = Tween<double>(begin: 0.3, end: 1).animate(animation) ;


    animation.addStatusListener((status){
      if(status == AnimationStatus.completed){
        animation.reverse();
      }
      else if(status == AnimationStatus.dismissed){
        animation.forward();
      }
    });
    animation.forward();
    
    

  }

  @override
  void dispose() {
    animation.dispose(); 
    super.dispose();
    _animate = false;
  }

  @override
  Widget build(BuildContext context) {
    // if(widget.isStart){
    //     animation.forward();
    // }
    // if(animation.status == AnimationStatus.completed && widget.isStart == false){
    //     animation.reverse();
    // }
    return FadeTransition(
            opacity: _fadeInFadeOut,
            child: widget.widget,
          );
    
    // AnimatedOpacity(
    //   duration: Duration(milliseconds: 2000),
    //   opacity: _animate ? 1 : 0,
    //   curve: Curves.easeInOutQuart,
    //   child: widget.widget,
    // );
  }
}