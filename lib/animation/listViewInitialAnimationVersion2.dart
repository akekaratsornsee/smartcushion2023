import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AnimatedListItemVersion2 extends StatefulWidget {
  final int index;
  final Widget widget;

  AnimatedListItemVersion2(this.index, this.widget, {Key key}) : super(key: key);

  @override
  _AnimatedListItemVersion2State createState() => _AnimatedListItemVersion2State();
}

class _AnimatedListItemVersion2State extends State<AnimatedListItemVersion2> {
  bool _animate = false;

  static bool _isStart = true;

  @override
  void initState() {
    super.initState();
    _isStart
        ? Future.delayed(Duration(milliseconds: widget.index * 100), () {
            setState(() {
              _animate = true;
              _isStart = false;
            });
          })
        : _animate = true;
  }

  @override
  void dispose() {

    super.dispose();
    _animate = false;
    _isStart = true;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 700),
      opacity: _animate ? 1 : 0,
      curve: Curves.easeInOutQuart,
      child: widget.widget,
    );
  }
}