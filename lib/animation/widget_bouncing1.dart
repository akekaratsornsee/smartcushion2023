import 'package:appmattress/changetimesit.dart';
import 'package:flutter/material.dart';

class BouncingButton1 extends StatefulWidget {
  var widgetChild;
  BouncingButton1(this.widgetChild);
  @override
  _BouncingButton1State createState() => _BouncingButton1State();
}

class _BouncingButton1State extends State<BouncingButton1>
    with SingleTickerProviderStateMixin {
  double _scale;
  AnimationController _controller;
  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.05,
    )..addListener(() {
        setState(() {});
      });
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      }
    });
    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;
    return GestureDetector(
      // onTapCancel: _tapDownV2,
      onLongPress: () => _controller.forward(),
      onTap: () {
        // _controller.forward();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ChangeTS()));
      },
      // onTapDown: _tapDown,
      // onTapUp: _tapUp,
      child: Transform.scale(
        scale: _scale,
        child: widget.widgetChild,
      ),
    );
  }

  void _tapDownV2() {
    var i = 0;
    while (i < 20) {
      _controller.forward();
      i++;
    }
  }

  void _tapUpV2() {
    // var i = 0;
    // while(i < 20){
    //   _controller.reverse();
    //   i++;
    // }
  }

  void _tapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _tapUp(TapUpDetails details) {
    _controller.reverse();
  }
}

Widget _animatedButton() {
  return Container(
    height: 70,
    width: 200,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        boxShadow: [
          BoxShadow(
            color: Color(0x80000000),
            blurRadius: 12.0,
            offset: Offset(0.0, 5.0),
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xff33ccff),
            Color(0xffff99cc),
          ],
        )),
    child: Center(
      child: Text(
        'Press',
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    ),
  );
}
